package com.qa.pageActions;

import com.qa.pages.OwnerInformationContPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.FirstProposedInsuredAction.flagowner;

public class OwnerInfoContPageAction extends OwnerInformationContPage{
	
	public OwnerInfoContPageAction() {
		super();
	}
	
	public PrimarycarephysicianAction enterOwnerinfoCont(String typeOfOwner) throws InterruptedException {
		
		if(flagowner == true) {								
//				boolean flag = typeOfOwner.equalsIgnoreCase("Business") || typeOfOwner.equalsIgnoreCase("Qualified Plan") || typeOfOwner.equalsIgnoreCase("412");
				if(typeOfOwner.equalsIgnoreCase("Qualified Plan")) {
					extentTest.log(LogStatus.INFO, " - Owner Information Cont. Page - ");
					switchToFrame(frameOwnerInformationCont);
					EnterText(txtFirstName, "First name", "First Name");
					EnterText(txtLastName, "Last Name", "Last Name");
					EnterText(txtPosition, "Test", "Position or Title");
					ComboSelectValue(dropdownState, "GA", "State");
					takeScreenshot("OwnerInformationdContPage");
					ClickElement(btnNext, "Next Button");
					switchToDefault();
					Thread.sleep(5000);
					return new PrimarycarephysicianAction();	
				}
				else if(typeOfOwner.equalsIgnoreCase("Business")) {
					extentTest.log(LogStatus.INFO, " - Owner Information Cont. Page - ");
					switchToFrame(frameOwnerInformationCont);
					EnterText(txtFirstName_business, "First name", "First Name");
					EnterText(txtLastName_business, "Last Name", "Last Name");
					EnterText(txtPosition_business, "Test", "Position or Title");
					ComboSelectValue(dropdownState_business, "GA", "State");
					takeScreenshot("OwnerInformationdContPage");
					ClickElement(btnNext, "Next Button");
					switchToDefault();
					Thread.sleep(5000);
					return new PrimarycarephysicianAction();	
				}
				else if (typeOfOwner.equalsIgnoreCase("Established Trust")) {
					extentTest.log(LogStatus.INFO, " - Owner Information Cont. Page - ");
					switchToFrame(frameOwnerInformationCont);
					EnterText(txtFirstNameTrustee, "First name", "First Name");
					EnterText(txtLastNameTrustee, "Last Name", "Last Name");
					EnterText(txtSSN, "756339751", "SSN");
					ComboSelectValue(dropdownTrustInfo, "Family", "Trust Information");		
					takeScreenshot("OwnerInformationdContPage");
					ClickElement(btnNext, "Next Button");
					switchToDefault();
					Thread.sleep(5000);
					return new PrimarycarephysicianAction();
					
				}
				else
					return new PrimarycarephysicianAction();
				
		}
		else
			return new PrimarycarephysicianAction();
				
				
			
		
	}	
}
