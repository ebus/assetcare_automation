package com.qa.pageActions;

import com.qa.pages.ThirdPartyOptionPage;
import com.relevantcodes.extentreports.LogStatus;

public class ThirdPartyOptionPageAction extends ThirdPartyOptionPage{
	public ThirdPartyOptionPageAction() {
		super();
	}
	
	//Actions
	public ValidateAndLockDataAction enterThirdPartyOption(String state) throws InterruptedException {
		if(state.equalsIgnoreCase("Florida") || state.equalsIgnoreCase("North Carolina")|| state.equalsIgnoreCase("Vermont") || state.equalsIgnoreCase("Maine")) {
			extentTest.log(LogStatus.INFO, " - Third Party Option Page - ");
			switchToFrame(frameThirdPartyOption);
			
			
			
			if(state.equalsIgnoreCase("Maine")) {
				ClickJSElement(rdo_IWould_Maine, "I would like_rdo");
			}else {
				ClickJSElement(rdo_IWould, "I would like_rdo");
			}
			
			EnterText(txtFirst, "TestFirst3rdParty", "3rdParty_NameFirst");
			EnterText(txtLast, "TestLast3rdParty", "3rdParty_NameLast");
			EnterText(txtAddress, "Address", "3rdParty_Address");
			EnterText(txtCity, "City", "City");
			ComboSelectValue(dropdownState, "Alaska", "dropdown3rdParty_State");
			EnterText(txtZIP, "000124520", "3rdParty_ZIP");
			
			takeScreenshot("ThirdPartyOptionPage");
			ClickElement(btnNext, "Next Button");
			switchToDefault();
			Thread.sleep(5000);
			return new ValidateAndLockDataAction();
			
		}else {
			return new ValidateAndLockDataAction();
		}
	}

}
