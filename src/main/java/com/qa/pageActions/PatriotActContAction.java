package com.qa.pageActions;

import com.qa.pages.PatriotActContPage;
import com.relevantcodes.extentreports.LogStatus;



public class PatriotActContAction extends PatriotActContPage{
	
	public PatriotActContAction() {
		super();
	}
	
	public SuitabilityOwnerAction enterDataPatriotActCont(String formofID_value,String isMultipleOwner) throws InterruptedException {
		

//		String formofID_value = "Other";
		
		if(isMultipleOwner.equalsIgnoreCase("Yes")) {
			extentTest.log(LogStatus.INFO, " - Patriot Act Page - ");
			switchToFrame(framePatriotActCont);
			
			ComboSelectValue(formofID_dropdown, formofID_value, "Form of ID");
			if(formofID_value.equalsIgnoreCase("other"))
				EnterText(other_desc, "other", "other describe");
			
			EnterText(issuar_name, "Company", "Issuer");
			EnterText(name, "ABC", "Name");
			EnterText(streetaddresss, "10 Holmberg Circle", "street addresss");
			EnterText(city, "Newyork", "city");
			ComboSelectValue(state, "MN", "state");
			EnterText(zip, "21212-2717", "zip");
	
			EnterText(numberonID, "7542689", "ID number");
			enterIDexpirationdate("01/01/2030");
			EnterText(stateorcountry, "Texas", "State or Country");
				
			takeScreenshot("PatriotActContPage");
			ClickElement(btnNext, "Next button");	
			switchToDefault();
			Thread.sleep(5000);
			return new SuitabilityOwnerAction();
		}
		else
			return new SuitabilityOwnerAction();
	
	}
	public void enterIDexpirationdate(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		id_expiration_txtMonth.sendKeys(strMonth);
		id_expiration_txtYear.sendKeys(strYear);
		id_expiration_txtDay.sendKeys(strDay);
		
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}}
