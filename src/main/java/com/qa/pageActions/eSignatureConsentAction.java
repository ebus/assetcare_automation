package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.eSignatureConsentPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

public class eSignatureConsentAction extends eSignatureConsentPage{
	
	public eSignatureConsentAction() {
		super();
	}
	
	//Actions
	public eSignaturePartiesAction enterDataESignatureConsent() throws InterruptedException, AWTException {
		WebDriverWait wait = new WebDriverWait(driver, 100); 
		extentTest.log(LogStatus.INFO, " - eSignatureConsent Page - ");
		switchToFrame(frameeSignatureConsent);
		scrollIntoView(btnReviewApplication, driver);
		ClickJSElement(btnReviewApplication, "Review Application button");
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		Thread.sleep(20000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		
			ClickElement(primaryinsured_disclosure, "Click on primary insured disclosure");

		if(secondproposedinsuredflag == true ||secondproposedinsuredillustrationflag == true) 
			ClickElement(secondinsured_disclosure, "Click on second insured disclosure");
		
		ClickJSElement(cbOwner_disclosure, "Click on cbOwner disclosure");
		ClickJSElement(payor_disclosure, "Click on payor disclosure");
		ClickJSElement(coowner_disclosure, "Click on coowner disclosure");
		ClickJSElement(representative_disclosure, "Click on representative disclosure");
		
		takeScreenshot("eSignatureConsentPage");
		ClickElement(btnNext, "NextButton");
		switchToDefault();
		Thread.sleep(5000);
		return new eSignaturePartiesAction();
	}
}
