package com.qa.pageActions;

import com.qa.pages.OwnerInformationPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.FirstProposedInsuredAction.flagowner;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class OwnerInformationPageAction extends OwnerInformationPage{
	
	public OwnerInformationPageAction() {
		super();
	}
	
	
	//Actions:
	public OwnerInfoContPageAction enterOwnerInformationData(String state,  String typeOwner, String relnProposedIns, String ownerFirstName, String ownerLastName, String ownerSSN, String strDOB,
			 String ownerGender, String isMultipleOwner, String mulOwnerFirst, String mulOwnerLast, String mOwner_Street, String mOwner_city, String mOwner_State, String mOwner_ZIPCode, String mOwner_DOB, String mOwner_SSN) throws InterruptedException, AWTException {	
		
		if(flagowner == true) {
		
				extentTest.log(LogStatus.INFO, " - Owner Information Page - ");
				switchToFrame(frameOwnerInformation);
				ComboSelectValue(dropdownTypeOwner, typeOwner, "Type of Owner");
				if(typeOwner.equalsIgnoreCase("Individual")) {
					
					ComboSelectValue(dropdownRelProposedInsured, relnProposedIns, "Relationship to Proposed Insured");
					ClickElement(cbOwnerAddress, "Same owner Address");
					EnterText(emailid, "owner@abc.com", "email id");
					EnterText(txtPhoneNumber, "3119485094", "Phone Number");
					EnterText(txtOwnerFirstName, ownerFirstName, "OwnerFirstName");
					EnterText(txtOwnerLastName, ownerLastName, "OwnerLastName");
					EnterText(txtOwnerSSN, ownerSSN, "Owner SSN");
					enterDOB_OwnerInformation(strDOB);
					selectGender(ownerGender, rdoGender_Male, rdoGender_Female);
					ClickElement(rdoUSCitizen_Yes, "USCitizen_Yes");

					selectYesNoRdoBtn(isMultipleOwner, "Multiple Indicator", rdoMultipleOwners_Yes, rdoMultipleOwners_No);
					if(isMultipleOwner.equalsIgnoreCase("Yes")) {
						ClickElement(btnClickHere, "Proposed Insured");
						switchToDefault();
						Thread.sleep(5000);
						switchToFrame(frameMultipleOwnersInformation);
						EnterText(mowner_txtFirst, mulOwnerFirst, "First");
						EnterText(mowner_txtLast, mulOwnerLast, "Last");
						EnterText(mowner_txtSSN, mOwner_SSN, "SSN");
						enterDOB(mOwner_DOB, mowner_txtMonth, mowner_txtDay, mowner_txtYear);
						EnterText(mowner_txtStreet, mOwner_Street, "Street");
						EnterText(mowner_txtCity, mOwner_city, "City");
						ComboSelectValue(mowner_dropdownState, mOwner_State, "State");
						EnterText(mowner_txtZipcode, mOwner_ZIPCode, "ZIPCode");							
						Robot r = new Robot();
						for(int i=1; i<=2; i++) {
							r.keyPress(KeyEvent.VK_TAB);
							r.keyRelease(KeyEvent.VK_TAB);
							r.keyPress(KeyEvent.VK_ENTER);
							r.keyRelease(KeyEvent.VK_ENTER);
						}
							
					}
					switchToDefault();
					switchToFrame(frameOwnerInformation);	

				}	
				else if(typeOwner.equalsIgnoreCase("Business")) {
					ComboSelectValue(dropdownBusinessType, "LLC", "Business Type");
					ComboSelectValue(dropdownInsurableInterest, "Employee", "Insurable Interest");
					ClickElement(cbOwnerAddress, "Same owner Address");
					EnterText(txtPhoneNumber, "3119485094", "Phone Number");
					EnterText(emailid, "owner@abc.com", "email id");
					EnterText(txtEntityName, "Test", "Entity Name");
					EnterText(txtTaxIDNumber, "432534546", "Tax ID number");
					selectYesNoRdoBtn(isMultipleOwner, "Multiple Indicator", rdoMultipleOwners_Yes, rdoMultipleOwners_No);
					if(isMultipleOwner.equalsIgnoreCase("Yes")) {
						ClickElement(btnClickHere, "Proposed Insured");
						switchToDefault();
						Thread.sleep(5000);
						switchToFrame(frameMultipleOwnersInformation);
						EnterText(mowner_txtFirst, mulOwnerFirst, "First");
						EnterText(mowner_txtLast, mulOwnerLast, "Last");
						EnterText(mowner_txtSSN, mOwner_SSN, "SSN");
						enterDOB(mOwner_DOB, mowner_txtMonth, mowner_txtDay, mowner_txtYear);
						EnterText(mowner_txtStreet, mOwner_Street, "Street");
						EnterText(mowner_txtCity, mOwner_city, "City");
						ComboSelectValue(mowner_dropdownState, mOwner_State, "State");
						EnterText(mowner_txtZipcode, mOwner_ZIPCode, "ZIPCode");							
						Robot r = new Robot();
						for(int i=1; i<=2; i++) {
							r.keyPress(KeyEvent.VK_TAB);
							r.keyRelease(KeyEvent.VK_TAB);
							r.keyPress(KeyEvent.VK_ENTER);
							r.keyRelease(KeyEvent.VK_ENTER);
						}
							
					}
					switchToDefault();
					switchToFrame(frameOwnerInformation);	
				}
				else if(typeOwner.equalsIgnoreCase("Established Trust")) {
					ClickElement(cbOwnerAddress, "Same owner Address");
					EnterText(txtPhoneNumber, "3119485094", "Phone Number");
					EnterText(txtTrust, "Test", "Trust");
					EnterText(txtTaxIDNumber, "432534546", "Tax ID number");
					DateOfTrust("01/01/1990");
					selectYesNoRdoBtn(isMultipleOwner, "Multiple Indicator", rdoMultipleOwners_Yes, rdoMultipleOwners_No);
					if(isMultipleOwner.equalsIgnoreCase("Yes")) {
						ClickElement(btnClickHere, "Proposed Insured");
						switchToDefault();
						Thread.sleep(5000);
						switchToFrame(frameMultipleOwnersInformation);
						EnterText(mowner_txtFirst, mulOwnerFirst, "First");
						EnterText(mowner_txtLast, mulOwnerLast, "Last");
						EnterText(mowner_txtSSN, mOwner_SSN, "SSN");
						enterDOB(mOwner_DOB, mowner_txtMonth, mowner_txtDay, mowner_txtYear);
						EnterText(mowner_txtStreet, mOwner_Street, "Street");
						EnterText(mowner_txtCity, mOwner_city, "City");
						ComboSelectValue(mowner_dropdownState, mOwner_State, "State");
						EnterText(mowner_txtZipcode, mOwner_ZIPCode, "ZIPCode");							
						Robot r = new Robot();
						for(int i=1; i<=2; i++) {
							r.keyPress(KeyEvent.VK_TAB);
							r.keyRelease(KeyEvent.VK_TAB);
							r.keyPress(KeyEvent.VK_ENTER);
							r.keyRelease(KeyEvent.VK_ENTER);
						}
							
					}
					switchToDefault();
					switchToFrame(frameOwnerInformation);	
				}
				else if(typeOwner.equalsIgnoreCase("Qualified Plan")) {
					ClickElement(cbOwnerAddress, "Same owner Address");
					EnterText(txtPhoneNumber, "3119485094", "Phone Number");
					EnterText(emailid, "owner@abc.com", "email id");
					EnterText(txtEntityNameQualifiedPlan, "Test", "Entity Name");
					EnterText(txtTaxIDNumber, "432534546", "Tax ID number");
					selectYesNoRdoBtn(isMultipleOwner, "Multiple Indicator", rdoMultipleOwners_Yes, rdoMultipleOwners_No);
					if(isMultipleOwner.equalsIgnoreCase("Yes")) {
						ClickElement(btnClickHere, "Proposed Insured");
						switchToDefault();
						Thread.sleep(5000);
						switchToFrame(frameMultipleOwnersInformation);
						EnterText(mowner_txtFirst, mulOwnerFirst, "First");
						EnterText(mowner_txtLast, mulOwnerLast, "Last");
						EnterText(mowner_txtSSN, mOwner_SSN, "SSN");
						enterDOB(mOwner_DOB, mowner_txtMonth, mowner_txtDay, mowner_txtYear);
						EnterText(mowner_txtStreet, mOwner_Street, "Street");
						EnterText(mowner_txtCity, mOwner_city, "City");
						ComboSelectValue(mowner_dropdownState, mOwner_State, "State");
						EnterText(mowner_txtZipcode, mOwner_ZIPCode, "ZIPCode");							
						Robot r = new Robot();
						for(int i=1; i<=2; i++) {
							r.keyPress(KeyEvent.VK_TAB);
							r.keyRelease(KeyEvent.VK_TAB);
							r.keyPress(KeyEvent.VK_ENTER);
							r.keyRelease(KeyEvent.VK_ENTER);
						}
							
					}
					switchToDefault();
					switchToFrame(frameOwnerInformation);	
				}
				
//				if(state.equalsIgnoreCase("Florida")) {
//					ClickElement(rdoSecondaryAddressee_No, "SecondaryAddressee_No");
//				}
				
				takeScreenshot("OwnerInformationdPage");
				Thread.sleep(2000);
				ClickJSElement(btnNext, "Next Button");
				switchToDefault();
				Thread.sleep(3000);
				return new OwnerInfoContPageAction();
			
		}else {
			return new OwnerInfoContPageAction();
		}
	}
	
	
	public void enterDOB_OwnerInformation(String dob)
	{
		enterDOB(dob, txtMonth, txtDay, txtYear);
	}
	
	
	public void DateOfTrust(String date)
	{	
		String[] dateDemo = date.split("/");
		String strMonth = dateDemo[0];
		String strDay = dateDemo[1];
		String strYear = dateDemo[2];
		txtMonthTrust.sendKeys(strMonth);	
		txtYearTrust.sendKeys(strYear);
		txtDayTrust.sendKeys(strDay);
		
		extentTest.log(LogStatus.INFO, date + " is entered in Date of Trust");	
	}
}
