package com.qa.pageActions;

import com.qa.pages.OtherInfoPage;
import com.relevantcodes.extentreports.LogStatus;

public class OtherInfoPageAction extends OtherInfoPage{
	
	public OtherInfoPageAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public ExistingInsuranceAction enterdataotherinfo(String state) throws InterruptedException {
		String anyofpolicypremiumspaidthroughpremiumfinancingloan = "Yes";
		String intendnoworinthefuturetoselltransferorassign = "No";
		if(state.equalsIgnoreCase("Ohio")) {
			extentTest.log(LogStatus.INFO, " - Other Info Page - ");
			switchToFrame(frameOtherInfo);
		    if(anyofpolicypremiumspaidthroughpremiumfinancingloan.equalsIgnoreCase("Yes")) {
		    	ClickJSElement(anyofpolicypremiumspaidthroughpremiumfinancingloan_yes,"Click on any of policy premiums paid through premium financing loan as yes");
		    	EnterText(anyofpolicypremiumspaidthroughpremiumfinancingloan_explanation,"Explanation","Enter explanation");		
		    }
		    else
		    	ClickJSElement(anyofpolicypremiumspaidthroughpremiumfinancingloan_no,"Click on any of policy premiums paid through premium financing loan as no");
		    
		    if(intendnoworinthefuturetoselltransferorassign.equalsIgnoreCase("Yes")) {
		    	ClickJSElement(intendnoworinthefuturetoselltransferorassign_yes,"Click on intend now or in the future to sell transfer or assign as yes");
		    	EnterText(intendnoworinthefuturetoselltransferorassign_explanation,"Explanation","Enter explanation");		
		    }
		    else
		    	ClickJSElement(intendnoworinthefuturetoselltransferorassign_no,"Click on intend now or in the future to sell transfer or assign as no");
		   
		    takeScreenshot("OtherInfoPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(2000);			
			return new ExistingInsuranceAction();
		}
		else
			return new ExistingInsuranceAction();
	}

}
