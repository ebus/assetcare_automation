package com.qa.pageActions;

import com.qa.pages.SuitabilityOwnerContPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;


public class SuitabilityOwnerContAction extends SuitabilityOwnerContPage{
	
	public SuitabilityOwnerContAction() {
		super();		
	}
	
	Wait wait = new Wait();
	
	//Actions 
	public SuitabilityAgentAction enterDataSuitabilityOwnerCont(String state,String illustration,String productFundingOptions,String productFundingOptions_illus) throws InterruptedException {
		
		if(state.equalsIgnoreCase("Florida")&&((illustration.equalsIgnoreCase("No")&&productFundingOptions.equalsIgnoreCase("Annuity Funding Whole Life"))||(illustration.equalsIgnoreCase("Yes")&&productFundingOptions_illus.equalsIgnoreCase("Single Premium Annuity")))) {
			
			extentTest.log(LogStatus.INFO, " - Suitability Owner Page - ");
			switchToFrame(frameSuitabilityOwnerCont);
			EnterText(annuitypurchasingreason, "advantage", "annuitypurchasingreason");
			ClickJSElement(investmentobjective_income, "investment objective as income");
			ClickJSElement(risktolerance_conservative, "risk tolerance as conservative");
			EnterText(comments, "comments", "comments");
			EnterText(investmentexperiencetypelengthoftime, "investment experience", "investment experience type length of time");
			EnterText(sourceoffunds, "source of funds", "source of funds");
			EnterText(howlongplantokeepproposedannuity, "10 months", "how long plan to keep proposed annuity");
			ClickJSElement(proposedannuityreplaceproduct_no, "proposed annuity replace product as no");
			ClickJSElement(btnNext, "NextButton");
			switchToDefault();
			Thread.sleep(3000);
			return new SuitabilityAgentAction();
		}
		else
			return new SuitabilityAgentAction();
	}
}
