package com.qa.pageActions;

import java.awt.AWTException;

import com.qa.pages.RequestofFundsPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class RequestofFundsPageAction extends RequestofFundsPage{
	
	public RequestofFundsPageAction() {
		super();
	}
	
	Wait wait = new Wait();
	
	//Actions:
	public ValidateAndLockDataAction detailsOfRequestfund(String illustration,String productFundingOptions_illus,String productFundingOptions,String cdtransfer,String other,String curr_custodian,String company_street_address,String company_city,String company_state,String company_zip,String company_telephone,
			String account_info,String fund_transferred_from,String current_AccNo,String cust_name,String policyvalue,String insuredname,String identification_no,String policy_enclosed,
			String txt1035Exchangesdetail,String policy_current_status,String typeoftaxfreefundtransfer,String fundamount,String otherfund,String amountoffund,String partialfund,
			String maturitydate,String premiumdropinrider,String clientsendfunddirect) throws InterruptedException, AWTException {		
		
		if(((illustration.equalsIgnoreCase("Yes"))&&(productFundingOptions_illus.equalsIgnoreCase("Recurring Premium")))) 
			return new ValidateAndLockDataAction();
		else if((((illustration.equalsIgnoreCase("No"))&&(productFundingOptions.equalsIgnoreCase("Recurring Premium Whole Life"))&&premiumdropinrider.equalsIgnoreCase("No")))||((illustration.equalsIgnoreCase("No"))&&(productFundingOptions.equalsIgnoreCase("Recurring Premium Whole Life"))&&premiumdropinrider.equalsIgnoreCase("Yes")&&clientsendfunddirect.equalsIgnoreCase("Yes")))
			return new ValidateAndLockDataAction();
		else if((illustration.equalsIgnoreCase("Yes"))&&(productFundingOptions_illus.equalsIgnoreCase("Single Premium Cash"))&&cdtransfer.equalsIgnoreCase("No")&&other.equalsIgnoreCase("No"))
			return new ValidateAndLockDataAction();
		else {
			extentTest.log(LogStatus.INFO, " - Request of Funds Page - ");
			switchToFrame(frameRequestofFundsInfo);
			ClickJSElement(btnAdd, "Click here to Add");
			switchToDefault();
			switchToFrame(frameRequestofFunds);
			Thread.sleep(3000);
			EnterText(curr_Custodian, curr_custodian, "Enter current custodian value");
			EnterText(address, company_street_address, "Enter address");
			EnterText(city, company_city, "Enter city");
			ComboSelectValue(state, company_state, "Enter state");
			EnterText(zip, company_zip, "Enter zip");
			EnterText(telephone, company_telephone, "Enter telephone");
			ComboSelectValue(dropDownSelectaccountinfo, account_info, "Select Account info");
			if(account_info.equalsIgnoreCase("Section 1035 Exchanges")) {
				ComboSelectValue(funds_transfered_from, fund_transferred_from, "Select from funds transferred from dropdown");
				EnterText(curr_policyoraccountno, current_AccNo, "Enter current Account No");
				EnterText(customername, cust_name, "Enter customer name");
				EnterText(policy_value, policyvalue, "Enter estimated policy value");
				EnterText(insured_name, insuredname, "Enter insured or annuitant name");
				EnterText(taxpayor_identification_No, identification_no, "Enter taxpayor identification No");
				if(policy_enclosed.equalsIgnoreCase("Yes"))
					ClickJSElement(checkbox_policy_enclosed, "Click on checkbox policy enclosed");
				else
					ClickJSElement(checkbox_policy_lost, "Click on checkbox policy lost");
				
				EnterText(txt1035ExchangesDetails, txt1035Exchangesdetail, "Enter txt1035 Exchanges Details");		
			}
			else if(account_info.equalsIgnoreCase("Qualified Funds: Transfers and Direct Rollovers")) {
				EnterText(curr_policyoraccountno, current_AccNo, "Enter current Account No");
				EnterText(customername, cust_name, "Enter customer name");
				EnterText(policy_value, policyvalue, "Enter estimated policy value");
				EnterText(insured_name, insuredname, "Enter insured or annuitant name");
				EnterText(taxpayor_identification_No, identification_no, "Enter taxpayor identification No");
				if(policy_current_status.equalsIgnoreCase("other")) {
					ClickJSElement(curr_status_other, "Click on other current status");
					EnterText(curr_status_other_desc, "other", "Enter description on other current status");
				}
				else if(policy_current_status.equalsIgnoreCase("Qualified"))
					ClickJSElement(curr_status_qualifiedRetirement_plan, "Click on current status as qualified Retirement plan");
				
				else
					ClickJSElement(curr_status_traditional_IRA, "Click on current status as traditional IRA");
				
				if(typeoftaxfreefundtransfer.equalsIgnoreCase("direct")) 
					ClickJSElement(taxtype_direct_rollover, "Click on taxtype as direct rollover");
	
				else if(typeoftaxfreefundtransfer.equalsIgnoreCase("NonDirect"))
					ClickJSElement(taxtype_nondirect_rollover, "Click on taxtype as nondirect rollover");
				
				else
					ClickJSElement(taxtype_trusteetotrusteetransfer, "Click on taxtype as trustee to trustee transfer");
				
				if(fundamount.equalsIgnoreCase("rollover")) 
					ClickJSElement(amountoffund_rollover, "Click on amount of fund rollover");
	
				else
					ClickJSElement(amountoffund_partial_rollover, "Click on amount of fund partial rollover");		
					
			}
			else {
				EnterText(curr_policyoraccountno, current_AccNo, "Enter current Account No");
				EnterText(customername, cust_name, "Enter customer name");
				EnterText(policy_value, policyvalue, "Enter estimated policy value");
				EnterText(insured_name, insuredname, "Enter insured or annuitant name");
				EnterText(taxpayor_identification_No, identification_no, "Enter taxpayor identification No");
				
				if(otherfund.equalsIgnoreCase("DepositCertificate")) {
					ClickJSElement(fundstype_depositscertificate, "Click on funds type as certificate of deposit");
					enterMaturitydate(maturitydate);
				}
				else
					ClickJSElement(fundstype_mutualfund, "Click on funds type as mutualfund");
				
				if(amountoffund.equalsIgnoreCase("partial")) {
					ClickJSElement(amount_fund_partial, "Click on funds amount as partial");
					if(partialfund.equalsIgnoreCase("amount"))
						ClickJSElement(amount_fund_partial_amount, "Click on partial fund amount as amount");
					else
						ClickJSElement(amount_fund_partial_percentage, "Click on partial fund amount as percentage");
					EnterText(amount_fund_desc, "Partial amount fund description", "Enter partial amount fund description");
				}
				else
					ClickJSElement(amount_fund_all, "Click on funds amount as all of the stated funds");
				
			}
	
			switchToDefault();
			scrollIntoView(btnSave, driver);
			ClickJSElement(btnSave, "SaveButton");
			Thread.sleep(5000);
			switchToFrame(frameRequestofFundsInfo);
			takeScreenshot("RequestofFundsPage");
			ClickElement(btnNext, "Next button");
			Thread.sleep(5000);
			switchToDefault();
			return new ValidateAndLockDataAction();
		}
	}
	
	public void enterMaturitydate(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		maturitydate_month.sendKeys(strMonth);
		maturitydate_year.sendKeys(strYear);
		maturitydate_day.sendKeys(strDay);	
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}
}
