package com.qa.pageActions;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;
import com.qa.pages.PersonalInfoPage;
import com.relevantcodes.extentreports.LogStatus;

public class PersonalInfoAction extends PersonalInfoPage{
	
	public PersonalInfoAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public BeneficiaryInfoPageAction enterDataPersonalInfopage(String medical_application_medium) throws InterruptedException {
		
			if(!medical_application_medium.equalsIgnoreCase("teleinterview")) {
				extentTest.log(LogStatus.INFO, " - Personal Information Page - ");
				switchToFrame(framePersonalInformation);	
				ClickJSElement(rdoQuestionNo_1_no_firstinsured, "rdoQuestionNo_1 as no for first insured");
				ClickJSElement(rdoQuestionNo_2_no_firstinsured, "rdoQuestionNo_2 as no for first insured");
				ClickJSElement(rdoQuestionNo_3_no_firstinsured, "rdoQuestionNo_3 as no for first insured");
				ClickJSElement(rdoQuestionNo_4_no_firstinsured, "rdoQuestionNo_4 as no for first insured");
				ClickJSElement(rdoQuestionNo_5_no_firstinsured, "rdoQuestionNo_5 as no for first insured");
				ClickJSElement(rdoQuestionNo_6_no_firstinsured, "rdoQuestionNo_6 as no for first insured");
				ClickJSElement(rdoQuestionNo_7_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_8_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_9_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_10_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_11_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				
				if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
					ClickJSElement(rdoQuestionNo_1_no_secondinsured, "rdoQuestionNo_1 as no for second insured");
					ClickJSElement(rdoQuestionNo_2_no_secondinsured, "rdoQuestionNo_2 as no for second insured");
					ClickJSElement(rdoQuestionNo_3_no_secondinsured, "rdoQuestionNo_3 as no for second insured");
					ClickJSElement(rdoQuestionNo_4_no_secondinsured, "rdoQuestionNo_4 as no for second insured");
					ClickJSElement(rdoQuestionNo_5_no_secondinsured, "rdoQuestionNo_5 as no for second insured");
					ClickJSElement(rdoQuestionNo_6_no_secondinsured, "rdoQuestionNo_6 as no for second insured");
					ClickJSElement(rdoQuestionNo_7_no_secondinsured, "rdoQuestionNo_7 as no for second insured");
					ClickJSElement(rdoQuestionNo_8_no_secondinsured, "rdoQuestionNo_8 as no for second insured");
					ClickJSElement(rdoQuestionNo_9_no_secondinsured, "rdoQuestionNo_9 as no for second insured");
					ClickJSElement(rdoQuestionNo_10_no_secondinsured, "rdoQuestionNo_10 as no for second insured");
					ClickJSElement(rdoQuestionNo_11_no_secondinsured, "rdoQuestionNo_11 as no for second insured");
					}
				takeScreenshot("PersonalInfoPage");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);			
				return new BeneficiaryInfoPageAction();
			}
			else
				return new BeneficiaryInfoPageAction();
	}

}
