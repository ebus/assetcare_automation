package com.qa.pageActions;

import java.awt.AWTException;

import com.qa.pages.AgentInformationPage;
import com.relevantcodes.extentreports.LogStatus;

public class AgentInformationAction extends AgentInformationPage{
	public AgentInformationAction() {
		super();
	}
	
	//Actions 
	public RepresentativeInformationAction enterDataAgentInfo(String state,String streetaddress,String city,String zip,String email,String knowledge_replacement_exist_insurance,String witness_completion_signatures,String identificationway_wellknown,
			String identificationway_photoid,String identificationway_related,String Desc,String evaluated_other_app,String relationship,String additional_representative) throws InterruptedException, AWTException {
		
		if(state.equalsIgnoreCase("Florida")) {
			extentTest.log(LogStatus.INFO, " - Agent Information Page - ");
			switchToFrame(frameRepresentativeInformation);
	
			EnterText(txtstreetaddress, streetaddress, "Enter street address");
			EnterText(txtcity, city, "Enter city");
			ComboSelectValue(dropdownstate, state,"Select state value");
			EnterText(txtzip, zip, "Enter zip");
			EnterText(phonenumber, "1234567899", "Enter phone number");
			
			EnterText(txtemailaddress, email, "Enter Email id");
			if(knowledge_replacement_exist_insurance.equalsIgnoreCase("Yes"))
				ClickJSElement(knowledge_replacement_exist_insurance_yes, "click on knowledge or reason to believe that replacement of existing insurance or annuity coverage as yes");
			else
				ClickJSElement(knowledge_replacement_exist_insurance_no, "click on knowledge or reason to believe that replacement of existing insurance or annuity coverage as no");
			
			if(witness_completion_signatures.equalsIgnoreCase("Yes"))
				ClickJSElement(witness_completion_signatures_yes, "witnessed the completion of signatures");
			else
				ClickJSElement(witness_completion_signatures_no, "Not witnessed the completion of signatures");
			
			if(identificationway_wellknown.equalsIgnoreCase("Yes")) {
				ClickJSElement(identify_proposed_insured_wellknownyou, "identify proposed insured well known to you");
				EnterText(txt_relatedorwellknown, Desc, "Enter Desc");
			}
			
			if(identificationway_photoid.equalsIgnoreCase("Yes")) 
				ClickJSElement(identify_proposed_insured_photoid, "identify proposed insured by photoid");
			
			if(identificationway_related.equalsIgnoreCase("Yes")) {
				ClickJSElement(identify_proposed_insured_related, "identify proposed insured well known to you");
				EnterText(txt_relatedorwellknown, Desc, "Enter Desc");
			}
			if(evaluated_other_app.equalsIgnoreCase("No"))
				ClickJSElement(applicationevaluatedwithotherapp_no, "application not evaluated with other app");
			else {
				ClickJSElement(applicationevaluatedwithotherapp_yes, "application evaluated with other app");
				EnterText(txt_name, "TestName", "Name");
				ComboSelectValue(dropdown_relationshiptoinsured, relationship,"Select relationship to insured");
				if(relationship.equalsIgnoreCase("Other"))
					EnterText(txt_otherdetails, "Other", "Enter other details");
			}
			EnterText(txt_concernedperson_name, "TestName", "Enter concerned person name");
			EnterText(txt_concernedperson_contact, "9632587412", "Enter concerned person contact");
			if(additional_representative.equalsIgnoreCase("No"))
				ClickJSElement(additional_representatives_no, "Click on no additional representatives");
			else {
				ClickJSElement(additional_representatives_yes, "Click on additional representatives");
				ClickJSElement(additional_representatives_addbutton, "Click here to Add");
				Thread.sleep(3000);
				switchToDefault();
				switchToFrame(frameaddlrepresentative);
				Thread.sleep(2000);
				EnterText(txt_agentname, "TestName", "Enter agent name");
				EnterText(txt_RepresentativeNumber, "9876543", "Enter Representative Number");
				EnterText(txt_licensenumber,"1234","Enter License Number");
				EnterText(txt_split, "45", "Enter split %");
				Thread.sleep(2000);
				switchToDefault();
				scrollIntoView(btnSave, driver);
				ClickJSElement(btnSave, "SaveButton");
				Thread.sleep(3000);
				switchToFrame(frameRepresentativeInformation);
			}
			ClickJSElement(includecoverletterbutton, "Click here include cover letter button");
			switchToDefault();
			switchToFrame(framecoverletter);
			EnterText(txt_coverletter, "Cover letter", "Enter cover letter desc");
			Thread.sleep(3000);
			switchToDefault();
			scrollIntoView(btnSave, driver);
			ClickJSElement(btnSave, "SaveButton");
			switchToFrame(frameRepresentativeInformation);
				
			takeScreenshot("RepresentativeInformationPage");
			ClickJSElement(btnNext, "NextButton");	
			switchToDefault();
			Thread.sleep(5000);
			return new RepresentativeInformationAction();
	}
		else
			return new RepresentativeInformationAction();
		
	}
	
}
