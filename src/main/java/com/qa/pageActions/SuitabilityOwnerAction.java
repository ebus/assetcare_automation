package com.qa.pageActions;

import com.qa.pages.SuitabilityOwnerPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;


public class SuitabilityOwnerAction extends SuitabilityOwnerPage{
	
	public SuitabilityOwnerAction() {
		super();		
	}
	
	Wait wait = new Wait();
	
	//Actions 
	public SuitabilityOwnerContAction enterDataSuitabilityOwner(String state,String illustration,String productFundingOptions,String productFundingOptions_illus) throws InterruptedException {
	
		if(state.equalsIgnoreCase("Florida")&&((illustration.equalsIgnoreCase("No")&&productFundingOptions.equalsIgnoreCase("Annuity Funding Whole Life"))||(illustration.equalsIgnoreCase("Yes")&&productFundingOptions_illus.equalsIgnoreCase("Single Premium Annuity")))) {
			extentTest.log(LogStatus.INFO, " - Suitability Owner Page - ");
			switchToFrame(frameSuitabilityOwner);
			EnterText(annualincome, "1000", "annual income");
			EnterText(sourceincome, "ABC", "source of income");
			EnterText(annualhouseholdincome, "10000", "annual household income");
			EnterText(networth, "10000", "net worth");
			EnterText(liquidAssets, "1000", "liquid Assets");
			ComboSelectValue(taxbracket, "16-25%","Select tax bracket");
			
			ClickJSElement(currentlyownanyannuities_no, "Click on currently own no annuities");
			ClickJSElement(currentlyownlifeinsurance_no, "Click on currently own no lifeinsurance");
			ClickJSElement(incomecoveryourlivingexpenses_yes, "Click on income cover your living expenses");
			ClickJSElement(expectchangesyourlivingexpenses_yes, "Click on expect changes your living expenses");
			ClickJSElement(anticipatechangesyouroutofpocketmedicalexpenses_yes, "Click on anticipate changes your out of pocket medical expenses");
			ClickJSElement(incomesufficienttocoverfuturechanges_yes, "Click on income sufficient to cover future changes");
			ClickJSElement(emergencyfundforunexpectedexpenses_yes, "Click on emergency fund for unexpected expenses");
			ClickJSElement(annuitypurchasenotbasedonrecommendationofagent, "Click on annuity purchase not based on recommendation of agent");
			
			ClickJSElement(btnNext, "NextButton");
			switchToDefault();
			Thread.sleep(3000);
			return new SuitabilityOwnerContAction();
		}
		else
			return new SuitabilityOwnerContAction();
	}
}
