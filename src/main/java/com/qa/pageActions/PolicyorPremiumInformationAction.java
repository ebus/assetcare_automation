package com.qa.pageActions;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.PolicyorPremiumInformationPage;
import com.relevantcodes.extentreports.LogStatus;

public class PolicyorPremiumInformationAction extends PolicyorPremiumInformationPage {
	public PolicyorPremiumInformationAction() {
		super();
	}
	
	public OwnerInformationPageAction enterDataPolicyorPremiumInformation(String state,String illustration,String productFundingOptions_illus,String taxqualification,String productFundingOptions,String faceamount,String annuityowner_firstproposedinsured,
			String premiumorcash,String withapp,String app_premium_amount,String exchange_1035,String exchange_1035_premiumamount,String IRAtransfer,String IRAtransfer_premiumamount,String cdtransfer,String cdtransferpremium,
			String wishStateLifetofacilitate,String other,String sourceoffund,String other_premium,String aob_duration,String cob,String cob_duration,String cob_paymentperiod,String billing_mode,String providingbankinfo,String account_type,
			String account_category,String accountno,String routingno,String Nonforfeiture_Benefit,String aob_inflation,String cob_inflation,String withdrawal_begins,String annuitantpremiumpaymentsage59by2,String inflationduration,
			String electperson,String recurr_premium_payperiod,String recurr_premium_billingmode,String recurrpremium_providingbankinfo,String recurrpremium_account_type,String recurrpremium_account_category,String recurrpremium_cashwithapp,
			String cashorpremium,String cashwithapp_PremiumAmount,String premiumdropinrider,String premiumDropAmount,String clientsendfunddirect,String sendfundafterissuingpolicy)throws InterruptedException, AWTException {
				
		extentTest.log(LogStatus.INFO, " - Policy or Premium Information Page - ");
		switchToFrame(PolicyorPremiumInfo);
		if(illustration.equalsIgnoreCase("No")) {
			EnterText(wholelifefaceAmount, faceamount, "wholelife Face Amount");
			ComboSelectValue(productfundingoptions, productFundingOptions, "Select Product Funding Options");
		}
			
		if((illustration.equalsIgnoreCase("No"))&&(productFundingOptions.equalsIgnoreCase("Single Premium Whole Life")||productFundingOptions.equalsIgnoreCase("Single Premium Whole Life with ROP"))) {
			if(premiumorcash.equalsIgnoreCase("Yes")) {				
				ClickJSElement(checkbox_premiumorcash, "checkbox selected for premium or cash");				
				if(withapp.equalsIgnoreCase("Yes")) {
					ClickJSElement(withapplication_yes, "checkbox selected for with application");
					EnterText(application_premiumAmount, app_premium_amount, "Enter application premium Amount");
				}
				else
					ClickJSElement(withapplication_no, "checkbox selected for with application no");
			}
			
			if(cdtransfer.equalsIgnoreCase("Yes")) {
				ClickJSElement(checkbox_cdtransfer, "checkbox selected for cd transfer");
				EnterText(cdtransfer_premiumAmount, cdtransferpremium, "Enter cdtransfer premium Amount");
			
				if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
					ClickJSElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
				else
					ClickJSElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
			}
			if(other.equalsIgnoreCase("Yes")){
				ClickJSElement(checkbox_other, "checkbox selected for other");
				EnterText(other_sourceoffunding, sourceoffund, "Enter source of funding");
				EnterText(other_premiumAmount, other_premium, "Enter other premium Amount");
				if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
					ClickJSElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
				else
					ClickJSElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
			
			}
			if(productFundingOptions.equalsIgnoreCase("Single Premium Whole Life")) {
				ComboSelectValue(AOB, aob_duration, "Select AOB duration");
				if(state.equalsIgnoreCase("Massachusetts")) {
					EnterText(AOB_premium, "1000", "Enter AOB premium Amount");

				}
				if(cob.equalsIgnoreCase("Yes")) {
					ClickJSElement(COB_checkbox, "checkbox selected for cob duration");
					ComboSelectValue(COB_duration, cob_duration, "Select COB duration");
					ComboSelectValue(COB_paymentperiod, cob_paymentperiod, "Select COB payment period");
					if(!cob_paymentperiod.equalsIgnoreCase("Single-Pay")) {
						if(billing_mode.equalsIgnoreCase("Monthly Bank Draft")) {					
							ComboSelectValue(COB_billingmode, billing_mode, "Select billing mode");
							if(providingbankinfo.equalsIgnoreCase("Yes")) {
								ClickJSElement(provideMnthlyDraftinformation_yes, "Providing Mnthly Draft information");
								if(account_type.equalsIgnoreCase("Saving"))
									ClickJSElement(account_type_saving, "Select saving account type");
								else
									ClickJSElement(account_type_checking, "Select check account type");
								
								if(account_category.equalsIgnoreCase("consumer"))
									ClickJSElement(account_Category_consumer, "Select consumer account Category");
								else
									ClickJSElement(account_Category_business, "Select business account Category");
								
							EnterText(accountnumber, accountno, "Enter account number");
							EnterText(routingnumber, routingno, "Enter routing number");
												
						}
							else
								ClickJSElement(provideMnthlyDraftinformation_no, "not providing Mnthly Draft information");
						
					}
						else
							ComboSelectValue(COB_billingmode, billing_mode, "Select billing mode");

					}
				}
			}
			else {
				if(cob.equalsIgnoreCase("Yes")) {
					ClickJSElement(COB_checkbox, "checkbox selected for cob duration");
					}
				  }
			if(Nonforfeiture_Benefit.equalsIgnoreCase("Yes"))
				ClickJSElement(Nonforfeiturebenefit_yes, "Click on Nonforfeiturebenefit as yes");
			else
				ClickJSElement(Nonforfeiturebenefit_no, "Click on Nonforfeiturebenefit as no");
			if(aob_inflation.equalsIgnoreCase("None")&& cob.equalsIgnoreCase("Yes")) {
				ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
				ComboSelectValue(COB_inflation, cob_inflation, "Select COB inflation");
				ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
			}
			else {
				ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
				ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
			}
			if(electperson.equalsIgnoreCase("Yes")) {
				
				ClickJSElement(electtodesignateaperson_yes, "Click on elect to designate a person");
				enterpersondetails();
				switchToDefault();
				switchToFrame(PolicyorPremiumInfo);
				Thread.sleep(2000);
				
			}
			else
				ClickJSElement(electtodesignateaperson_no, "Click on elect not to designate a person");
				
			
		}
		else if(illustration.equalsIgnoreCase("Yes")&&productFundingOptions_illus.equalsIgnoreCase("Single Premium Cash")) {
			if(premiumorcash.equalsIgnoreCase("Yes")) {				
				ClickJSElement(checkbox_premiumorcash, "checkbox selected for premium or cash");				
				if(withapp.equalsIgnoreCase("Yes")) {
					ClickJSElement(withapplication_yes, "checkbox selected for with application");
					EnterText(application_premiumAmount, app_premium_amount, "Enter application premium Amount");
				}
				else
					ClickJSElement(withapplication_no, "checkbox selected for with application no");
			}
			
		if(cdtransfer.equalsIgnoreCase("Yes")) {
			ClickJSElement(checkbox_cdtransfer, "checkbox selected for cd transfer");
			EnterText(cdtransfer_premiumAmount, cdtransferpremium, "Enter cdtransfer premium Amount");
		
			if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
				ClickJSElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
			else
				ClickJSElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
		}
		if(other.equalsIgnoreCase("Yes")){
			ClickJSElement(checkbox_other, "checkbox selected for other");
			EnterText(other_sourceoffunding, sourceoffund, "Enter source of funding");
			EnterText(other_premiumAmount, other_premium, "Enter other premium Amount");
			if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
				ClickElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
			else
				ClickElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
		
		}
		if(electperson.equalsIgnoreCase("Yes")) {
			
			ClickJSElement(electtodesignateaperson_yes, "Click on elect to designate a person");
			enterpersondetails();
			switchToDefault();
			switchToFrame(PolicyorPremiumInfo);
			Thread.sleep(2000);
			
		}
		else
			ClickJSElement(electtodesignateaperson_no, "Click on elect not to designate a person");
		}
		else if((illustration.equalsIgnoreCase("No")&&productFundingOptions.equalsIgnoreCase("Annuity Funding Whole Life"))||(illustration.equalsIgnoreCase("Yes")&&productFundingOptions_illus.equalsIgnoreCase("Single Premium Annuity"))) {
			if(illustration.equalsIgnoreCase("No")) {
				ComboSelectValue(recurringpremium_payperiod, cob_paymentperiod, "Select recurring premium mode");
				ComboSelectValue(tax_qualification, taxqualification, "Select Tax qualification");
			}
			if(annuityowner_firstproposedinsured.equalsIgnoreCase("Yes"))
				ClickJSElement(firstproposedinsured_ownerannuity_yes, "checkbox selected for first proposed insured as owner of annuity");
			else {
				ClickJSElement(firstproposedinsured_ownerannuity_no, "checkbox selected for first proposed insured as not an owner of annuity");
				ClickJSElement(firstproposedinsured_annuity_no, "checkbox selected for first proposed insured as not an owner of annuity");
			}
			if(premiumorcash.equalsIgnoreCase("Yes")) {				
					ClickJSElement(checkbox_premiumorcash, "checkbox selected for premium or cash");				
					if(withapp.equalsIgnoreCase("Yes")) {
						ClickJSElement(withapplication_yes, "checkbox selected for with application");
						EnterText(application_premiumAmount, app_premium_amount, "Enter application premium Amount");
					}
					else
						ClickJSElement(withapplication_no, "checkbox selected for with application no");
				}
				
			if(taxqualification.equalsIgnoreCase("Qualified")) {
				if(IRAtransfer.equalsIgnoreCase("Yes")) {
					ClickJSElement(checkbox_IRAtransfer, "checkbox selected for IRAtransfer");
					EnterText(IRAtransfer_premiumAmount, IRAtransfer_premiumamount, "Enter IRA Transfer premium Amount");
					}
			}
			else {
				if(exchange_1035.equalsIgnoreCase("Yes")) {
					ClickJSElement(checkbox_exchange1035, "checkbox selected for 1035 exchange");
					EnterText(exchange1035_premiumAmount, exchange_1035_premiumamount, "Enter 1035 exchange premium Amount");
					}
			}
			if(cdtransfer.equalsIgnoreCase("Yes")) {
				ClickJSElement(checkbox_cdtransfer, "checkbox selected for cd transfer");
				EnterText(cdtransfer_premiumAmount, cdtransferpremium, "Enter cdtransfer premium Amount");
				if(illustration.equalsIgnoreCase("No")) {
					if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
						ClickJSElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
					else
						ClickJSElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
					}
				}
			if(other.equalsIgnoreCase("Yes")){
				ClickJSElement(checkbox_other, "checkbox selected for other");
				EnterText(other_sourceoffunding, sourceoffund, "Enter source of funding");
				EnterText(other_premiumAmount, other_premium, "Enter other premium Amount");
				if(illustration.equalsIgnoreCase("No")) {
					if(wishStateLifetofacilitate.equalsIgnoreCase("Yes"))
						ClickElement(wishStateLifetofacilitate_yes, "checkbox selected for wish for State Life to facilitate the Request of Funds ");
					else
						ClickElement(wishStateLifetofacilitate_no, "checkbox selected for not  wish for State Life to facilitate the Request of Funds ");
				}
			}
			if(illustration.equalsIgnoreCase("No")) {
				ComboSelectValue(AOB, aob_duration, "Select AOB duration");
				if(cob.equalsIgnoreCase("Yes")) {
					ClickJSElement(COB_checkbox, "checkbox selected for cob duration");
					ComboSelectValue(COB_duration, cob_duration, "Select COB duration");
				}
				if(Nonforfeiture_Benefit.equalsIgnoreCase("Yes"))
					ClickJSElement(Nonforfeiturebenefit_yes, "Click on Nonforfeiturebenefit as yes");
				else
					ClickJSElement(Nonforfeiturebenefit_no, "Click on Nonforfeiturebenefit as no");
				if(aob_inflation.equalsIgnoreCase("None")&& cob.equalsIgnoreCase("Yes")) {
					ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
					ComboSelectValue(COB_inflation, cob_inflation, "Select COB inflation");
					ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
				}
				else {
					ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
					ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
				}
				ComboSelectValue(withdrawalbegins, withdrawal_begins, "Select Withdrawal Begins");
			}
			Thread.sleep(2000);
			if(annuitantpremiumpaymentsage59by2.equalsIgnoreCase("Yes"))
				ClickJSElement(annuitantpremiumpaymentsage59by2_yes, "Click on annuitantpremiumpaymentsage59by2 as Yes");
			else
				ClickJSElement(annuitantpremiumpaymentsage59by2_no, "Click on annuitantpremiumpaymentsage59by2 as no");
			
			
			if(electperson.equalsIgnoreCase("Yes")) {
				
				ClickJSElement(electtodesignateaperson_yes, "Click on elect to designate a person");
				enterpersondetails();
				switchToDefault();
				switchToFrame(PolicyorPremiumInfo);
				Thread.sleep(2000);
				
			}
			else
				ClickJSElement(electtodesignateaperson_no, "Click on elect not to designate a person");

			
		}
		else {
			if(illustration.equalsIgnoreCase("No"))
				ComboSelectValue(recurringpremium_payperiod, cob_paymentperiod, "Select recurring premium mode");
			ComboSelectValue(recurringpremium_billingmode, recurr_premium_billingmode, "Select recurring premium billing mode");
			if(recurr_premium_billingmode.equalsIgnoreCase("Monthly Bank Draft")) {					
				if(recurrpremium_providingbankinfo.equalsIgnoreCase("Yes")) {
					ClickJSElement(recurrpremium_provideMnthlyDraftinformation_yes, "Providing Mnthly Draft information");
					if(recurrpremium_account_type.equalsIgnoreCase("Saving"))
						ClickJSElement(recurrpremium_account_type_saving, "Select saving account type");
					else
						ClickJSElement(recurrpremium_account_type_checking, "Select check account type");
					
					if(recurrpremium_account_category.equalsIgnoreCase("consumer"))
						ClickJSElement(recurrpremium_account_Category_consumer, "Select consumer account Category");
					else
						ClickJSElement(recurrpremium_account_Category_business, "Select business account Category");
					
				EnterText(recurrpremium_accountnumber, accountno, "Enter account number");
				EnterText(recurrpremium_routingnumber, routingno, "Enter routing number");
			}
			else
				ClickJSElement(recurrpremium_provideMnthlyDraftinformation_no, "not providing Mnthly Draft information");
			
		}
			if(cashorpremium.equalsIgnoreCase("Yes")) {
				ClickJSElement(checkbox_premiumorcash, "Click on checkbox premium or cash");
				if(recurrpremium_cashwithapp.equalsIgnoreCase("Yes")) {
					ClickJSElement(cashwithapp_yes, "Click on cash with app yes");
					EnterText(cashwithapp_premiumamount, cashwithapp_PremiumAmount, "Enter cashwithapp premium amount");
				}
				else
					ClickJSElement(cashwithapp_no, "Click on cash with app no");
			}
			if(illustration.equalsIgnoreCase("No")) {
				ComboSelectValue(AOB, aob_duration, "Select AOB duration");
				if(cob.equalsIgnoreCase("Yes")) {
					ClickJSElement(COB_checkbox, "checkbox selected for cob duration");
					ComboSelectValue(COB_duration, cob_duration, "Select COB duration");
				}
			
				if(Nonforfeiture_Benefit.equalsIgnoreCase("Yes"))
					ClickJSElement(Nonforfeiturebenefit_yes, "Click on Nonforfeiturebenefit as yes");
				else
					ClickJSElement(Nonforfeiturebenefit_no, "Click on Nonforfeiturebenefit as no");

				if(aob_inflation.equalsIgnoreCase("None")&& cob.equalsIgnoreCase("Yes")) {
					ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
					ComboSelectValue(COB_inflation, cob_inflation, "Select COB inflation");
					ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
				}
				else {
					ComboSelectValue(AOB_inflation, aob_inflation, "Select AOB inflation");
					ComboSelectValue(Inflation_duration, inflationduration, "Select inflation duration");
				}
			
				if(premiumdropinrider.equalsIgnoreCase("Yes")) {
					ClickJSElement(premiumdroprider_checkbox,"checkbox selected for premium drop in rider");
					EnterText(premiumdropAmount, premiumDropAmount, "Enter premium drop in amount");
				
					if(clientsendfunddirect.equalsIgnoreCase("Yes")) {
						ClickJSElement(checkbox_clientsendfunddirect, "Click on Client will send funds directly to State Life Insurance Company.");
						if(sendfundafterissuingpolicy.equalsIgnoreCase("Yes")) 
							ClickJSElement(checkbox_sendthefunds, "Click on Send the funds after the Policy has been issued.");
						
						else
							ClickJSElement(checkbox_sendfundafterunderwriting, "Click on Send the funds after underwriting has been completed.");
						
					}
					else
						ClickJSElement(checkbox_insurance_req_fund, "Click on The State Life Insurance Company will request funds");
				}
		}	
			if(electperson.equalsIgnoreCase("Yes")) {
				
				ClickJSElement(electtodesignateaperson_yes, "Click on elect to designate a person");
				enterpersondetails();
				switchToDefault();
				switchToFrame(PolicyorPremiumInfo);
				Thread.sleep(2000);
				
			}
			else
				ClickJSElement(electtodesignateaperson_no, "Click on elect not to designate a person");
			}
		
		takeScreenshot("PolicyorInformationPage");
		ClickJSElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(3000);
		return new OwnerInformationPageAction();
}
	public void enterpersondetails() throws AWTException, InterruptedException
	{	
			ClickElement(enterdetails, "Click on enter details button");
			switchToDefault();
			switchToFrame(framepersonDetails);
			Thread.sleep(2000);
			EnterText(designatedperson_firstname, "Jack", "Enter designated person firstname");
			EnterText(designatedperson_lastname, "Fernan", "Enter designated person lastname");
			EnterText(designatedperson_address, "10 Russel Street", "Enter designated person address");
			EnterText(designatedperson_city, "BCDF", "Enter designated person city");
			ComboSelectValue(designatedperson_state, "Indiana", "Enter designated person state");
			EnterText(designatedperson_zip, "21212-2717", "Enter designated person zip");
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
	}
