package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageAction extends LoginPage{
	
	public LoginPageAction() {
		super();
	}
	
	//Actions:
	public CaseInformationPageAction logIn(String un, String pwd) throws AWTException, InterruptedException{
		extentTest.log(LogStatus.INFO, " - Login Page - ");
		EnterText(txtUserId, un, "UserId");
		EnterText(txtPassWord, pwd, "Password");
		takeScreenshot("LogInPage");
		ClickElement(btnLogin, "Login Button");
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(5000);
		ClickJSElement(btnStartNewCase, "Start New Case Button");
		return new CaseInformationPageAction();
	}
	
	
		
}
