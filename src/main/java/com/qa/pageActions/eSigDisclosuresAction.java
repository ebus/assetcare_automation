package com.qa.pageActions;

import com.qa.pages.eSigDisclosuresPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;
//import static com.qa.pageActions.ProposedInsuredInfoAction.flagJuvenile;

public class eSigDisclosuresAction extends eSigDisclosuresPage{
	
	public eSigDisclosuresAction() {
		super();
	}
	
	//Actions
	public eSigDisclosuresCoownerAction enterDataESigDisclosures() throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - eSigDisclosures Page - ");
		switchToFrame(frameESigDisclosures);
		
		ClickElement(rdofirstProInsYes, "First Proposed Insured acknowledged Yes radio button");
		ComboSelectValue(dropdownIdentification_firstPro, "Drivers License", "Proof of Identification");
		
		if(secondproposedinsuredflag == true ||secondproposedinsuredillustrationflag == true) {
			ClickElement(rdosecondProInsYes, "Second Proposed Insured acknowledged Yes radio button");
			ComboSelectValue(dropdownIdentification_secondPro, "Drivers License", "Proof of Identification");
		}	
				
			ClickElement(rdoProInsYes_Owner, "Proposed Insured Yes radio button for Owner");
			ComboSelectValue(dropdownIdentification_Owner, "Drivers License", "Proof of Identification for Owner");
		
		
		ClickElement(rdopayorProInsYes, "Payor Insured acknowledged Yes radio button");
		ComboSelectValue(dropdownIdentification_payor, "Drivers License", "Proof of Identification");
		

		takeScreenshot("eSigDisclosuresPage");
		ClickElement(btnNext, "Next Button");
		Thread.sleep(5000);
		switchToDefault();
		return new eSigDisclosuresCoownerAction();
			
		
	}

}
