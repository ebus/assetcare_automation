package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.ApplicationSetupPage;
import com.relevantcodes.extentreports.LogStatus;

public class ApplicationSetupAction extends ApplicationSetupPage{
	
	public ApplicationSetupAction() {
		super();
	}
	public static boolean secondproposedinsuredflag;
	
	
	//Actions:
	public FirstProposedInsuredAction enterData(String illustration, String agentCode,String licenseNumber,String secondproposedinsured,String thirdPartyHIPAA,String name,String address,String city,
			String thirdpartystate,String zip,String medical_application_medium) throws AWTException, InterruptedException {
		secondproposedinsuredflag = false;
		extentTest.log(LogStatus.INFO, " - Application Setup Page - ");
		switchToFrame(frameProposedInsuredInformation);
		ComboSelectValue(agentcode, agentCode,"Agent's Code");
		ComboSelectValue(licensenumber, licenseNumber,"License Number");
		if(illustration.equalsIgnoreCase("No")) {
			if(secondproposedinsured.equalsIgnoreCase("Yes")) {
				ClickElement(secondproposedinsured_yes, "second proposed insured_yes");
				secondproposedinsuredflag = true;
			}
			else
				ClickElement(secondproposedinsured_no, "second proposed insured_no");
		}
		if(thirdPartyHIPAA.equalsIgnoreCase("Yes")) {
			ClickElement(thirdPartyHIPAA_yes, "click on thirdParty HIPAA_yes");
			EnterText(thirdPartyAuthorizationName, name, "third Party Authorization Name");
			EnterText(thirdPartyAuthorizationAddress, address, "third Party Authorization Address");
			EnterText(thirdPartyAuthorizationCity, city, "third Party Authorization City");
			ComboSelectValue(thirdPartyAuthorizationState, thirdpartystate, "third Party Authorization State");
			EnterText(thirdPartyAuthorizationZip, zip, "third Party Authorization Zip");
		}
		else
			ClickElement(thirdPartyHIPAA_no, "click on thirdParty HIPAA_no");
		
//		ClickElement(common_ineligible_impairments, "common ineligible impairments");
//		
//		Thread.sleep(30000);
//		Robot r = new Robot();
//		r.keyPress(KeyEvent.VK_ALT);
//		r.keyPress(KeyEvent.VK_F4);
//		r.keyRelease(KeyEvent.VK_F4);
//		r.keyRelease(KeyEvent.VK_ALT);
		
		Thread.sleep(2000);
		
		ClickElement(assetcaredeclaration, "click on assetcare declaration");
		
		if(medical_application_medium.equalsIgnoreCase("teleinterview"))
			ClickElement(medicalapplicationthroughteleinterview, "medical application through teleinterview");
		else
			ClickElement(medicalapplicationthroughfullunderwriting, "medical application through full underwriting");
		

		takeScreenshot("ApplicationSetupPage");
		ClickElement(btnNext, "Next Button");
		switchToDefault();
		Thread.sleep(5000);
		return new FirstProposedInsuredAction();
	}
	
}

