package com.qa.pageActions;

import com.qa.pages.eSigDisclosuresCoownerPage;
import com.relevantcodes.extentreports.LogStatus;


public class eSigDisclosuresCoownerAction extends eSigDisclosuresCoownerPage{
	
	public eSigDisclosuresCoownerAction() {
		super();
	}
	
	//Actions
	public eSignatureConsentAction enterDataESigDisclosurescoowner(String isMultipleOwner) throws InterruptedException {
		if(isMultipleOwner.equalsIgnoreCase("Yes")) {
			extentTest.log(LogStatus.INFO, " - eSigDisclosures Page - ");
			switchToFrame(frameESigDisclosures);
			
			ClickElement(rdocoownerInsYes, "Co owner Insured acknowledged Yes radio button");
			ComboSelectValue(dropdownIdentification_coowner, "Drivers License", "Proof of Identification");
	
			takeScreenshot("eSigDisclosurescoownerPage");
			ClickElement(btnNext, "Next Button");
			Thread.sleep(5000);
			switchToDefault();
			return new eSignatureConsentAction();
		}
		else
			return new eSignatureConsentAction();
		
	}

}
