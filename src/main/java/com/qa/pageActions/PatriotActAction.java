package com.qa.pageActions;

import com.qa.pages.PatriotActPage;
import com.relevantcodes.extentreports.LogStatus;


public class PatriotActAction extends PatriotActPage{
	
	public PatriotActAction() {
		super();
	}
	
	public PatriotActContAction enterDataPatriotAct(String cashorsavings,String earningsincome,String inheritance,String insuranceproceeds,String investementproceeds,String pension_IRA_RetirementSavings,
			String legalSettlements,String gift,String saleofbusiness,String transferorExchange_from_Existing_Life_Insurance,String transferorExchange_from_Existing_Annuity,
			String other,String otherfund,String formofID_value,String certify,String notifiedbyIRS,String State) throws InterruptedException {
			
		extentTest.log(LogStatus.INFO, " - Patriot Act Page - ");
		switchToFrame(framePatriotAct);
		if(cashorsavings.equalsIgnoreCase("Yes"))
			ClickJSElement(cashorsavings_checkbox, "Click on cash or savings_checkbox");
		
		if(earningsincome.equalsIgnoreCase("Yes"))
			ClickJSElement(earningsincome_checkbox, "Click on earnings income_checkbox");
		
		if(inheritance.equalsIgnoreCase("Yes"))
			ClickJSElement(inheritance_checkbox, "Click on inheritance_checkbox");
		
		if(insuranceproceeds.equalsIgnoreCase("Yes"))
			ClickJSElement(insuranceproceeds_checkbox, "Click on insurance proceeds_checkbox");
		
		if(investementproceeds.equalsIgnoreCase("Yes"))
			ClickJSElement(investementproceeds_checkbox, "Click on investement proceeds_checkbox");
		
		if(pension_IRA_RetirementSavings.equalsIgnoreCase("Yes"))
			ClickJSElement(pension_IRA_RetirementSavings_checkbox, "Click on pension or IRA Retirement Savings_checkbox");
		
		if(legalSettlements.equalsIgnoreCase("Yes"))
			ClickJSElement(legalSettlements_checkbox, "Click on legal Settlements_checkbox");
		
		if(gift.equalsIgnoreCase("Yes"))
			ClickJSElement(gift_checkbox, "Click on gift_checkbox");
				
		if(transferorExchange_from_Existing_Annuity.equalsIgnoreCase("Yes"))
			ClickJSElement(transferorExchange_from_Existing_Annuity_checkbox, "Click on transfer or Exchange from Existing Annuity_checkbox");
		
		if(saleofbusiness.equalsIgnoreCase("Yes"))
			ClickJSElement(saleofbusiness_checkbox, "Click on sale of business_checkbox");
		
		if(transferorExchange_from_Existing_Life_Insurance.equalsIgnoreCase("Yes"))
			ClickJSElement(transferorExchange_from_Existing_Life_Insurance_checkbox, "Click on transfer or Exchange from Existing Life Insurance_checkbox");
		
		if(other.equalsIgnoreCase("Yes")) {
			ClickJSElement(other_checkbox, "Click on other_checkbox");
			Thread.sleep(3000);
			EnterText(otherfunds, otherfund, "other fund details");
		}
		ComboSelectValue(formofID_dropdown, formofID_value, "Form of ID");
		if(formofID_value.equalsIgnoreCase("other"))
			EnterText(other_desc, "other", "other describe");
		
		EnterText(issuar_name, "Company", "Issuer");
		EnterText(name, "ABC", "Name");
		ClickJSElement(addresssameasowneraddress_checkbox, "Click on address same as owner address_checkbox");
		EnterText(numberonID, "7542689", "ID number");
		enterIDexpirationdate("01/01/2030");
		EnterText(stateorcountry, State, "State or Country");
		if(certify.equalsIgnoreCase("No")) {
			ClickJSElement(unabletocertify, "Click on unable to certify button");
			EnterText(reasonforunabletocertify, "Unable to certify", "Enter reason");
		}
		else
			ClickJSElement(certifybtn, "Click on unable to certify button");
		if(notifiedbyIRS.equalsIgnoreCase("Yes"))
			ClickJSElement(notify_yes, "Click on notify button");
		else
			ClickJSElement(notify_no, "Click on unable to notify button");
			
		takeScreenshot("PatriotActPage");
		ClickElement(btnNext, "Next button");	
		switchToDefault();
		Thread.sleep(5000);
		return new PatriotActContAction();	
	
	}
	public void enterIDexpirationdate(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		id_expiration_txtMonth.sendKeys(strMonth);
		id_expiration_txtYear.sendKeys(strYear);
		id_expiration_txtDay.sendKeys(strDay);
		
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}}
