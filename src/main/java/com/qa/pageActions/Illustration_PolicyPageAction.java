package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.Illustration_PolicyPage;
import com.relevantcodes.extentreports.LogStatus;


public class Illustration_PolicyPageAction extends Illustration_PolicyPage{
	
	public Illustration_PolicyPageAction() {
		super();
	}
	
	public static boolean secondproposedinsuredillustrationflag;
	public static boolean annuitypremiumillustrationflag;
	public static boolean childbenefitRider_illustration_flag;
	public static boolean returnofpremium_illustration_flag;
	
	
	//Action
	public ApplicationSetupAction enterDataPolicyPage(String illustration,String state,String firstinsuredrisk, String tableratingfirstinsured,
			String secondinsured, String secondinsured_fstname, String secondinsured_lstname, String gender_secondinsured, String dob,
			String secondinsuredrisk, String tableratingsecondinsured,String productFundingOptions,String recurr_premium_payperiod, String taxqualification, String inputmethod, String annuity_premium_amount, String life_premuim_amount, String inputmethod_recurringpremium,String inputmethod_premiumamount,
			String inputmethod_faceamount,String inputmethod_initialmonthlyltcbenefit,String acceleration_Benefits_Duration,String COB_Rider,String COB_paymentoption,String COB_duration,String COB_inflation,
			String AOB_inflation,String inflationduration,String PremiumDropinRider,String PremiumDropinRiderAmount,String NonforfeitureRider,String yearstoquote,String summarywithsign,String aboutOA,
			String financialprofessionals,String revisedQuote,String policynumber,String returnofpremium_cashfunding,String policyholderpayspremium,String startingyearthroughyear ) throws AWTException, InterruptedException{

		secondproposedinsuredillustrationflag = false; 
		returnofpremium_illustration_flag = false; 
		annuitypremiumillustrationflag = false;
		WebDriverWait wait = new WebDriverWait(driver, 100); 
		
		if(illustration.equalsIgnoreCase("Yes")) {
			extentTest.log(LogStatus.INFO, " - Illustration Policy Page - ");
			switchToFrame(frameIllustrationPolicy);
			if(firstinsuredrisk.equalsIgnoreCase("tobacco")) 
				ClickJSElement(firstinsured_tobaccobutton, "select tobacco button");
			ComboSelectValue(firstinsureddropdowntablerating, tableratingfirstinsured, "select table rating value");
			if(secondinsured.equalsIgnoreCase("Yes")) {
				ClickJSElement(secondinsuredbutton, "Click on second insured button");
				secondproposedinsuredillustrationflag = true;
			
			EnterText(secondinsured_firstname, secondinsured_fstname, "Enter secondinsured firstname");
			EnterText(secondinsured_lastname, secondinsured_lstname, "Enter secondinsured lastname");
			if(gender_secondinsured.equalsIgnoreCase("Male"))
				ClickJSElement(secondinsured_gender_male, "select gender as male");
			
			enterDOB(dob);
			
			if(secondinsuredrisk.equalsIgnoreCase("tobacco")) 
				ClickJSElement(secondinsured_tobacco, "select tobacco button for second insured");
			
			ComboSelectValue(secondinsured_dropdowntablerating, tableratingsecondinsured, "select table rating value");
		}
			if(productFundingOptions.equalsIgnoreCase("Recurring Premium")){
				ClickJSElement(recurringpremium, "select recurring premium");
				ComboSelectValue(payoption, recurr_premium_payperiod, "select recurring premium pay period");
				if(inputmethod_recurringpremium.equalsIgnoreCase("Premium Amount")) {
					ClickJSElement(premiumamount, "select premium amount");
					EnterText(txtpremiumamount, inputmethod_premiumamount, "Enter premium amount");
				}
				else if(inputmethod_recurringpremium.equalsIgnoreCase("Face Amount")) {
					ClickJSElement(faceamount, "select Face amount");
					EnterText(txtfaceamount, inputmethod_faceamount, "Enter Face amount");
				}
				else {
					ClickJSElement(initialmonthlyltcbenefit, "select initial monthly ltc benefit");
					EnterText(txtinitialmonthlyltcbenefit, inputmethod_initialmonthlyltcbenefit, "Enter initial monthly ltc benefit");
				}
				if((tableratingfirstinsured.equalsIgnoreCase("None"))&&(tableratingsecondinsured.equalsIgnoreCase("None")))
					ComboSelectValue(duration_accelerationBenefits, acceleration_Benefits_Duration, "select acceleration Benefits Duration");
				if(COB_Rider.equalsIgnoreCase("Yes")) {
					ComboSelectValue(cob_Duration, COB_duration, "select COB duration");
//					ComboSelectValue(cob_Inflation, COB_inflation, "select COB inflation");		
				}
				else
					ClickJSElement(cob_RiderCheckbox, "Unchecked COB Rider Checkbox");
				ComboSelectValue(aob_Inflation, AOB_inflation, "select AOB inflation");
				ComboSelectValue(inflation_duration, inflationduration, "select inflation duration");
				
				if(PremiumDropinRider.equalsIgnoreCase("Yes")) {
					ClickJSElement(premiumdropinrider_checkbox, "Select Premium drop in rider checkbox");
					EnterText(txt_premiumdropinrider, PremiumDropinRiderAmount, "Enter premium drop in rider value");
				}
				Thread.sleep(5);
				if(NonforfeitureRider.equalsIgnoreCase("Yes")) 
					ClickJSElement(nonforfeiture_checkbox , "Select nonforfeiture checkbox");
				
				
				}
			else if(productFundingOptions.equalsIgnoreCase("Single Premium Cash")) {
				 ClickJSElement(singlepremiumcash, "checkbox selected for single premium cash");
				 if((tableratingfirstinsured.equalsIgnoreCase("None"))&&(tableratingsecondinsured.equalsIgnoreCase("None"))) {
					 if(returnofpremium_cashfunding.equalsIgnoreCase("Yes")) {
						 ClickJSElement(returnofpremium, "select return of premium");
						 returnofpremium_illustration_flag = true;
					 }
				 }
				 if(inputmethod_recurringpremium.equalsIgnoreCase("Premium Amount")) {
						ClickJSElement(premiumamount, "select premium amount");
						EnterText(txtpremiumamount, inputmethod_premiumamount, "Enter premium amount");
					}
					else if(inputmethod_recurringpremium.equalsIgnoreCase("Face Amount")) {
						ClickJSElement(faceamount, "select Face amount");
						EnterText(txtfaceamount, inputmethod_faceamount, "Enter Face amount");
					}
					else {
						ClickJSElement(initialmonthlyltcbenefit, "select initial monthly ltc benefit");
						EnterText(txtinitialmonthlyltcbenefit, inputmethod_initialmonthlyltcbenefit, "Enter initial monthly ltc benefit");
					}
					if((tableratingfirstinsured.equalsIgnoreCase("None"))&&(tableratingsecondinsured.equalsIgnoreCase("None")&&(returnofpremium_illustration_flag==false)))
						ComboSelectValue(duration_accelerationBenefits, acceleration_Benefits_Duration, "select acceleration Benefits Duration");
					if(COB_Rider.equalsIgnoreCase("Yes")) {
						if(returnofpremium_illustration_flag==false) {
							ComboSelectValue(cob_Paymentoption, COB_paymentoption, "select COB payment option");
							ComboSelectValue(cob_Duration, COB_duration, "select COB duration");	
						}
					}
					else
						ClickJSElement(cob_RiderCheckbox, "Unchecked COB Rider Checkbox");
					ComboSelectValue(aob_Inflation, AOB_inflation, "select AOB inflation");
					ComboSelectValue(inflation_duration, inflationduration, "select inflation duration");
					Thread.sleep(5);
					if(NonforfeitureRider.equalsIgnoreCase("Yes")) 
						ClickJSElement(nonforfeiture_checkbox , "Select nonforfeiture checkbox");
					
				 }
			
		else {
				ClickJSElement(singlepremiumannuity, "checkbox selected for single premium annuity");
				annuitypremiumillustrationflag = true;
				if(taxqualification.equalsIgnoreCase("Qualified"))
					ClickJSElement(taxqualifiacation_qualified, "checkbox selected for tax qualifiacation as qualified");
				else
					ClickJSElement(taxqualifiacation_nonqualified, "checkbox selected for tax qualifiacation as non-qualified");
				if(inputmethod.equalsIgnoreCase("Annuity Premium Amount")) {
					ClickJSElement(inputmethod_annuitypremium, "checkbox selected for input method as annuity premium");
					if((Integer.parseInt(annuity_premium_amount))>(Integer.parseInt("81000")))
						EnterText(txt_annuitypremiumamout, annuity_premium_amount, "Enter annuity premium amount");
					else
						EnterText(txt_annuitypremiumamout, "81000", "Enter annuity premium amount");
				}
				else {
					ClickJSElement(inputmethod_lifepremium, "checkbox selected for input method as life premium");
					EnterText(txt_lifepremiumamount, life_premuim_amount, "Enter life premium amount");
					if(policyholderpayspremium.equalsIgnoreCase("Yes")) {
						ClickJSElement(policyholderpaypremiumdirectly, "checkbox selected for policy holder pay premium directly");
						EnterText(txt_startingyearthroughyear, startingyearthroughyear, "Enter starting year through year value");
					}
					
				}
				if((tableratingfirstinsured.equalsIgnoreCase("None"))&&(tableratingsecondinsured.equalsIgnoreCase("None")))
					ComboSelectValue(duration_accelerationBenefits, acceleration_Benefits_Duration, "select acceleration Benefits Duration");
				if(COB_Rider.equalsIgnoreCase("Yes")) {
					ComboSelectValue(cob_Duration, COB_duration, "select COB duration");
//					ComboSelectValue(cob_Inflation, COB_inflation, "select COB inflation");		
				}
				else
					ClickJSElement(cob_RiderCheckbox, "Unchecked COB Rider Checkbox");
				ComboSelectValue(aob_Inflation, AOB_inflation, "select AOB inflation");
				ComboSelectValue(inflation_duration, inflationduration, "select inflation duration");
				if(NonforfeitureRider.equalsIgnoreCase("Yes")) 
					ClickJSElement(nonforfeiture_checkbox , "Select nonforfeiture checkbox");
				
			}

			ComboSelectValue(yearstoquote_dropdown, yearstoquote, "select years to quote value");
			if(summarywithsign.equalsIgnoreCase("Yes"))
				ClickJSElement(summarywithsign_checkbox , "Select summary with sign checkbox");
			if(aboutOA.equalsIgnoreCase("Yes"))
				ClickJSElement(aboutOA_checkbox , "Select about OA checkbox");
			if(financialprofessionals.equalsIgnoreCase("Yes"))
				ClickJSElement(financialprofessionals_checkbox , "Select financial professionals checkbox");
			if(revisedQuote.equalsIgnoreCase("Yes"))
				ClickJSElement(revisedquote_checkbox , "Select revised quote checkbox");
			
			
			if(state.equalsIgnoreCase("Arkansas"))
				EnterText(license_no, "11111", "Enter License number");
			
			EnterText(txt_policy_no, policynumber, "Enter policy number");
			
			ClickElement(button_quote, "common ineligible impairments");
			wait.until(ExpectedConditions.numberOfWindowsToBe(2));
			Thread.sleep(20000);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ALT);
			r.keyPress(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_ALT);
			Thread.sleep(2000);		
			switchToDefault();
			takeScreenshot("Illustration_PolicyPage");
			ClickJSElement(tabApplication, "Application Tab");
			Thread.sleep(15000);
			return new ApplicationSetupAction();
		}	
		
	else
			return new ApplicationSetupAction();
		

}
	public void enterDOB(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		secondinsured_dob_month.sendKeys(strMonth);
		secondinsured_dob_year.sendKeys(strYear);
		secondinsured_dob_day.sendKeys(strDay);	
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}

}
