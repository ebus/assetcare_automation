package com.qa.pageActions;
import com.qa.pages.TemporaryInsuranceAgreementPage;
import com.relevantcodes.extentreports.LogStatus;


public class TemporaryInsuranceAgreementAction extends TemporaryInsuranceAgreementPage{
	
	public TemporaryInsuranceAgreementAction() {
		super();
	}
	
	//Actions
	public HIVConsentFPIAction enterDataTempInsAgreement() throws InterruptedException {
		
			extentTest.log(LogStatus.INFO, " - Temporary Insurance Agreement Page - ");
			switchToFrame(frameTemporaryInsuranceAgreement);
			ComboSelectValue(dropDownPaymentMethod, "Check", "PaymentMethod");
			ClickElement(rdoQuestionNo_1, "Question_1_No");
			takeScreenshot("TemporaryInsuranceAgreementPage");
			ClickJSElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(2000);
		
		return new HIVConsentFPIAction();
	}
}
