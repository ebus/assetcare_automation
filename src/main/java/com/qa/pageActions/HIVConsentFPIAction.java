package com.qa.pageActions;

import com.qa.pages.HIVConsentFPIPage;
import com.relevantcodes.extentreports.LogStatus;

public class HIVConsentFPIAction extends HIVConsentFPIPage{
	
	public HIVConsentFPIAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public HIVConsentSPIAction enterDataHIVFPIConsent(String illustration,String state) throws InterruptedException {
		String[] stateBucket = {"Georgia", "Alaska","Arkansas","Illinois","Maine","Maryland", "Minnesota", "Mississippi", "North Carolina", "North Dakota","South Carolina","Tennessee","Wisconsin","Wyoming"};
		boolean state_flag = false;
		for(String s : stateBucket) {
			if((state.equalsIgnoreCase(s))) {
				state_flag = true;
				break;
			}else {
				state_flag = false;
			}
		}		
			if(state_flag == false) {
				extentTest.log(LogStatus.INFO, " - HIV Consent FPI Page - ");
				switchToFrame(frameHIVConsentFPI);	
				if(state.equalsIgnoreCase("Michigan"))
					ClickJSElement(consenttobetestedforHIV, "Click on consent to be tested for HIV button");
				takeScreenshot("HIVConsentFPIPage");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);			
				return new HIVConsentSPIAction();
			}
			else
				return new HIVConsentSPIAction();
	}

}
