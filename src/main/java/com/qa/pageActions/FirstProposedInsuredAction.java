package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.qa.pages.FirstProposedInsuredPage;
import com.relevantcodes.extentreports.LogStatus;

public class FirstProposedInsuredAction extends FirstProposedInsuredPage{
	
	public FirstProposedInsuredAction() {
		super();
	}
	public static int insuredAge;
	public static boolean flagJuvenile;
	public static boolean flagowner;
	
	
	//Actions:
	public SecondProposedInsuredAction enterFirstProposedInsuredData(String illustration, String SSN, String willTheProposedInsuredmarriedorpartner, String birthCountry, String birthState, 
			String Street, String City, String ZipCode, String PhoneNumber, String Email,String earned, String unEarned, String netWorth, String isProposedInsuredUSCitizen,
			String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, String GCExpirationDate,String doesProposedInsuredHoldUSVisa, String typeOfVisa, 
			String visaExpirationDate,	String visaNumber, String provideDetails,String employerName,String occupation1,String dropdownOwneroflifepolicy,String dropdownPremiumpayor,String dropdownrelationshiptoowner) throws AWTException, InterruptedException {
		flagowner = false;
		String payor_other_Firstname = "Peter";
		String payor_other_Lastname = "Hu";
		String payor_other_Street = "10 Gordan Anderson";
		String payor_other_City = "Baltimore";
		String payor_other_State = "Indiana";
		String payor_other_Zip = "21212-2717";
		extentTest.log(LogStatus.INFO, " - First Proposed Insured Information Page - ");
		switchToFrame(frameFirstProposedInsured);
		EnterText(txtSSN, SSN, "SSN");
		selectYesNoRdoBtn(willTheProposedInsuredmarriedorpartner, "Is the Proposed Insured married or in a legally recognized civil union or domestic partnership?",rdoProposedInsuredmarriedorpartner_Yes, rdoProposedInsuredmarriedorpartner_No);		
		ComboSelectValue(dropdownCountryBirth, birthCountry,"Country of Birth");
		ComboSelectValue(dropdownBirthState, birthState,"Birth State");	
		isProInsUSCitizen(isProposedInsuredUSCitizen, countryofCitizenship, doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate,
				doesProposedInsuredHoldUSVisa, typeOfVisa, visaExpirationDate, visaNumber, provideDetails);	
		switchToDefault();
		switchToFrame(frameFirstProposedInsured);
		Thread.sleep(5000);
		EnterText(txtStreetAddress, Street, "StreetAddress");
		EnterText(txtCity, City, "City");
		EnterText(txtZip, ZipCode, "ZipCode");
		EnterText(email, Email, "Email");
		EnterText(txtPhoneNumber, PhoneNumber, "Phone Number");
		EnterText(employername, employerName, "employer name");
		EnterText(occupation, occupation1, "occupation");
		ComboSelectValue(dropdownOwnerofLifepolicy, dropdownOwneroflifepolicy,"dropdown Owner of Life policy");	
		if(dropdownOwneroflifepolicy.equalsIgnoreCase("other")) {
			flagowner = true;
		}
		ComboSelectValue(dropdownPremiumPayor, dropdownPremiumpayor,"dropdown Premium Payor");
		if(dropdownPremiumpayor.equalsIgnoreCase("Other")) {
			ComboSelectValue(dropdownRelationshiptoowner, dropdownrelationshiptoowner,"dropdown Relationship to owner");
			EnterText(payor_other_firstname, payor_other_Firstname, "payor other firstname");
			EnterText(payor_other_lastname, payor_other_Lastname, "payor other lastname");
			EnterText(payor_other_street, payor_other_Street, "payor other street");
			EnterText(payor_other_city, payor_other_City, "payor other city");
			ComboSelectValue(payor_other_state, payor_other_State, "payor other state");
			EnterText(payor_other_zip, payor_other_Zip, "payor other zip");
		}
		EnterText(txtEarned, earned, "Earned");
		EnterText(txtUnearned, unEarned, "UnEarned");
		EnterText(txtNetWorth, netWorth, "NetWorth");
		ClickJSElement(authorization, "authorization");

		Thread.sleep(3000);
		takeScreenshot("FirstProposedInsuredPage");
		ClickElement(btnNext, "Next Button");
		switchToDefault();
		Thread.sleep(3000);
		return new SecondProposedInsuredAction();
	}
	
	public void isProInsUSCitizen(String indicator, String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, 
			String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, String visaNumber, 
			String provideDetails) throws AWTException, InterruptedException
	{	
		selectYesNoRdoBtn(indicator,"Is Proposed Insured US Citizen ", rdoUSCitizen_Yes, rdoUSCitizen_No);
		if(indicator.equalsIgnoreCase("No")) {
			ClickElement(btnAddCitizenShipDetails, "Additional Citizenship Details");
			switchToDefault();
			switchToFrame(frameCitizenshipDetails);
			Thread.sleep(2000);
			ComboSelectValue(dropdownCountryCitizenship, countryofCitizenship,"Country of Citizenship");
			selectYesNoRdoBtn(doesProposedInsuredHoldGreenCard, "DoesProposedInsuredHoldGreenCard", rdoProposedInsGreenCard_Yes, rdoProposedInsGreenCard_No);
			if(doesProposedInsuredHoldGreenCard.equalsIgnoreCase("Yes")) {
				EnterText(txtGreenCardNumber, greenCardNumber, "GreenCard Number");
			}else {
				selectYesNoRdoBtn(doesProposedInsuredHoldUSVisa, "DoesProposedInsuredHoldUSVisa", rdoProposedInsHoldUSVisa_Yes, rdoProposedInsHoldUSVisa_No);
				if(doesProposedInsuredHoldUSVisa.equalsIgnoreCase("Yes")) {
					EnterText(txtTypeOfVisa, typeOfVisa, "Type Of Visa");
					EnterText(txtVISANumber, visaNumber, "Visa Number");
				}else {
					EnterText(txtProvideDetails, provideDetails, "Provide Details");
				}
			}
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
	}
}
