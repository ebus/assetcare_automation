package com.qa.pageActions;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

import com.qa.pages.HealthQuesPage;
import com.relevantcodes.extentreports.LogStatus;

public class HealthQuesAction extends HealthQuesPage{
	
	public HealthQuesAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public PersonalInfoAction enterDataHealthpage(String medical_application_medium) throws InterruptedException {
		
			if(!medical_application_medium.equalsIgnoreCase("teleinterview")) {
				extentTest.log(LogStatus.INFO, " - HIV Consent FPI Page - ");
				switchToFrame(frameHealthQuestion);	
				ComboSelectValue(height_ft_firstinsured, "5","height_ft");
				ComboSelectValue(height_in_firstinsured, "4","height_in");
				EnterText(weight_firstinsured, "65", "weight");
				if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
					ComboSelectValue(height_ft_secondinsured, "5","height_ft");
					ComboSelectValue(height_in_secondinsured, "4","height_in");
					EnterText(weight_secondinsured, "65", "weight");
				}
				
				ClickJSElement(rdoQuestionNo_1_no_firstinsured, "rdoQuestionNo_1 as no for first insured");
				ClickJSElement(rdoQuestionNo_2_no_firstinsured, "rdoQuestionNo_2 as no for first insured");
				ClickJSElement(rdoQuestionNo_3_no_firstinsured, "rdoQuestionNo_3 as no for first insured");
				ClickJSElement(rdoQuestionNo_4_no_firstinsured, "rdoQuestionNo_4 as no for first insured");
				ClickJSElement(rdoQuestionNo_5_no_firstinsured, "rdoQuestionNo_5 as no for first insured");
				ClickJSElement(rdoQuestionNo_6_no_firstinsured, "rdoQuestionNo_6 as no for first insured");
				ClickJSElement(rdoQuestionNo_7_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_8_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_9_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_10_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_11_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_12_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_13_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_14_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_15_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_16_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_17_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_18_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_19_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_20_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				ClickJSElement(rdoQuestionNo_21_no_firstinsured, "rdoQuestionNo_7 as no for first insured");
				
				if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
					ClickJSElement(rdoQuestionNo_1_no_secondinsured, "rdoQuestionNo_1 as no for second insured");
					ClickJSElement(rdoQuestionNo_2_no_secondinsured, "rdoQuestionNo_2 as no for second insured");
					ClickJSElement(rdoQuestionNo_3_no_secondinsured, "rdoQuestionNo_3 as no for second insured");
					ClickJSElement(rdoQuestionNo_4_no_secondinsured, "rdoQuestionNo_4 as no for second insured");
					ClickJSElement(rdoQuestionNo_5_no_secondinsured, "rdoQuestionNo_5 as no for second insured");
					ClickJSElement(rdoQuestionNo_6_no_secondinsured, "rdoQuestionNo_6 as no for second insured");
					ClickJSElement(rdoQuestionNo_7_no_secondinsured, "rdoQuestionNo_7 as no for second insured");
					ClickJSElement(rdoQuestionNo_8_no_secondinsured, "rdoQuestionNo_8 as no for second insured");
					ClickJSElement(rdoQuestionNo_9_no_secondinsured, "rdoQuestionNo_9 as no for second insured");
					ClickJSElement(rdoQuestionNo_10_no_secondinsured, "rdoQuestionNo_10 as no for second insured");
					ClickJSElement(rdoQuestionNo_11_no_secondinsured, "rdoQuestionNo_11 as no for second insured");
					ClickJSElement(rdoQuestionNo_12_no_secondinsured, "rdoQuestionNo_12 as no for second insured");
					ClickJSElement(rdoQuestionNo_13_no_secondinsured, "rdoQuestionNo_13 as no for second insured");
					ClickJSElement(rdoQuestionNo_14_no_secondinsured, "rdoQuestionNo_14 as no for second insured");
					ClickJSElement(rdoQuestionNo_15_no_secondinsured, "rdoQuestionNo_15 as no for second insured");
					ClickJSElement(rdoQuestionNo_16_no_secondinsured, "rdoQuestionNo_16 as no for second insured");
					ClickJSElement(rdoQuestionNo_17_no_secondinsured, "rdoQuestionNo_17 as no for second insured");
					ClickJSElement(rdoQuestionNo_18_no_secondinsured, "rdoQuestionNo_18 as no for second insured");
					ClickJSElement(rdoQuestionNo_19_no_secondinsured, "rdoQuestionNo_19 as no for second insured");
					ClickJSElement(rdoQuestionNo_21_no_secondinsured, "rdoQuestionNo_21 as no for second insured");
				}
				takeScreenshot("HealthQuesPage");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);			
				return new PersonalInfoAction();
			}
			else
				return new PersonalInfoAction();
	}

}
