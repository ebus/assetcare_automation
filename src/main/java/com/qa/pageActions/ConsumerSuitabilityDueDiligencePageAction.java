package com.qa.pageActions;

import static com.qa.pageActions.Illustration_PolicyPageAction.annuitypremiumillustrationflag;

import com.qa.pages.ConsumerSuitabilityDueDiligencePage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class ConsumerSuitabilityDueDiligencePageAction extends ConsumerSuitabilityDueDiligencePage{
	
	public ConsumerSuitabilityDueDiligencePageAction() {
		super();
	}
	
	Wait wait = new Wait();
	
	//Actions:
	public OtherInfoPageAction detailsOfConsumerSuitabilityDueDiligence(String state,String financialobject_preservationofcapital,String financialobject_income,String financialobject_longtermgrowth,String financialobject_shorttermgrowth,
			String financialobject_incomeandgrowth,	String financialobject_longtermcarebenefit,String financialobject_interestearning,String financialobject_taxdeferral,
			String intendedUse_income,String intendedUse_assetaccumulation,String intendedUse_protectionofPrincipal,String intendedUse_retirementneedsplanning,
			String intendedUse_estatepreservationplanning,String intendedUse_longtermcareexpress,String intendedUse_deathbenefit,String intendedUse_other,String financialstocks,String financialstocksyears,String financialbonds,String financialbondsyears,String financialmutualfund,String financialmutualfundyears,
			String financialvariableannuities,String financialvariableannuitiesyears,String financialfixedannuities,String financialfixedannuitiesyears,String financiallifeinsurance,String financiallifeinsuranceyears,
			String financialother,String financialotheryears,String riskexposure,String financialtimehorizon,String surrenderedannuitylast5yrs,String surrender_charge,String aware_annuitycontractcontainnonguaranteedelements,String accept_nonguaranteedelements,
			String sufficientliquidassetspaymedicalexpenses,String pay_surrenderchargeorpenalty,String Percentageofliquidnetworth,String percentagenetworthpurchaseannuity,String premiumpaidfromfundingsources,
			String surrendercharge_amount,String plantokeepproposedannuitycontract,String offer_fixedannuities,String offer_variableannuities,String offer_lifeinsurance,
			String offer_mutualFunds,String offer_stocks,String offer_certificateofdeposits,String authorizedtosell,String paidcashcompensationcommission,
			String paidcashcompensationfees,String paidcashcompensationother,String producerexercisedmaterialcontrol,String productFundingOptions,String typeofowner) throws InterruptedException {		
		
		String[] stateBucket = {"Arizona", "Arkansas","Florida","Michigan","Nebraska","Ohio", "Rhode Island"};
		boolean stateconsumer_flag = false;
		for(String s : stateBucket) {
			if((state.equalsIgnoreCase(s))) {
				stateconsumer_flag = true;
				break;
			}else {
				stateconsumer_flag = false;
			}
		}		
		if((!state.equalsIgnoreCase("Florida"))&&(annuitypremiumillustrationflag == true||productFundingOptions.equalsIgnoreCase("Annuity Funding Whole Life"))) {
			extentTest.log(LogStatus.INFO, " - Consumer Suitability Due Diligence Page - ");
			switchToFrame(frameConsumerSuitabilityDueDiligence);
			if(typeofowner.equalsIgnoreCase("Individual")||typeofowner.equalsIgnoreCase("Established Trust")||typeofowner.equalsIgnoreCase("Business")) 
				ComboSelectValue(maritalstatus,"Single","marital status");
			ClickElement(btnDetails, "Details Button");
			switchToDefault();
			switchToFrame(frameFinancialinfo);
			if(typeofowner.equalsIgnoreCase("Individual")||typeofowner.equalsIgnoreCase("Established Trust")||typeofowner.equalsIgnoreCase("Business")) {
				ComboSelectValue(networth,"$30,000 and under","networth");
				ComboSelectValue(annual_income,"$30,000 and under","annual income");
				ComboSelectValue(liquidnetworth,"$30,000 and under","liquid networth");
				ComboSelectValue(taxrate,"0 - 15%","taxrate");
				ComboSelectValue(annualexpenses,"$30,000 and under","annualexpenses");
				ClickJSElement(currentlyownstock, "Click on currently own stock");
			}

			if(financialobject_preservationofcapital.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_preservationofcapital, "Click on preservation of capital");
			if(financialobject_income.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_income, "Click on income");
			if(financialobject_longtermgrowth.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_longtermgrowth, "Click on long term growth");
			if(financialobject_shorttermgrowth.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_shorttermgrowth, "Click on short term growth");
			if(financialobject_incomeandgrowth.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_incomeandgrowth, "Click on income and growth");
			if(financialobject_longtermcarebenefit.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_longtermcarebenefit, "Click on long term care benefit");
			if(financialobject_interestearning.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_interestearning, "Click on interest earning");
			if(financialobject_taxdeferral.equalsIgnoreCase("Yes"))
				ClickJSElement(financialobj_taxdeferral, "Click on tax deferral");
			
			if(intendedUse_income.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_income, "Click on income");
			if(intendedUse_assetaccumulation.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_assetaccumulation, "Click on asset accumulation");
			if(intendedUse_protectionofPrincipal.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_protectionofPrincipal, "Click on protection of Principal");
			if(intendedUse_retirementneedsplanning.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_retirementneedsplanning, "Click on retirement needs planning");
			if(intendedUse_estatepreservationplanning.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_estatepreservationplanning, "Click on estate preservation planning");
			if(intendedUse_longtermcareexpress.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_longtermcareexpress, "Click on long term careexpress");
			if(intendedUse_deathbenefit.equalsIgnoreCase("Yes"))
				ClickJSElement(intendeduse_deathbenefit, "Click on death benefit");
			if(intendedUse_other.equalsIgnoreCase("Yes")) {
				ClickJSElement(intendeduse_other, "Click on other");
				EnterText(intendeduse_other_desc,"Description","Enter Other Desc");
			}
			
			if(financialstocks.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_stocks, "Click on stocks");
				EnterText(financial_stocks_years,financialstocksyears,"Enter financial stocks years");
				}
			if(financialbonds.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_bonds, "Click on bonds");
				EnterText(financial_bonds_years,financialbondsyears,"Enter financial bonds years");
			}
			if(financialmutualfund.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_mutualfund, "Click on mutual fund");
				EnterText(financial_mutualfund_years,financialmutualfundyears,"Enter financial mutualfund years");
			}
			if(financialvariableannuities.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_variableannuities, "Click on variable annuities");
				EnterText(financial_variableannuities_years,financialvariableannuitiesyears,"Enter financial variableannuities years");
			}
			if(financialfixedannuities.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_fixedannuities, "Click on fixed annuities");
				EnterText(financial_fixedannuities_years,financialfixedannuitiesyears,"Enter financial fixedannuities years");
			}
			if(financiallifeinsurance.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_lifeinsurance, "Click on life insurance");
				EnterText(financial_lifeinsurance_years,financiallifeinsuranceyears,"Enter financial lifeinsurance years");
			}
			if(financialother.equalsIgnoreCase("Yes")) {
				ClickJSElement(financial_other, "Click on other");
				EnterText(financial_other_years,financialotheryears,"Enter financial other years");
				EnterText(financial_other_desc,"Description","Enter Other Desc");
			}
			ComboSelectValue(dropdown_riskexposure, riskexposure,"Risk Exposure");
			ComboSelectValue(dropdown_FinancialTimeHorizon, financialtimehorizon,"Financial Time Horizon");
			if(financialtimehorizon.equalsIgnoreCase("Retirement Savings"))
				EnterText(txt_yearstoretirement,"12","years to retirement");
			if(stateconsumer_flag == true) {
				if(surrenderedannuitylast5yrs.equalsIgnoreCase("Yes")) {
					ClickJSElement(surrenderedannuity60mnths_yes, "Click on surrendered annuity in last 60mnths");
					if(surrender_charge.equalsIgnoreCase("Yes"))
						ClickJSElement(incursurrendercharge_yes, "Click on incur surrender charge as yes");
					else
						ClickJSElement(incursurrendercharge_no, "Click on incur surrender charge as no");
					
					EnterText(txt_surrendercharge_explanation,"explanation","Enter explanation");
					
					
				}
				else
					ClickJSElement(surrenderedannuity60mnths_no, "Click on not surrendered annuity in last 60mnths");
				if(aware_annuitycontractcontainnonguaranteedelements.equalsIgnoreCase("Yes"))
					ClickJSElement(proposedannuitycontainnonguarnteedele_yes, "Click on aware annuity contract contain non guaranteed elements");
				else
				{
					ClickJSElement(proposedannuitycontainnonguarnteedele_no, "Click on aware annuity contract contain non guaranteed elements as no");
					EnterText(explanation,"explanation","Enter explanation");
				}
				if(accept_nonguaranteedelements.equalsIgnoreCase("Yes"))
					ClickJSElement(acceptnonguaranteedelement_yes, "Click on accept non guaranteed element as yes");
				else
				{
					ClickJSElement(acceptnonguaranteedelement_no, "Click on accept non guaranteed element as no");
					EnterText(explanation,"explanation","Enter explanation");
				}
				Thread.sleep(2000);
				EnterText(explanation_possibilitytaxpenalties,"explanation","Enter explanation");
				EnterText(explanation_relationtootheravailableproduct,"explanation","Enter explanation");
				Thread.sleep(2000);
				if(sufficientliquidassetspaymedicalexpenses.equalsIgnoreCase("Yes"))
					ClickJSElement(sufficient_liquidassets_paymedicalexpenses_yes, "Click on sufficient liquid assets to pay medical expenses");
				else {
					ClickJSElement(sufficient_liquidassets_paymedicalexpenses_no, "Click on sufficient liquid assets to pay medical expenses as no");
					EnterText(sufficient_liquidassets_paymedicalexpenses_explanation,"explanation","Enter explanation");
				}
				if(pay_surrenderchargeorpenalty.equalsIgnoreCase("Yes"))
					ClickJSElement(pay_surrenderchargeorpenalty_yes, "Click on pay surrender charge or penalty as yes");
				else
					ClickJSElement(pay_surrenderchargeorpenalty_no, "Click on pay surrender charge or penalty as no");
				if(percentagenetworthpurchaseannuity.equalsIgnoreCase("1% - 30%"))
					ComboSelectValue(drpdwnpercentageliquidnetworthusedpurchaseannuity, percentagenetworthpurchaseannuity, "Enter percentage of net worth used to purchase annuity");
				else if(percentagenetworthpurchaseannuity.equalsIgnoreCase("0%")){
					ComboSelectValue(drpdwnpercentageliquidnetworthusedpurchaseannuity, percentagenetworthpurchaseannuity, "Enter percentage of net worth used to purchase annuity");
					EnterText(drpdwnpercentageliquidnetworthusedpurchaseannuity_explanation_rationale,"explanation","Enter explanation");
				}
				else{
					ComboSelectValue(drpdwnpercentageliquidnetworthusedpurchaseannuity, percentagenetworthpurchaseannuity, "Enter percentage of net worth used to purchase annuity");
					EnterText(drpdwnpercentageliquidnetworthusedpurchaseannuity_explanation,"explanation","Enter explanation");			
				}
			}
			else {
				if(surrenderedannuitylast5yrs.equalsIgnoreCase("Yes")) {
					ClickJSElement(surrenderedannuity60mnths_yes, "Click on surrendered annuity in last 60mnths");
					if(surrender_charge.equalsIgnoreCase("Yes"))
						ClickJSElement(incursurrendercharge_yes, "Click on incur surrender charge as yes");
					else
						ClickJSElement(incursurrendercharge_no, "Click on incur surrender charge as no");
					
					EnterText(txt_surrendercharge_explanation,"explanation","Enter explanation");
				}
				else
					ClickJSElement(surrenderedannuity60mnths_no, "Click on not surrendered annuity in last 60mnths");
				
				EnterText(explanation_possibilitytaxpenalties,"explanation","Enter explanation");
				EnterText(explanation_relationtootheravailableproduct,"explanation","Enter explanation");
				
				if(sufficientliquidassetspaymedicalexpenses.equalsIgnoreCase("Yes"))
					ClickJSElement(sufficient_liquidassets_paymedicalexpenses_yes, "Click on sufficient liquid assets to pay medical expenses");
				else {
					ClickJSElement(sufficient_liquidassets_paymedicalexpenses_no, "Click on sufficient liquid assets to pay medical expenses as no");
					EnterText(sufficient_liquidassets_paymedicalexpenses_explanation,"explanation","Enter explanation");
				}
				if(pay_surrenderchargeorpenalty.equalsIgnoreCase("Yes"))
					ClickJSElement(pay_surrenderchargeorpenalty_yes, "Click on pay surrender charge or penalty as yes");
				else
					ClickJSElement(pay_surrenderchargeorpenalty_no, "Click on pay surrender charge or penalty as no");
				EnterText(percentageliquidnetworth,Percentageofliquidnetworth,"Enter Percentage of liquid networth");
				if(Integer.parseInt(Percentageofliquidnetworth)>30) 
					EnterText(explanationetworthmovingintoannuity,"explanation","Enter explanation");	
			}
			Thread.sleep(2000);
			switchToDefault();
			scrollIntoView(btnSave, driver);
			ClickJSElement(btnSave, "SaveButton");
			Thread.sleep(5000);
			switchToFrame(frameConsumerSuitabilityDueDiligence);
			if(premiumpaidfromfundingsources.equalsIgnoreCase("Yes")) {
				ClickJSElement(premiumpaidfromfundingsources_yes, "Click on premium paid from funding sources as yes");
				EnterText(surrenderchargeamount,surrendercharge_amount,"Enter surrender charge amount");
				EnterText(suitablerecommendation,"suitable recommendation","Enter suitable recommendation");
				
			}
			else
				ClickJSElement(premiumpaidfromfundingsources_no, "Click on premium paid from funding sources as no");
			if(plantokeepproposedannuitycontract.equalsIgnoreCase("yes"))
				ClickJSElement(plantokeepproposedannuitycontract_yes, "Click on plan to keep proposed annuity contract as yes");
			else {
				ClickJSElement(plantokeepproposedannuitycontract_no, "Click on plan to keep proposed annuity contract as no");
				EnterText(plantokeepproposedannuitycontract_explanation,"Explanation","Enter plan to keep proposed annuity contract explanation");
			}
			if(offer_fixedannuities.equalsIgnoreCase("Yes"))
				ClickJSElement(offerfixedannuities, "Click on fixed annuities");
			if(offer_variableannuities.equalsIgnoreCase("Yes"))
				ClickJSElement(offervariableannuities, "Click on variable annuities");
			if(offer_lifeinsurance.equalsIgnoreCase("Yes"))
				ClickJSElement(offerlifeinsurance, "Click on life insurance");
			if(offer_mutualFunds.equalsIgnoreCase("Yes"))
				ClickJSElement(offermutualFunds, "Click on mutualFunds");
			if(offer_stocks.equalsIgnoreCase("Yes"))
				ClickJSElement(offerstocks, "Click on stocks");
			if(offer_certificateofdeposits.equalsIgnoreCase("Yes"))
				ClickJSElement(offercertificateofdeposits, "Click on certificate of deposits");
			if(authorizedtosell.equalsIgnoreCase("Annuities from Two or More Insurers although I primarily sell annuities from")) {
				ComboSelectValue(authorized_to_sell, authorizedtosell, "authorized to sell");
				EnterText(primarilysellannuitiesfrom,"ABC organization","Enter primarily sell annuities from value");	
			}
			else
				ComboSelectValue(authorized_to_sell, authorizedtosell, "authorized to sell");
			if(paidcashcompensationcommission.equalsIgnoreCase("Yes")) {
				ClickJSElement(paidcashcompensation_commission, "Click on paid cash compensation commission");
				EnterText(paidcashcompensation_commission_desc,"Description","Enter paid cash compensation commission desc");	
			}
			if(paidcashcompensationfees.equalsIgnoreCase("Yes")) 
				ClickJSElement(paidcashcompensation_fees, "Click on paid cash compensation fees");
			if(paidcashcompensationother.equalsIgnoreCase("Yes")) {
				ClickJSElement(paidcashcompensation_other, "Click on paid cash compensation other");
				EnterText(paidcashcompensation_other_desc,"Description","Enter paid cash compensation other desc");		
			}
			if(producerexercisedmaterialcontrol.equalsIgnoreCase("Yes")) {
				ClickJSElement(producerexercisedmaterialcontrol_yes, "Click on producer exercised material control as yes");
				EnterText(representativename,"Kim","Enter representative name");		
			}
			else
				ClickJSElement(producerexercisedmaterialcontrol_no, "Click on producer exercised material control as no");
			
			takeScreenshot("ConsumerSuitabilityDueDiligencePage");
			ClickElement(btnNext, "Next button");
			Thread.sleep(5000);
			switchToDefault();
			return new OtherInfoPageAction();
		}
		else
			return new OtherInfoPageAction();
	
}
}
