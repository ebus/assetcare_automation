package com.qa.pageActions;

import com.qa.pages.SuitabilityAgentPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;


public class SuitabilityAgentAction extends SuitabilityAgentPage{
	
	public SuitabilityAgentAction() {
		super();		
	}
	
	Wait wait = new Wait();
	
	//Actions 
	public ConsumerSuitabilityDueDiligencePageAction enterDataeSuitabilityAgent(String state,String illustration,String productFundingOptions,String productFundingOptions_illus) throws InterruptedException {
		
		if(state.equalsIgnoreCase("Florida")&&((illustration.equalsIgnoreCase("No")&&productFundingOptions.equalsIgnoreCase("Annuity Funding Whole Life"))||(illustration.equalsIgnoreCase("Yes")&&productFundingOptions_illus.equalsIgnoreCase("Single Premium Annuity")))) {
		
			extentTest.log(LogStatus.INFO, " - Suitability Agent Page - ");
			switchToFrame(frameSuitabilityAgent);
			
			EnterText(advantage, "advantage", "advantage");
			EnterText(disadvantage, "disadvantage", "Disadvantage");
			EnterText(recommendation, "recommendation", "recommendation");
			
			ClickJSElement(btnNext, "NextButton");
			switchToDefault();
			Thread.sleep(3000);
			return new ConsumerSuitabilityDueDiligencePageAction();
		}
		else
			return new ConsumerSuitabilityDueDiligencePageAction();
	}
}
