package com.qa.pageActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import com.qa.pages.CaseInformationPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class CaseInformationPageAction extends CaseInformationPage{
	
	public CaseInformationPageAction() {
		super();
	}
	
	Wait wait = new Wait();
	public static boolean ageflag;
	public static int age;
	//Actions:
	public ProposedInsuredInfoAction selectProduct(String firstName, String lastName, String dob, String gender, 
			String state, String productType, String product, String illustration) throws InterruptedException {
		ageflag = false;
		extentTest.log(LogStatus.INFO, " - Case Information Page - ");
		switchToFrame(frameCaseInformation);
		EnterText(txtFirstName, firstName, "FirstName");
		EnterText(txtLastName, lastName, "LastName");
		enterDOB(dob);
		Thread.sleep(2000);
		int age = getAge(dob);
        if(age <= 55) {
        	ageflag = true;
        }
		selectState(state);
		selectProductType(productType);
//		Thread.sleep(2000);
		wait.waitForElementToEnable(driver, btnFindAvailableProducts);
		ClickJSElement(btnFindAvailableProducts, "Find Available Products Button");
		if(SelectionIndAsYes(illustration, "Illustartion")) {
			clickOnIllustration(product);
		}else {
			
			clickOnSelect(product);
		}
			
		takeScreenshot("CaseInformationPage");
		Thread.sleep(15000);
		switchToDefault();
		return new ProposedInsuredInfoAction();
	}
	
	public Illustration_PolicyPageAction selectProductIllustration(String firstName, String lastName, String dob, String gender, 
			String state, String productType, String product, String illustration) throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - Case Information Page - ");
		switchToFrame(frameCaseInformation);
		EnterText(txtFirstName, firstName, "FirstName");
		EnterText(txtLastName, lastName, "LastName");
		enterDOB(dob);
		Thread.sleep(2000);
		age = getAge(dob);
        if(age <= 55) {
        	ageflag = true;
        }
        ClickElement(dropDownGender,"Clicking on dropDownGender");
        List<WebElement> genderlist =driver.findElements(By.xpath("//button[@data-id='ddlGender']/following-sibling::div/ul/li/a"));
		for(int i=0;i<genderlist.size();i++) {
			if(genderlist.get(i).getText().equalsIgnoreCase(gender))
				genderlist.get(i).click();
		}
        selectState(state);
		selectProductType(productType);
		wait.waitForElementToEnable(driver, btnFindAvailableProducts);
		ClickJSElement(btnFindAvailableProducts, "Find Available Products Button");
		if(SelectionIndAsYes(illustration, "Illustartion")) {
			clickOnIllustration(product);
		}else {
			clickOnSelect(product);
		}
			
		takeScreenshot("CaseInformationPage");
		Thread.sleep(15000);
		switchToDefault();
		return new Illustration_PolicyPageAction();
	}
	
	public void selectState(String state)
	{
         driver.findElement(By.xpath("//div[@id='UpdatePanel1']/div/button/span[@class='filter-option pull-left']")).click();
         WebElement ddlState = driver.findElement(By.xpath("//span[text()=" +  "'" + state + "'" + "]"));
         ClickJSElement(ddlState, "State: "+state);         
	}
	
	public void selectProductType(String productType) throws InterruptedException
	{	
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='UpdatePanel2']/div/button/span[@class='filter-option pull-left']")).click();
		WebElement ddlProductType = driver.findElement(By.xpath("//span[text()=" +  "'" + productType + "'" + "]"));
        ClickJSElement(ddlProductType, "Product Type: "+productType);  
	}
	
	public void enterDOB(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		txtMonth.sendKeys(strMonth);
		txtYear.sendKeys(strYear);
		txtDay.sendKeys(strDay);
		
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}
	
	public void clickOnSelect(String product) {
		WebElement element = driver.findElement(By.xpath("//div[@id='divAvailableProductsGrid1']/div/table/tbody/tr[td[3][contains(text(),'"+product+"')]]/td[5]/input"));
		ClickJSElement(element, "Select button under iGO e-App for "+product);		
	}
	
	public void clickOnIllustration(String product) {
		WebElement element = driver.findElement(By.xpath("//div[@id='divAvailableProductsGrid1']/div/table/tbody/tr[td[3][contains(text(),'"+product+"')]]/td[4]/input"));
		ClickJSElement(element, "Select button under Illustrations for "+product);		
	}
	
	public static int getAge(String dob) {
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];		
		String strYear = dateOfBirth[2];
		LocalDate birthDate = LocalDate.of(Integer.parseInt(strYear), Integer.parseInt(strMonth), Integer.parseInt(strDay));
        LocalDate curDate = LocalDate.now();
        return Period.between(birthDate, curDate).getYears();
    }
	
}
