package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.LeaveBehindFormsPage;
import com.relevantcodes.extentreports.LogStatus;

public class LeaveBehindFormsPageAction extends LeaveBehindFormsPage{
	
	public LeaveBehindFormsPageAction() {
		super();
	}
	
	//Actions:
	public SignatureMethodAction validateleaveBehindForms() throws InterruptedException, AWTException {
		extentTest.log(LogStatus.INFO, " - Leave Behind Forms Page - ");
		switchToFrame(frameLeaveBehindForms);	
//		ClickJSElement(btnviewforms, "Click on view forms Button");
//		Thread.sleep(30000);
//		Robot r = new Robot();
//		r.keyPress(KeyEvent.VK_ALT);
//		r.keyPress(KeyEvent.VK_F4);
//		r.keyRelease(KeyEvent.VK_F4);
//		r.keyRelease(KeyEvent.VK_ALT);
		takeScreenshot("PartIIMedicalInterviewPage");
		ClickJSElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(5000);
		return new SignatureMethodAction();
	}
}
