package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.qa.pages.SecondProposedInsuredPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;
public class SecondProposedInsuredAction extends SecondProposedInsuredPage{
	
	public SecondProposedInsuredAction() {
		super();
	}
	public static int insuredAge;
	public static boolean flagJuvenile;
	
	//Actions:
	public PolicyorPremiumInformationAction enterSecondProposedInsuredData(String illustration,String secondProposedInsuredFirstname,String secondProposedInsuredLastname,String secondProposedInsuredgender,String dob,String secondProposedInsuredssn,String willTheProposedInsuredmarriedorpartner,String secondProposedInsuredbirthCountry,String secondProposedInsuredbirthState,String issecondProposedInsuredUSCitizen,String relationshiptoFirstinsured
			,String sameaddressasfirstproposedinsured,String secondProposedInsuredStreet,String secondProposedInsuredCity,String secondProposedInsuredZip,String secondProposedInsuredemail,String secondProposedInsuredphonenumber,String employerName,String occupation1,String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, String GCExpirationDate,
			String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate,	String visaNumber, String provideDetails) throws AWTException, InterruptedException {
		
		if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
			
			extentTest.log(LogStatus.INFO, " - Second Proposed Insured Information Page - ");
			switchToFrame(frameSecondProposedInsured);
			Thread.sleep(2000);
			if(illustration.equalsIgnoreCase("No")) {
				EnterText(SecondProposedInsuredfirstname, secondProposedInsuredFirstname, "Second Proposed Insured firstname");
				EnterText(SecondProposedInsuredlastname, secondProposedInsuredLastname, "Second Proposed Insured lastname");
			}
			EnterText(SecondProposedInsuredSSN, secondProposedInsuredssn, "Second Proposed Insured SSN");
			if(illustration.equalsIgnoreCase("No")) {
				selectGender(secondProposedInsuredgender, rdoGender_Male, rdoGender_Female);
				enterDOB(dob);
			}
			selectYesNoRdoBtn(willTheProposedInsuredmarriedorpartner, "Is the Proposed Insured married or in a legally recognized civil union or domestic partnership?",rdoProposedInsuredmarriedorpartner_Yes, rdoProposedInsuredmarriedorpartner_No);		
					
			ComboSelectValue(dropdownCountryBirth, secondProposedInsuredbirthCountry,"Country of Birth");
			ComboSelectValue(dropdownBirthState, secondProposedInsuredbirthState,"Birth State");	
			isProInsUSCitizen(issecondProposedInsuredUSCitizen, countryofCitizenship, doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate,
					doesProposedInsuredHoldUSVisa, typeOfVisa, visaExpirationDate, visaNumber, provideDetails);	
			switchToDefault();
			switchToFrame(frameSecondProposedInsured);
			Thread.sleep(3000);
			ComboSelectValue(relationshiptofirstinsured, relationshiptoFirstinsured,"relationship to first insured");
			if(sameaddressasfirstproposedinsured.equalsIgnoreCase("No")) {
				EnterText(txtStreetAddress, secondProposedInsuredStreet, "StreetAddress");
				EnterText(txtCity, secondProposedInsuredCity, "City");
				ComboSelectValue(drpdwnstate, secondProposedInsuredbirthState, "State");
				EnterText(txtZip, secondProposedInsuredZip, "ZipCode");
			}
			else
				ClickJSElement(checkboxforsameaddress, "Click on checkbox for same address");
			EnterText(email, secondProposedInsuredemail, "Email");
			EnterText(txtPhoneNumber, secondProposedInsuredphonenumber, "Phone Number");
			EnterText(employername, employerName, "employer name");
			EnterText(occupation, occupation1, "occupation");
	
			Thread.sleep(3000);
			takeScreenshot("FirstProposedInsuredPage");
			ClickElement(btnNext, "Next Button");
			switchToDefault();
			Thread.sleep(3000);
			return new PolicyorPremiumInformationAction();
			}
		else
			return new PolicyorPremiumInformationAction();
	}
	
	public void enterDOB(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		SecondProposedInsureddobmonth.sendKeys(strMonth);
		SecondProposedInsureddobyear.sendKeys(strYear);
		SecondProposedInsureddobday.sendKeys(strDay);	
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}
	
	public void isProInsUSCitizen(String indicator, String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, 
			String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, String visaNumber, 
			String provideDetails) throws AWTException, InterruptedException
	{	
		selectYesNoRdoBtn(indicator,"Is Proposed Insured US Citizen ", rdoUSCitizen_Yes, rdoUSCitizen_No);
		if(indicator.equalsIgnoreCase("No")) {
			ClickElement(btnAddCitizenShipDetails, "Additional Citizenship Details");
			switchToDefault();
			switchToFrame(frameCitizenshipDetails);
			Thread.sleep(2000);
			ComboSelectValue(dropdownCountryCitizenship, countryofCitizenship,"Country of Citizenship");
			selectYesNoRdoBtn(doesProposedInsuredHoldGreenCard, "DoesProposedInsuredHoldGreenCard", rdoProposedInsGreenCard_Yes, rdoProposedInsGreenCard_No);
			if(doesProposedInsuredHoldGreenCard.equalsIgnoreCase("Yes")) {
				EnterText(txtGreenCardNumber, greenCardNumber, "GreenCard Number");
			}else {
				selectYesNoRdoBtn(doesProposedInsuredHoldUSVisa, "DoesProposedInsuredHoldUSVisa", rdoProposedInsHoldUSVisa_Yes, rdoProposedInsHoldUSVisa_No);
				if(doesProposedInsuredHoldUSVisa.equalsIgnoreCase("Yes")) {
					EnterText(txtTypeOfVisa, typeOfVisa, "Type Of Visa");
					EnterText(txtVISANumber, visaNumber, "Visa Number");
				}else {
					EnterText(txtProvideDetails, provideDetails, "Provide Details");
				}
			}
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
	}
}
