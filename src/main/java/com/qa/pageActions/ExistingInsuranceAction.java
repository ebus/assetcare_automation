package com.qa.pageActions;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

import java.awt.AWTException;


import com.qa.pages.ExistingInsurancePage;
import com.relevantcodes.extentreports.LogStatus;


public class ExistingInsuranceAction extends ExistingInsurancePage{
	
	public ExistingInsuranceAction() {
		super();
	}
	
	public PersonalWorkbookAction enterDataExistingInsurance(String state,String insuring_company_name,String policynumber,String typeofinsurance,String cashvalue,String amountbenefit,String yearissued,String beingreplaced,String exchange1035) throws InterruptedException, AWTException{

//		String insuring_company_name = "American United Life"; // American United Life,Pioneer Mutual Life,State Life,Other
//		String policynumber = "12345678";
//		String typeofinsurance = "Annuity"; //Annuity, Life Insurance, Long Term Care
//		String cashvalue = "100";
//		String amountbenefit = "100";
//		String yearissued = "2011";
//		String beingreplaced = "Yes";
//		String exchange1035 = "Yes";
		extentTest.log(LogStatus.INFO, " - Existing Insurance Page - ");
		switchToFrame(frameExistingInsurance);
		ClickJSElement(rdoQuestionNo_1_no, "Click on rdo Question No_1 as no");
		ClickJSElement(rdoQuestionNo_2_no, "Click on rdo Question No_2 as no");
		if(state.equalsIgnoreCase("Florida")) {
			EnterText(otherhealthinsurancepolicies, "NA", "Enter other health insurance policies");
			EnterText(Listpoliciessold, "NA", "Enter List of policies sold");
			EnterText(Listpoliciessoldlast5yrs, "NA", "Enter List of policies sold last 5yrs");
			ClickJSElement(ProposedInsuredreceiveComparativeInfo_no, "Click on Proposed Insured receive Comparative Info as no");
			
		}
			
		ClickJSElement(rdoQuestionNo_3_no, "Click on rdo Question No_3 as no");
		ClickJSElement(rdoQuestionNo_4_yes, "Click on rdo Question No_4 as yes");
		ClickJSElement(rdoQuestionNo_5_no, "Click on rdo Question No_5 as no");
		ClickJSElement(rdoQuestionNo_6_no, "Click on rdo Question No_6 as no");
		ClickJSElement(rdoQuestionNo_7_no, "Click on rdo Question No_7 as no");

		ClickJSElement(addbutton_firstproposedinsured, "Click on add button for firstproposedinsured");
		switchToDefault();
		switchToFrame(framelongtermexistinginsurance_firstproposedinsured);
		
		Thread.sleep(2000);
		ComboSelectValue(insuringcompanyname_firstproposedinsured, insuring_company_name, "select insuring company name");
		if(insuring_company_name.equalsIgnoreCase("other"))
			EnterText(companyname_firstproposedinsured, "ABC org", "Enter company name");
		EnterText(policynumber_firstproposedinsured, policynumber, "Enter policy number");
		ComboSelectValue(typeofinsurance_firstproposedinsured, typeofinsurance, "select type of insurance");
		if(typeofinsurance.equalsIgnoreCase("Long Term Care"))
		{
			EnterText(amountofbenefit_firstproposedinsured, amountbenefit, "Enter amount of benefit");
			EnterText(yearissued_firstproposedinsured, yearissued, "Enter year issued");
			if(beingreplaced.equalsIgnoreCase("Yes"))
			{
				ClickJSElement(beingreplacedorchanged_yes_firstproposedinsured, "Click on being replaced as yes");
				EnterText(typeofoptionalbenefit_firstproposedinsured, "Optional Benefit", "Enter type of optional benefit");
				ClickJSElement(financing, "Click on financing");
			}
			else
				ClickJSElement(beingreplacedorchanged_no_firstproposedinsured, "Click on being replaced as no");
		}
		else
		{
			EnterText(cashvalue_firstproposedinsured, cashvalue, "Enter cash value");
			EnterText(yearissued_firstproposedinsured, yearissued, "Enter year issued");
			if(beingreplaced.equalsIgnoreCase("Yes"))
			{
				ClickJSElement(beingreplacedorchanged_yes_firstproposedinsured, "Click on being replaced as yes");
				if(exchange1035.equalsIgnoreCase("yes"))
				{
					ClickJSElement(exchange1035_yes_firstproposedinsured, "Click on 1035 exchange as yes");
					EnterText(typeofoptionalbenefit_firstproposedinsured, "Optional Benefit", "Enter type of optional benefit");
					ClickJSElement(financing, "Click on financing");
					ClickJSElement(agreementacceptance, "Click on agreement acceptance");
					EnterText(liquidate$, "1000", "Enter liquidate in $");
					ClickJSElement(policyrequiredforfullsurrender, "Click on policy required for full surrender");
					
				}
				else {
					ClickJSElement(exchange1035_no_firstproposedinsured, "Click on 1035 exchange as no");
					EnterText(typeofoptionalbenefit_firstproposedinsured, "Optional Benefit", "Enter type of optional benefit");
					ClickJSElement(financing, "Click on financing");
				}
				}
			else
				ClickJSElement(beingreplacedorchanged_no_firstproposedinsured, "Click on being replaced as no");
			
		}
		switchToDefault();
		scrollIntoView(savebtn_firstinsured, driver);
		ClickJSElement(savebtn_firstinsured, "SaveButton");
		Thread.sleep(5000);
		switchToFrame(frameExistingInsurance);
		
		if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
			ClickJSElement(addbutton_secondproposedinsured, "Click on add button for second proposed insured");
			switchToDefault();
			switchToFrame(framelongtermexistinginsurance_secondproposedinsured);
			
			Thread.sleep(2000);
			ComboSelectValue(insuringcompanyname_secondproposedinsured, insuring_company_name, "select insuring company name");
			if(insuring_company_name.equalsIgnoreCase("other"))
				EnterText(companyname_secondproposedinsured, "ABC org", "Enter company name");
			EnterText(policynumber_secondproposedinsured, policynumber, "Enter policy number");
			ComboSelectValue(typeofinsurance_secondproposedinsured, typeofinsurance, "select type of insurance");
			if(typeofinsurance.equalsIgnoreCase("Long Term Care"))
			{
				EnterText(amountofbenefit_secondproposedinsured, amountbenefit, "Enter amount of benefit");
				EnterText(yearissued_secondproposedinsured, yearissued, "Enter year issued");
				if(beingreplaced.equalsIgnoreCase("Yes"))
				{
					ClickJSElement(beingreplacedorchanged_yes_secondproposedinsured, "Click on being replaced as yes");
					EnterText(typeofoptionalbenefit_secondproposedinsured, "Optional Benefit", "Enter type of optional benefit");
					ClickJSElement(financing, "Click on financing");
				}
				else
					ClickJSElement(beingreplacedorchanged_no_secondproposedinsured, "Click on being replaced as no");
			}
			else
			{
				EnterText(cashvalue_secondproposedinsured, cashvalue, "Enter cash value");
				EnterText(yearissued_secondproposedinsured, yearissued, "Enter year issued");
				if(beingreplaced.equalsIgnoreCase("Yes"))
				{
					ClickJSElement(beingreplacedorchanged_yes_secondproposedinsured, "Click on being replaced as yes");
					if(exchange1035.equalsIgnoreCase("yes"))
					{
						ClickJSElement(exchange1035_yes_secondproposedinsured, "Click on 1035 exchange as yes");
						EnterText(typeofoptionalbenefit_secondproposedinsured, "Optional Benefit", "Enter type of optional benefit");
						ClickJSElement(financing, "Click on financing");
						ClickJSElement(agreementacceptance, "Click on agreement acceptance");
						EnterText(liquidate$, "1000", "Enter liquidate in $");
						ClickJSElement(policyrequiredforfullsurrender, "Click on policy required for full surrender");
						
					}
					else {
						ClickJSElement(exchange1035_no_secondproposedinsured, "Click on 1035 exchange as no");
						EnterText(typeofoptionalbenefit_secondproposedinsured, "Optional Benefit", "Enter type of optional benefit");
						ClickJSElement(financing, "Click on financing");
					}
					}
				else
					ClickJSElement(beingreplacedorchanged_no_secondproposedinsured, "Click on being replaced as no");
				
			}
			switchToDefault();
			scrollIntoView(savebtn_secondinsured, driver);
			ClickJSElement(savebtn_secondinsured, "SaveButton");
			Thread.sleep(5000);
			switchToFrame(frameExistingInsurance);
		}
		ClickJSElement(rdoQuestionNo_8_no, "Click on rdo Question No_8 as no");
		takeScreenshot("ExistingInsurancePage");
		ClickJSElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(5000);
		return new PersonalWorkbookAction();
	}
}
