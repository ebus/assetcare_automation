package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.ProposedInsuredInformationPage;
import com.relevantcodes.extentreports.LogStatus;

public class ProposedInsuredInfoAction extends ProposedInsuredInformationPage{
	
	public ProposedInsuredInfoAction() {
		super();
	}
	public static int insuredAge;
	public static boolean flagJuvenile;
	
	//Actions:
	public OwnerInformationPageAction enterData(String illustration, String state,  
			String middleName, String SSN, String gender, String birthCountry, String birthState, 
			String maritalQuestion,	String Street, String City, String ZipCode, String County, String yearsAtAddress, 
			String PhoneNumber, String Email, String willTheProposedInsuredBeOwner, String whoWillBePayor,String thirdPartyflag,String applicationSignedBy,
			String indLegalGuardianPolicyOwner, String firstName_LG, String lastName_LG, String SSN_LG, String DoesInsuredHaveDL, String DLNo, 
			String IssueState, String ExpirationDate, String earned, String unEarned, String netWorth, String isProposedInsuredUSCitizen,
			String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, String GCExpirationDate,
			String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate,	String visaNumber, String provideDetails) throws AWTException, InterruptedException {

		extentTest.log(LogStatus.INFO, " - Proposed Insured Information Page - ");
		switchToFrame(frameProposedInsuredInformation);
//		String rdo3rdPartyflag = "Yes";
		EnterText(txtMiddleName, middleName, "Middle Name");
		EnterText(txtSSN, SSN, "SSN");
		selectGender(gender, rdoGender_Male, rdoGender_Female);
		ComboSelectValue(dropdownCountryBirth, birthCountry,"Country of Birth");
		ComboSelectValue(dropdownBirthState, birthState,"Birth State");	
		maritalStatusReusable(maritalQuestion,rdoMaritalStatus_Married, rdoMaritalStatus_NotMarried);
		String age = txtAge.getAttribute("value");
		insuredAge=Integer.parseInt(age);
		EnterText(txtStreetAddress, Street, "StreetAddress");
		EnterText(txtCity, City, "City");
		EnterText(txtZip, ZipCode, "ZipCode");
		EnterText(txtCounty, County, "County");
		EnterText(txtYearsAddress, yearsAtAddress, "Years At Address");
		EnterText(txtPhoneNumber, PhoneNumber, "Phone Number");
		EnterText(email, Email, "Email");
		selectYesNoRdoBtn(willTheProposedInsuredBeOwner, "The Proposed Insured Be Owner", rdoProposedInsuredOwner_Yes, rdoProposedInsuredOwner_No);
		ComboSelectValue(dropdownPayor, whoWillBePayor,"Payor");
		selectYesNoRdoBtn(DoesInsuredHaveDL,"Driving License", rdoDriverLicense_Yes, rdoDriverLicense_No);
		driverLicenseDetails(DoesInsuredHaveDL, DLNo, IssueState, ExpirationDate);
		EnterText(txtEarned, earned, "Earned");
		EnterText(txtUnearned, unEarned, "UnEarned");
		EnterText(txtNetWorth, netWorth, "NetWorth");
		isProInsUSCitizen(isProposedInsuredUSCitizen, countryofCitizenship, doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate,
				doesProposedInsuredHoldUSVisa, typeOfVisa, visaExpirationDate, visaNumber, provideDetails);	
		switchToDefault();
		switchToFrame(frameProposedInsuredInformation);
		Thread.sleep(5000);
		ComboSelectValue(dropdownEmploymentStatus, "Retired","EmploymentStatus");	
		if(willTheProposedInsuredBeOwner.equalsIgnoreCase("Yes")) {
			ClickElement(btnW9Sub_Cert_Yes, "W-9 Substitute Certification Yes button");
		}
//		if(state.equalsIgnoreCase("Florida") || state.equalsIgnoreCase("North Carolina")|| state.equalsIgnoreCase("Vermont") || state.equalsIgnoreCase("Maine")) {
		if(thirdPartyflag.equalsIgnoreCase("Yes")){
			ClickElement(rdo3rdParty_Yes, "3rdParty_Yes");
			EnterText(txt3rdParty_Name, "TestNAae", "3rdParty_Name");
			EnterText(txt3rdParty_Address, "Address", "3rdParty_Address");
			EnterText(txt3rdParty_City, "City", "City");
			ComboSelectValue(dropdown3rdParty_State, "Alaska", "dropdown3rdParty_State");
			EnterText(txt3rdParty_ZIP, "000124520", "3rdParty_ZIP");
		}
		else
			ClickElement(rdo3rdParty_No, "3rdParty_No");
		Thread.sleep(5000);
		takeScreenshot("ProposedInsuredPage");
		ClickElement(btnNext, "Next Button");
		switchToDefault();
		Thread.sleep(5000);
		return new OwnerInformationPageAction();
	}
	
	public void driverLicenseDetails(String indicator_DL, String DLNo, String IssueState, String ExpirationDate) {
		if(indicator_DL.equalsIgnoreCase("Yes"))			
		{
			EnterText(txtDriverLicenseNo, DLNo, "DriverLicenseNo");
			ComboSelectValue(dropdownIssueState, IssueState, "Issue State");
			DLexpirationDate(ExpirationDate);
		}	
	}
	
	public void DLexpirationDate(String date)
	{	
		String[] dateDemo = date.split("/");
		String strMonth = dateDemo[0];
		String strDay = dateDemo[1];
		String strYear = dateDemo[2];
		txtMonth.sendKeys(strMonth);	
		txtYear.sendKeys(strYear);
		txtDay.sendKeys(strDay);
		
		extentTest.log(LogStatus.INFO, date + " is entered in Driver License Expiration Date");	
	}
	
	public void isProInsUSCitizen(String indicator, String countryofCitizenship, String doesProposedInsuredHoldGreenCard, String greenCardNumber, 
			String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, String visaNumber, 
			String provideDetails) throws AWTException, InterruptedException
	{	
		selectYesNoRdoBtn(indicator,"Is Proposed Insured US Citizen ", rdoUSCitizen_Yes, rdoUSCitizen_No);
		if(indicator.equalsIgnoreCase("No")) {
			ClickElement(btnAddCitizenShipDetails, "Additional Citizenship Details");
			switchToDefault();
			switchToFrame(frameCitizenshipDetails);
			Thread.sleep(2000);
			ComboSelectValue(dropdownCountryCitizenship, countryofCitizenship,"Country of Citizenship");
			selectYesNoRdoBtn(doesProposedInsuredHoldGreenCard, "DoesProposedInsuredHoldGreenCard", rdoProposedInsGreenCard_Yes, rdoProposedInsGreenCard_No);
			if(doesProposedInsuredHoldGreenCard.equalsIgnoreCase("Yes")) {
				EnterText(txtGreenCardNumber, greenCardNumber, "GreenCard Number");
			}else {
				selectYesNoRdoBtn(doesProposedInsuredHoldUSVisa, "DoesProposedInsuredHoldUSVisa", rdoProposedInsHoldUSVisa_Yes, rdoProposedInsHoldUSVisa_No);
				if(doesProposedInsuredHoldUSVisa.equalsIgnoreCase("Yes")) {
					EnterText(txtTypeOfVisa, typeOfVisa, "Type Of Visa");
					EnterText(txtVISANumber, visaNumber, "Visa Number");
				}else {
					EnterText(txtProvideDetails, provideDetails, "Provide Details");
				}
			}
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		}
	}
}
