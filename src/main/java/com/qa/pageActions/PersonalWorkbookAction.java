package com.qa.pageActions;

import java.awt.AWTException;
import com.qa.pages.PersonalWorksheetPage;
import com.relevantcodes.extentreports.LogStatus;


public class PersonalWorkbookAction extends PersonalWorksheetPage {
	
	public PersonalWorkbookAction() {
		super();
	}
	
	//Actions
	public RequestofFundsPageAction enterDataPersonalWorkbook(String state,String premium_permonth,String premium_permonth_amount,String premium_peryear,String premium_peryear_amount,String onetime_single_premium_amount ,String current_employment_income,
			String current_investment_income,String saving,String sell_investments, String other_current_income,String sell_assets,String moneyfromfamily,String others,
			String otherfund,String ques_affordto_keep_policy,String household_annual_income,String ques_incometochangeovernxt10yrs,String ques_plan_pay_premiums_from_income,String buy_inflation_protection,String differences_paymentsource_income,String differences_paymentsource_savings,String differences_paymentsource_investments,String differences_paymentsource_sell_assets,
			String differences_paymentsource_fromfamily,String differences_paymentsource_other,String paydiff_fromotherfund,String No_of_days_elimination_prod,String Approx_cost_period_of_care,
			String Payment_plan_for_care,String assets_worth,String assets_change_over_10years,String disclosure_statement) throws InterruptedException, AWTException {
		
			
			extentTest.log(LogStatus.INFO, " - Personal Information Page - ");
			switchToFrame(framePersonalWorksheet);
			Thread.sleep(1000);
			if(premium_permonth.equalsIgnoreCase("Yes"))
				EnterText(permonthpremium, premium_permonth_amount, "Enter per month premium amount");
			
			else if(premium_peryear.equalsIgnoreCase("Yes"))
				EnterText(peryearpremium, premium_peryear_amount, "Enter peryear premium amount");
			
			else
				EnterText(onetime_single_premium, onetime_single_premium_amount, "Enter onetime single premium amount");
			
			if(state.equalsIgnoreCase("Florida")) {
				if(current_employment_income.equalsIgnoreCase("Yes"))
					ClickJSElement(curr_employment_income_florida, "Click on current employment income checkbox");
				if(saving.equalsIgnoreCase("Yes"))
					ClickJSElement(savings_florida, "Click on savings checkbox");
				if(other_current_income.equalsIgnoreCase("Yes"))
					ClickJSElement(fmilywillpay, "Click on fmily will pay");
				ComboSelectValue(household_annualincome, "$10-20,000", "annual income");
				ComboSelectValue(incometochangeovernxt10yrs, "Increase", "ques related to income change over nxt 10yrs");
				if(buy_inflation_protection.equalsIgnoreCase("No")) {
					ClickJSElement(inflation_protection_no, "Click on inflation protection as no");
					if(differences_paymentsource_income.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_income_florida, "Click on pay differences from income");
					
					if(differences_paymentsource_savings.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_savings_florida, "Click on pay differences from savings");
					
					if(differences_paymentsource_fromfamily.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_familymoney_florida, "Click on pay differences from family money checkbox");	
					
				}
				else
					ClickJSElement(inflation_protection_yes, "Click on inflation protection as yes");
				
				EnterText(elimination_period_days, No_of_days_elimination_prod, "elimination period days");
				EnterText(elimination_period_cost, Approx_cost_period_of_care , "Approx cost of period of care ");
				ComboSelectValue(elimination_period_plantopay_your_care, Payment_plan_for_care , "elimination_period_plantopay_your_care");
				ComboSelectValue(assets, "$20,000-30,000", "assets worth value");
				ComboSelectValue(assetstochangeovernext10years, "Increase", "assets to change over nxt 10yrs");	
				if(disclosure_statement.equalsIgnoreCase("Describe financial situation"))
					ClickJSElement(disclosure_Statement1, "Click on disclosure Statement1");
				else
					ClickJSElement(disclosure_Statement2, "Click on disclosure Statement2");
				
				ClickJSElement(applicant_acknowledgement_florida, "Click on applicant acknowledgement");
				ClickJSElement(agent_acknowledgementforimportance_florida, "Click on agent acknowledgement for explaining the importance of answering the question");
			
				
			}
			else {
				
				if(current_employment_income.equalsIgnoreCase("Yes"))
					ClickJSElement(curr_employment_income, "Click on current employment income checkbox");
				
				if(saving.equalsIgnoreCase("Yes"))
					ClickJSElement(savings, "Click on savings checkbox");
				
				if(current_investment_income.equalsIgnoreCase("Yes"))
					ClickJSElement(curr_investment_income, "Click on current investment income checkbox");
				
				if(sell_investments.equalsIgnoreCase("Yes"))
					ClickJSElement(sellinvestments, "Click on sell investments checkbox");
				
				if(other_current_income.equalsIgnoreCase("Yes"))
					ClickJSElement(other_curr_income, "Click on other current income checkbox");
				
				if(sell_assets.equalsIgnoreCase("Yes"))
					ClickJSElement(sell_other_assets, "Click on sell other assets checkbox");
				
				if(moneyfromfamily.equalsIgnoreCase("Yes"))
					ClickJSElement(moneyfrom_family, "Click on money from my family Savings_checkbox");			
				
				if(others.equalsIgnoreCase("Yes")) {
					ClickJSElement(other, "Click on other_checkbox");
					Thread.sleep(3000);
					EnterText(other_details, otherfund, "other fund details");
				}
		
				ComboSelectValue(affordto_keep_policy, ques_affordto_keep_policy, "ques related to afford to keep policy if partner dies");
				ComboSelectValue(household_annualincome, household_annual_income, "household annual income");
			
				ComboSelectValue(incometochangeovernxt10yrs, ques_incometochangeovernxt10yrs, "ques related to income change over nxt 10yrs");
				ComboSelectValue(plan_pay_premiums_from_income, ques_plan_pay_premiums_from_income, "ques related to plan pay premiums from income");
			
				if(buy_inflation_protection.equalsIgnoreCase("No")) {
					ClickJSElement(inflation_protection_no, "Click on inflation protection as no");
					if(differences_paymentsource_income.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_income, "Click on pay differences from income");
					
					if(differences_paymentsource_savings.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_savings, "Click on pay differences from savings");
					
					if(differences_paymentsource_investments.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_investments, "Click on pay differences from investments checkbox");
					
					if(differences_paymentsource_sell_assets.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_sellingassets, "Click on pay differences from selling assets checkbox");
					
					if(differences_paymentsource_fromfamily.equalsIgnoreCase("Yes"))
						ClickJSElement(pay_differences_from_familymoney, "Click on pay differences from family money checkbox");
					
					
					if(differences_paymentsource_other.equalsIgnoreCase("Yes")) {
						ClickJSElement(pay_differences_from_other, "Click on pay differences from other checkbox");
						Thread.sleep(3000);
						EnterText(pay_differences_from_other_details, paydiff_fromotherfund, "other fund details");
					}
				}
				else
					ClickJSElement(inflation_protection_yes, "Click on inflation protection as yes");
			
				EnterText(elimination_period_days, No_of_days_elimination_prod, "elimination period days");
				EnterText(elimination_period_cost, Approx_cost_period_of_care , "Approx cost of period of care ");
				ComboSelectValue(elimination_period_plantopay_your_care, Payment_plan_for_care , "elimination_period_plantopay_your_care");
				ComboSelectValue(assets, assets_worth, "assets worth value");
				ComboSelectValue(assetstochangeovernext10years, assets_change_over_10years, "assets to change over nxt 10yrs");
			
				if(disclosure_statement.equalsIgnoreCase("Describe financial situation"))
					ClickJSElement(disclosure_Statement1, "Click on disclosure Statement1");
				else
					ClickJSElement(disclosure_Statement2, "Click on disclosure Statement2");
				
				ClickJSElement(applicant_acknowledgement, "Click on applicant acknowledgement");
				ClickJSElement(agent_acknowledgementforimportance, "Click on agent acknowledgement for explaining the importance of answering the question");
			}
			takeScreenshot("PersonalWorksheetPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new RequestofFundsPageAction();
		
	
}}
