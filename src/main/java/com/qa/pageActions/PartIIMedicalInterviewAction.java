package com.qa.pageActions;

import com.qa.pages.PartIIMedicalInterviewPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class PartIIMedicalInterviewAction extends PartIIMedicalInterviewPage{
	
	public PartIIMedicalInterviewAction() {
		super();
	}
	Wait wait = new Wait();
	
	//Actions:
	public LeaveBehindFormsPageAction validatePartIIMedicalInterview(String medical_application_medium) throws InterruptedException {
		if(medical_application_medium.equalsIgnoreCase("teleinterview")) {
			extentTest.log(LogStatus.INFO, " - Part II Medical Interview Page - ");
			switchToFrame(framePartIIMedicalInterview);	
			ClickJSElement(orderinterview, "Click on order interview Button");
			Thread.sleep(5000);	
			wait.waitForPageLoad(driver);
			takeScreenshot("PartIIMedicalInterviewPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new LeaveBehindFormsPageAction();
	}
		else
			return new LeaveBehindFormsPageAction();

	}
}
