package com.qa.pageActions;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

import com.qa.pages.HIVConsentSPIPage;
import com.relevantcodes.extentreports.LogStatus;

public class HIVConsentSPIAction extends HIVConsentSPIPage{
	
	public HIVConsentSPIAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public PatriotActAction enterDataHIVSPIConsent(String illustration,String state) throws InterruptedException {
		String[] stateBucket = {"Georgia", "Alaska","Arkansas","Illinois","Maine","Maryland", "Minnesota", "Mississippi", "North Carolina", "North Dakota","South Carolina","Tennessee","Wisconsin","Wyoming"};
		boolean state_flag = false;
		for(String s : stateBucket) {
			if((state.equalsIgnoreCase(s))) {
				state_flag = true;
				break;
			}else {
				state_flag = false;
			}
		}		
		if((secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true)&& state_flag == false){
			extentTest.log(LogStatus.INFO, " - HIV Consent SPI Page - ");
			switchToFrame(frameHIVConsentSPI);		
			if(state.equalsIgnoreCase("Michigan"))
				ClickJSElement(consenttobetestedforHIV, "Click on consent to be tested for HIV button");
			takeScreenshot("HIVConsentSPIPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(3000);			
		
			return new PatriotActAction();
		}
		else
			return new PatriotActAction();
	}

}
