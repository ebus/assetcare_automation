package com.qa.pageActions;

import com.qa.pages.eSignaturePartiesPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

public class eSignaturePartiesAction extends eSignaturePartiesPage{
	
	public eSignaturePartiesAction() {
		super();		
	}
	
	Wait wait = new Wait();
	
	//Actions 
	public AgentInformationAction enterDataeSignatureParties(String Signed_city) throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - eSignatureParties Page - ");
		switchToFrame(frameESignatureParties);
		
		ClickElement(firstproposedinsured, "Click on first proposed insured");
		
		if(secondproposedinsuredflag == true||secondproposedinsuredillustrationflag == true) 
			ClickElement(secondproposedinsured, "Click on second proposed insured");
		
		ClickElement(ownerinsured, "Click on owner insured");
		ClickElement(payorinsured, "Click on payor insured");
		ClickElement(coownerinsured, "Click on coowner insured");

		EnterText(txtCity, Signed_city, "City");
		ClickElement(signedstate, "Click on signed state");
		
		
		ClickElement(btnApply, "Apply Button");
		Thread.sleep(5000);
		wait.waitForPageLoad(driver);
		takeScreenshot("eSignatureParties");
		ClickElement(btnYesContinue, "YesContinue Button");
		switchToDefault();
		Thread.sleep(5000);
		return new AgentInformationAction();
	}
}
