package com.qa.pageActions;

import static com.qa.pageActions.ApplicationSetupAction.secondproposedinsuredflag;
import static com.qa.pageActions.Illustration_PolicyPageAction.secondproposedinsuredillustrationflag;

import com.qa.pages.HIVConsentFPIPage;
import com.qa.pages.PrimarycarephysicianPage;
import com.relevantcodes.extentreports.LogStatus;

public class PrimarycarephysicianAction extends PrimarycarephysicianPage{
	
	public PrimarycarephysicianAction() {
		super();
	}
	
	public static boolean flag;
	//Actions
	public HealthQuesAction enterPrimarycarephysiciandetails(String medical_application_medium) throws InterruptedException {
		
			if(!medical_application_medium.equalsIgnoreCase("teleinterview")) {
				extentTest.log(LogStatus.INFO, " - HIV Consent FPI Page - ");
				switchToFrame(frameprimarycarephysician);	
				EnterText(primarycarephysician_name, "Physician name", "primary care physician name");
				EnterText(city, "city", "city");
				ComboSelectValue(state, "INDIANA","state");
				
				enterDOB("06/06/2019");
				EnterText(reasonforvisit, "reason for visit", "reason for visit");
				ClickJSElement(medication_no, "Click on taking any medication as no");
				if(secondproposedinsuredflag == true || secondproposedinsuredillustrationflag == true) {
					EnterText(primarycarephysician_name_secondinsured, "Physician name", "primary care physician name");
					EnterText(city_secondinsured, "city", "city");
					ComboSelectValue(state_secondinsured, "INDIANA","state");
					
					enterDOB("06/06/2019");
					EnterText(reasonforvisit_secondinsured, "reason for visit", "reason for visit");
					ClickJSElement(medication_no_secondinsured, "Click on taking any medication as no");
				}
				takeScreenshot("Primarycarephysician");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);			
				return new HealthQuesAction();
			}
			else
				return new HealthQuesAction();
	}
	
	public void enterDOB(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		lastseendate_month.sendKeys(strMonth);
		lastseendate_year.sendKeys(strYear);
		lastseendate_date.sendKeys(strDay);	
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}

}
