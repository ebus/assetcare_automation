package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class SuitabilityAgentPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameSuitabilityAgent;
	
	@FindBy(id="flda_12")
	public WebElement advantage;
	
	@FindBy(id="flda_13")
	public WebElement disadvantage;
	
	@FindBy(id="flda_14")
	public WebElement recommendation;
	
	@FindBy(id="btn_4")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public SuitabilityAgentPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
