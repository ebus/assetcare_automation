package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PatriotActContPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement framePatriotActCont;	
	
	@FindBy(id="lb_61")
	public WebElement formofID_dropdown;
	
	@FindBy(id="flda_62")
	public WebElement other_desc;
	
	@FindBy(id="flda_58")
	public WebElement issuar_name;
	
	@FindBy(id="flda_66")
	public WebElement name;
	
	@FindBy(id="flda_64")
	public WebElement streetaddresss;
	
	@FindBy(id="flda_72")
	public WebElement city;
	
	@FindBy(id="lb_70")
	public WebElement state;
	
	@FindBy(id="flda_68")
	public WebElement zip;
	
	@FindBy(id="flda_51")
	public WebElement numberonID;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	public WebElement id_expiration_txtMonth;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	public WebElement id_expiration_txtDay;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	public WebElement id_expiration_txtYear;
	
	@FindBy(id="flda_52")
	public WebElement stateorcountry;
	
	@FindBy(id="btn_32")
	public WebElement btnNext;

	
	
//	@FindBy(id="cb_104")
//	public WebElement cbInheritance;
//	
//	@FindBy(id="cb_117")
//	public WebElement cbAutoFill;
//	
//	@FindBy(id="lb_76")
//	public WebElement dropdownFormID;
//	
//	@FindBy(id="flda_75")
//	public WebElement txtIssuer;
//	
//	
//	@FindBy(id="flda_79")
//	public WebElement txtNumberID;
//	
//	@FindBy(id="flda_69")
//	public WebElement txtState;
//	
//	@FindBy(id="lb_74")
//	public WebElement dropdownDocReviewed;
//	
//		
//	
//	//Multiple owner
//	@FindBy(id="lb_90")
//	public WebElement dropdownFormID_MO;
//	
//	@FindBy(id="flda_89")
//	public WebElement txtIssuer_MO;
//	
//	@FindBy(id="flda_84")
//	public WebElement txtName_MO;
//	
//	@FindBy(id="flda_83")
//	public WebElement txtStreet_MO;
//	
//	@FindBy(id="flda_82")
//	public WebElement txtCity_MO;
//	
//	@FindBy(id="lb_81")
//	public WebElement dropdownState_MO;
//	
//	@FindBy(id="flda_80")
//	public WebElement txtZip_MO;
//	
//	@FindBy(id="flda_88")
//	public WebElement txtNumberID_MO;
//	
//	@FindBy(id="flda_86")
//	public WebElement txtCountry_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-month hint jq-dte-is-required']")
//	public WebElement txtMonth_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-day hint jq-dte-is-required']")
//	public WebElement txtDay_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-year hint jq-dte-is-required']")
//	public WebElement txtYear_MO;	
//	
//	//********************
//	
//	@FindBy(id="cb_61")
//	public WebElement cbICertify;
	
		
	//Initializing the Page Objects:
	public PatriotActContPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
