package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PatriotActPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement framePatriotAct;
	
	@FindBy(id="cb_94")
	public WebElement cashorsavings_checkbox;
	
	@FindBy(id="cb_91")
	public WebElement earningsincome_checkbox;
	
	@FindBy(id="cb_90")
	public WebElement inheritance_checkbox;
	
	@FindBy(id="cb_84")
	public WebElement insuranceproceeds_checkbox;
	
	@FindBy(id="cb_83")
	public WebElement investementproceeds_checkbox;
	
	@FindBy(id="cb_82")
	public WebElement pension_IRA_RetirementSavings_checkbox;
	
	@FindBy(id="cb_81")
	public WebElement legalSettlements_checkbox;
	
	@FindBy(id="cb_80")
	public WebElement gift_checkbox;
	
	@FindBy(id="cb_92")
	public WebElement transferorExchange_from_Existing_Annuity_checkbox;
	
	@FindBy(id="cb_79")
	public WebElement saleofbusiness_checkbox;
	
	@FindBy(id="cb_78")
	public WebElement transferorExchange_from_Existing_Life_Insurance_checkbox;
	
	@FindBy(id="cb_77")
	public WebElement other_checkbox;
	
	@FindBy(id="flda_89")
	public WebElement otherfunds;
	
	@FindBy(id="flda_87")
	public WebElement premiumpayor_name;
	
	@FindBy(id="flda_85")
	public WebElement relationship_to_owner;
	
	@FindBy(id="lb_54")
	public WebElement formofID_dropdown;
	
	@FindBy(id="flda_26")
	public WebElement other_desc;
	
	@FindBy(id="flda_51")
	public WebElement issuar_name;
	
	@FindBy(id="flda_22")
	public WebElement name;
	
	@FindBy(id="cb_97")
	public WebElement addresssameasowneraddress_checkbox;
	
	@FindBy(id="flda_1")
	public WebElement numberonID;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	public WebElement id_expiration_txtMonth;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	public WebElement id_expiration_txtDay;
	
	@FindBy (xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	public WebElement id_expiration_txtYear;
	
	@FindBy(id="flda_30")
	public WebElement stateorcountry;
	
	@FindBy(id="cb_2")
	public WebElement certifybtn;
	
	@FindBy(id="cb_3")
	public WebElement unabletocertify;
	
	@FindBy(id="flda_45")
	public WebElement reasonforunabletocertify;
	
	@FindBy(id="rdo_67_1")
	public WebElement notify_yes;
	
	@FindBy(id="rdo_67_1")
	public WebElement notify_no;
	
	@FindBy(id="btn_57")
	public WebElement btnNext;

	
	
//	@FindBy(id="cb_104")
//	public WebElement cbInheritance;
//	
//	@FindBy(id="cb_117")
//	public WebElement cbAutoFill;
//	
//	@FindBy(id="lb_76")
//	public WebElement dropdownFormID;
//	
//	@FindBy(id="flda_75")
//	public WebElement txtIssuer;
//	
//	
//	@FindBy(id="flda_79")
//	public WebElement txtNumberID;
//	
//	@FindBy(id="flda_69")
//	public WebElement txtState;
//	
//	@FindBy(id="lb_74")
//	public WebElement dropdownDocReviewed;
//	
//		
//	
//	//Multiple owner
//	@FindBy(id="lb_90")
//	public WebElement dropdownFormID_MO;
//	
//	@FindBy(id="flda_89")
//	public WebElement txtIssuer_MO;
//	
//	@FindBy(id="flda_84")
//	public WebElement txtName_MO;
//	
//	@FindBy(id="flda_83")
//	public WebElement txtStreet_MO;
//	
//	@FindBy(id="flda_82")
//	public WebElement txtCity_MO;
//	
//	@FindBy(id="lb_81")
//	public WebElement dropdownState_MO;
//	
//	@FindBy(id="flda_80")
//	public WebElement txtZip_MO;
//	
//	@FindBy(id="flda_88")
//	public WebElement txtNumberID_MO;
//	
//	@FindBy(id="flda_86")
//	public WebElement txtCountry_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-month hint jq-dte-is-required']")
//	public WebElement txtMonth_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-day hint jq-dte-is-required']")
//	public WebElement txtDay_MO;
//	
//	@FindBy (xpath = "//input[@class='jq-dte-year hint jq-dte-is-required']")
//	public WebElement txtYear_MO;	
//	
//	//********************
//	
//	@FindBy(id="cb_61")
//	public WebElement cbICertify;
	
		
	//Initializing the Page Objects:
	public PatriotActPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
