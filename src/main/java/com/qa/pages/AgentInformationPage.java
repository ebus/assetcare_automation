package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class AgentInformationPage extends GenericFunction {
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameRepresentativeInformation;
	
	@FindBy(id="flda_34")
	public WebElement txtstreetaddress;
	
	@FindBy(id="flda_40")
	public WebElement txtcity;
	
	@FindBy(id="lb_30")
	public WebElement dropdownstate;
	
	@FindBy(id="flda_39")
	public WebElement txtzip;
	
	@FindBy(id="flda_45")
	public WebElement phonenumber;
	
	@FindBy(id="flda_47")
	public WebElement txtemailaddress;
	
	@FindBy(id="rdo_114_1")
	public WebElement knowledge_replacement_exist_insurance_yes;
	
	@FindBy(id="rdo_114_2")
	public WebElement knowledge_replacement_exist_insurance_no;
	
	@FindBy(id="rdo_80_1")
	public WebElement witness_completion_signatures_yes;
	
	@FindBy(id="rdo_80_2")
	public WebElement witness_completion_signatures_no;
	
	@FindBy(id="cb_78")
	public WebElement identify_proposed_insured_wellknownyou;
	
	@FindBy(id="cb_77")
	public WebElement identify_proposed_insured_photoid;
	
	@FindBy(id="cb_76")
	public WebElement identify_proposed_insured_related;
	
	@FindBy(id="flda_85")
	public WebElement txt_relatedorwellknown;
	
	@FindBy(id="rdo_91_1")
	public WebElement applicationevaluatedwithotherapp_yes;
	
	@FindBy(id="rdo_91_2")
	public WebElement applicationevaluatedwithotherapp_no;
	
	@FindBy(id="flda_92")
	public WebElement txt_name;
	
	@FindBy(id="lb_88")
	public WebElement dropdown_relationshiptoinsured;
	
	@FindBy(id="flda_86")
	public WebElement txt_otherdetails;
	
	@FindBy(id="flda_108")
	public WebElement txt_concernedperson_name;
	
	@FindBy(id="flda_112")
	public WebElement txt_concernedperson_contact;
	
	@FindBy(id="rdo_31_1")
	public WebElement additional_representatives_yes;
	
	@FindBy(id="rdo_31_2")
	public WebElement additional_representatives_no;
	
	@FindBy(id="grdx37_addRowButton")
	public WebElement additional_representatives_addbutton;
	
	@FindBy(id="modalIframe")
	public WebElement frameaddlrepresentative;
	
	@FindBy(id="flda_2")
	public WebElement txt_agentname;
	
	@FindBy(id="flda_16")
	public WebElement txt_licensenumber;
	
	@FindBy(id="flda_11")
	public WebElement txt_RepresentativeNumber;
	
	@FindBy(id="flda_9")
	public WebElement txt_split;
	
	@FindBy(id="btn_6")
	public WebElement btnSave;
	
	@FindBy(id="btn_1")
	public WebElement includecoverletterbutton;
	
	@FindBy(id="modalIframe")
	public WebElement framecoverletter;
	
	@FindBy(id="flda_7")
	public WebElement txt_coverletter;
	
	@FindBy(id="btn_53")
	public WebElement btnNext;
	
//	@FindBy(id="lb_7")
//	public WebElement dropdownRepresentativeCode;
//	
//	@FindBy(id="flda_20")
//	public WebElement txtName;
//	
//	@FindBy(id="flda_34")
//	public WebElement txtPhoneNumber;
//	
//	@FindBy(id="lb_2")
//	public WebElement dropdownLicenseNumber;
//	
//	@FindBy(id="rdo_27_1")
//	public WebElement rdoAdditionalRepresentatives_Yes;
//	
//	@FindBy(id="rdo_27_2")
//	public WebElement rdoAdditionalRepresentatives_No;
//	
//	@FindBy(id="modalIframe")
//	public WebElement frameAddRepresentative;
//	
//	@FindBy(id="grdx29_addRowButton")
//	public WebElement btnClickHere;
//	
//	@FindBy(id="flda_8")
//	public WebElement txtRepresentativesName;
//	
//	@FindBy(id="flda_20")
//	public WebElement license_Identification_No;
//	
//	@FindBy(id="flda_15")
//	public WebElement txtRepresentativesPercentage;
//	
//	@FindBy(id="flda_17")
//	public WebElement txtRepresentativesCode;
//	
//	
//	@FindBy(id="rdo_63_2")
//	public WebElement rdoQuestion2_No;
//	
//	@FindBy(id="rdo_73_1")
//	public WebElement rdoQuestion3_Yes;
//	
//	@FindBy(id="rdo_61_1")
//	public WebElement rdoQuestion4_Yes;
//	
//	@FindBy(id="cb_69")
//	public WebElement cbPhotoID;
//		
//	@FindBy(id="rdo_79_2")
//	public WebElement rdoShdAppEvaluated_No;
//	
//	
//	
//	@FindBy(css="button#btn_12")
//	public WebElement btnAgentSave;
	
	//Initializing the Page Objects:
	public AgentInformationPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
