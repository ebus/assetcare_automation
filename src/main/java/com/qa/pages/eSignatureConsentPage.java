package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class eSignatureConsentPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameeSignatureConsent;
	
	@FindBy(id="btn_15")
	public WebElement btnReviewApplication ;
	
	@FindBy(id="cb_2")
	public WebElement primaryinsured_disclosure;
	
	@FindBy(id="cb_39")
	public WebElement secondinsured_disclosure;
	
	@FindBy(id="cb_10")
	public WebElement cbOwner_disclosure;
	
	@FindBy(id="cb_37")
	public WebElement payor_disclosure;
	
	@FindBy(id="cb_55")
	public WebElement coowner_disclosure;
	
	@FindBy(id="cb_19")
	public WebElement representative_disclosure;
	
	@FindBy(id="btn_8")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public eSignatureConsentPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
