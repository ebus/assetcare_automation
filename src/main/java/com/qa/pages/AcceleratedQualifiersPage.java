package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class AcceleratedQualifiersPage extends GenericFunction{
	//Initializing the Page Objects:
	public AcceleratedQualifiersPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameAcceleratedQualifiers;
	
	@FindBy(css="#rdo_41_1")
	public WebElement rdo_No_Q1_Yes;
	
	@FindBy(css="#rdo_41_2")
	public WebElement rdo_No_Q1_No;
	
	@FindBy(css="#flda_44")
	public WebElement Q1_Details;
	
	@FindBy(css="#rdo_34_1")
	public WebElement rdo_No_Q2_Yes;
	
	@FindBy(css="#rdo_34_2")
	public WebElement rdo_No_Q2_No;
	
	@FindBy(css="#rdo_26_1")
	public WebElement rdo_No_Q3_Yes;
	
	@FindBy(css="#rdo_26_2")
	public WebElement rdo_No_Q3_No;
	
	@FindBy(css="#rdo_23_1")
	public WebElement rdo_Ido;
	
	@FindBy(css="#rdo_23_2")
	public WebElement rdo_Idonot;
	
	@FindBy(css="input#flda_33")
	public WebElement txtPhoneNumber;
	
	@FindBy(css="input#cb_48")
	public WebElement cb_Authorization;
	
	@FindBy(css="button#btn_50")
	public WebElement btnInitiateProcess;
	
	@FindBy(css="div#lbl_54")
	public WebElement labelAUWMessage;
	
	@FindBy(css="#btn_10")
	public WebElement btnNext;
	
	@FindBy(id="lbl_56")
	public WebElement labelAUWQualified;
		
}
