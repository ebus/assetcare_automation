package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class LeaveBehindFormsPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameLeaveBehindForms;
	
	@FindBy(id="btn_6")
	public WebElement btnviewforms;
	
	@FindBy(id="btn_11")
	public WebElement btnNext;


	//Initializing the Page Objects:
	public LeaveBehindFormsPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
