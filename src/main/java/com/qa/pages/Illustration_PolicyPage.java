package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class Illustration_PolicyPage extends GenericFunction{
	//Initializing the Page Objects:
	public Illustration_PolicyPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameIllustrationPolicy;
	
	@FindBy(id="rdo_92_1")
	public WebElement firstinsured_nontobaccobutton;
	
	@FindBy(id="rdo_92_2")
	public WebElement firstinsured_tobaccobutton;
	
	@FindBy(css="select#lb_19")
	public WebElement firstinsureddropdowntablerating;
	
	@FindBy(id="cb_12")
	public WebElement secondinsuredbutton;
	
	@FindBy(id="flda_117")
	public WebElement secondinsured_firstname;
	
	@FindBy(id="flda_123")
	public WebElement secondinsured_lastname;
	
	@FindBy(id="rdo_114_1")
	public WebElement secondinsured_gender_male;
	
	@FindBy(id="rdo_114_2")
	public WebElement secondinsured_gender_female;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-month hint']")
	public WebElement secondinsured_dob_month;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-day hint']")
	public WebElement secondinsured_dob_day;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-year hint']")
	public WebElement secondinsured_dob_year;
	
	@FindBy(id="rdo_113_1")
	public WebElement secondinsured_nontobacco;
	
	@FindBy(id="rdo_113_2")
	public WebElement secondinsured_tobacco;
	
	@FindBy(css="select#lb_127")
	public WebElement secondinsured_dropdowntablerating;
	
	@FindBy(id="rdo_195_1")
	public WebElement recurringpremium;
	
	@FindBy(id="rdo_195_2")
	public WebElement singlepremiumcash;
	
	@FindBy(id="rdo_195_3")
	public WebElement singlepremiumannuity;
	
	@FindBy(css="select#lb_44")
	public WebElement payoption;
	
	@FindBy(id="cb_190")
	public WebElement returnofpremium;
	
	@FindBy(id="rdo_147_1")
	public WebElement premiumamount;
	
	@FindBy(id="flda_156")
	public WebElement txtpremiumamount;
	
	@FindBy(id="rdo_147_2")
	public WebElement faceamount;
	
	@FindBy(id="flda_153")
	public WebElement txtfaceamount;
	
	@FindBy(id="rdo_147_3")
	public WebElement initialmonthlyltcbenefit;
	
	@FindBy(id="flda_192")
	public WebElement txtinitialmonthlyltcbenefit;
	
	@FindBy(id="lb_162")
	public WebElement duration_accelerationBenefits ;
	
	@FindBy(id="cb_36")
	public WebElement cob_RiderCheckbox ;
	
	@FindBy(id="lb_53")
	public WebElement cob_Paymentoption ;
	
	@FindBy(id="lb_55")
	public WebElement cob_Duration ;
	
	@FindBy(id="lb_52")
	public WebElement cob_Inflation ;
	
	@FindBy(id="lb_167")
	public WebElement aob_Inflation ;
	
	@FindBy(id="lb_169")
	public WebElement inflation_duration ;
	
	@FindBy(id="cb_171")
	public WebElement premiumdropinrider_checkbox ;
	
	@FindBy(id="flda_173")
	public WebElement txt_premiumdropinrider ;
	
	@FindBy(id="cb_175")
	public WebElement nonforfeiture_checkbox ;
	
	@FindBy(id="lb_176")
	public WebElement yearstoquote_dropdown ;
	
	@FindBy(id="cb_183")
	public WebElement summarywithsign_checkbox ;
	
	@FindBy(id="cb_184")
	public WebElement aboutOA_checkbox ;
	
	@FindBy(id="cb_197")
	public WebElement financialprofessionals_checkbox ;
	
	@FindBy(id="cb_185")
	public WebElement revisedquote_checkbox ;
	
	@FindBy(id="flda_90")
	public WebElement txt_policy_no ;
	
	@FindBy(id="flda_81")
	public WebElement license_no ;
	
	@FindBy(id="btn_194")
	public WebElement button_quote ;
	
	@FindBy(id="rdo_144_2")
	public WebElement taxqualifiacation_qualified;
	
	@FindBy(id="rdo_144_1")
	public WebElement taxqualifiacation_nonqualified;
	
	@FindBy(id="rdo_151_1")
	public WebElement inputmethod_annuitypremium;
	
	@FindBy(id="rdo_151_2")
	public WebElement inputmethod_lifepremium;
	
	@FindBy(id="cb_159")
	public WebElement policyholderpaypremiumdirectly;
	
	@FindBy(id="flda_161")
	public WebElement txt_startingyearthroughyear;
	
	@FindBy(id="flda_139")
	public WebElement txt_annuitypremiumamout;
	
	@FindBy(id="flda_154")
	public WebElement txt_lifepremiumamount;
	
	@FindBy(css="#spanApplication")
	public WebElement tabApplication;
	
//	@FindBy(id="flda_123")
//	public WebElement secondinsured_lastname;
	
	@FindBy(css="select#lb_20")
	public WebElement dropdownGender;
	
	@FindBy(css="#cb_68")
	public WebElement cbUseUnisexRates;
	
	@FindBy(css="select#lb_11")
	public WebElement dropdownPrimaryInsuredClass;
	
	@FindBy(css="#btn_9")
	public WebElement btnRating;
	
	@FindBy(id="modalIframe")
	public WebElement frameRatingDetails;
	
	@FindBy(css="input#flda_4")
	public WebElement txtPermanentFlatExtra;
	
	@FindBy(css="input#flda_6")
	public WebElement txtTemporaryFlatExtra;
	
	@FindBy(css="input#flda_8")
	public WebElement txtForYears;
	
	@FindBy(css="#btn_10")
	public WebElement btnSave;
	
	@FindBy(id ="lb_66")
	public WebElement dropdownPlan;
	
	@FindBy(css="select#lb_69")
	public WebElement dropdownInputMethods;
	
	@FindBy(css="input#flda_4")
	public WebElement txtIntialBaseFaceAmount;
	
	@FindBy(css="input#flda_30")
	public WebElement txtPremiumAmount;
	
	@FindBy(id = "lb_6")
	public WebElement modeofpayment;
	
	@FindBy(css="input#cb_74")
	public WebElement wpd_Rider_illustration;
	
	@FindBy(css="input#flda_86")
	public WebElement wpd_Rider_duration;
	
	@FindBy(id = "lb_77")
	public WebElement wpd_conversion_option;
	
	@FindBy(css="input#cb_81")
	public WebElement CBR;
	
	@FindBy(css="input#flda_89")
	public WebElement CBRunit;
	
	@FindBy(css="input#flda_82")
	public WebElement CBRduration;
	
	@FindBy(css="#lbl_57")
	public WebElement txtInputMethods;//
	
	@FindBy(css="#cb_74")
	public WebElement waiverofpremiumdisabilitybtn;
	
	@FindBy(css="#flda_86")
	public WebElement duration;
	
	@FindBy(css="#cb_81")
	public WebElement child_benefit_riderbtn;
	
	@FindBy(css="#flda_89")
	public WebElement units;
	
	@FindBy(css="#flda_82")
	public WebElement duration_child_rider;
	
	@FindBy(css="#cb_85")
	public WebElement input_summary_btn;
	
	@FindBy(css="#btnIllustrations")
	public WebElement illustrations_btn;
	
	
	
	
	
	
	
	@FindBy(css="select#lb_7")
	public WebElement dropdownDividends;
	
	@FindBy(css="#btn_5")
	public WebElement btnSaveDividendOption;
	
	@FindBy(id="modalIframe")
	public WebElement frameDividendOptionChange;
	
	@FindBy(css="#cb_49")
	public WebElement cbDividendOptionChange;
	
	@FindBy(css="#grdx8_rc_0_0")
	public WebElement txtFromAgeDividendOptionChange;
	
	@FindBy(css="input#flda_52")
	public WebElement txtPercentageDividendScale;
	
	@FindBy(css="input#cb_13")
	public WebElement cbScheduleLoansWithdrawals;
	
	@FindBy(css="select#lb_59")
	public WebElement dropdownWithdrawalOptions;
	
	@FindBy(css="select#lb_21")
	public WebElement dropdownLoanInterest;
	
	@FindBy(css="select#lb_84")
	public WebElement dropdownDistributionSolves;

	@FindBy(css="select#lb_28")
	public WebElement dropdownPaymentMode;

	@FindBy(css="#rdo_155_1")
	public WebElement rdoIsTermConversion_Yes;
	
	@FindBy(css="#rdo_155_2")
	public WebElement rdoIsTermConversion_No;
	
	@FindBy(css="#btn_156")
	public WebElement btnNext;
}
