package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class eSigDisclosuresPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameESigDisclosures;
	
	@FindBy(id="rdo_24_1")
	public WebElement rdofirstProInsYes;
	
	@FindBy(id="rdo_42_1")
	public WebElement rdosecondProInsYes;
	
	@FindBy(id="lb_28")
	public WebElement dropdownIdentification_firstPro;
	
	@FindBy(id="lb_40")
	public WebElement dropdownIdentification_secondPro;
	
	@FindBy(id="rdo_25_1")
	public WebElement rdoProInsYes_Owner;
	
	@FindBy(id="rdo_25_2")
	public WebElement rdoProInsNo_Owner;
	
	@FindBy(id="lb_30")
	public WebElement dropdownIdentification_Owner;
	
	@FindBy(id="rdo_67_1")
	public WebElement rdopayorProInsYes;
	
	@FindBy(id="rdo_67_2")
	public WebElement rdopayorProInsNo;
	
	@FindBy(id="lb_68")
	public WebElement dropdownIdentification_payor;
	
	@FindBy(id="btn_77")
	public WebElement btnNext;
	
	
	//Initializing the Page Objects:
	public eSigDisclosuresPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
