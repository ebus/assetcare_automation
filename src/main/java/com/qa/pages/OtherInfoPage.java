package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class OtherInfoPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameOtherInfo;
	
	@FindBy(id="rdo_7_1")
	public WebElement anyofpolicypremiumspaidthroughpremiumfinancingloan_yes;
	
	@FindBy(id="rdo_7_2")
	public WebElement anyofpolicypremiumspaidthroughpremiumfinancingloan_no;
	
	@FindBy(id="flda_5")
	public WebElement anyofpolicypremiumspaidthroughpremiumfinancingloan_explanation;
	
	@FindBy(id="rdo_3_1")
	public WebElement intendnoworinthefuturetoselltransferorassign_yes;
	
	@FindBy(id="rdo_3_2")
	public WebElement intendnoworinthefuturetoselltransferorassign_no;
	
	@FindBy(id="flda_1")
	public WebElement intendnoworinthefuturetoselltransferorassign_explanation;
	
	@FindBy(id="btn_9")
	public WebElement btnNext;
	
		
	//Initializing the Page Objects:
	public OtherInfoPage() {
		super();
		PageFactory.initElements(driver, this);
	}	
		
		
}
