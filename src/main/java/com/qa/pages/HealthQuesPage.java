package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuesPage extends GenericFunction {
	
	public HealthQuesPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
		@FindBy(id="CossScreenFrame")
		public WebElement frameHealthQuestion;
		
		@FindBy(id="lb_230")
		public WebElement height_ft_firstinsured;
		
		@FindBy(id="lb_231")
		public WebElement height_in_firstinsured;
		
		@FindBy(id="flda_229")
		public WebElement weight_firstinsured;
		
		@FindBy(id="cb_224")
		public WebElement weightgained_firstinsured;
		
		@FindBy(id="cb_225")
		public WebElement weightlost_firstinsured;
		
		@FindBy(id="flda_223")
		public WebElement weightamount_firstinsured;
		
		@FindBy(id="lb_55")
		public WebElement height_ft_secondinsured;
		
		@FindBy(id="lb_56")
		public WebElement height_in_secondinsured;
		
		@FindBy(id="flda_54")
		public WebElement weight_secondinsured;
		
		@FindBy(id="cb_226")
		public WebElement weightgained_secondinsured;
		
		@FindBy(id="cb_227")
		public WebElement weightlost_secondinsured;
		
		@FindBy(id="flda_228")
		public WebElement weightamount_secondinsured;
		
		
		
		@FindBy(id="rdo_80_1")
		public WebElement rdoQuestionNo_1_yes_firstinsured;
		
		@FindBy(id="rdo_80_2")
		public WebElement rdoQuestionNo_1_no_firstinsured;
		
		@FindBy(id="rdo_84_1")
		public WebElement rdoQuestionNo_2_yes_firstinsured;
		
		@FindBy(id="rdo_84_2")
		public WebElement rdoQuestionNo_2_no_firstinsured;
		
		@FindBy(id="rdo_88_1")
		public WebElement rdoQuestionNo_3_yes_firstinsured;
		
		@FindBy(id="rdo_88_2")
		public WebElement rdoQuestionNo_3_no_firstinsured;
		
		@FindBy(id="rdo_92_1")
		public WebElement rdoQuestionNo_4_yes_firstinsured;
		
		@FindBy(id="rdo_92_2")
		public WebElement rdoQuestionNo_4_no_firstinsured;
		
		@FindBy(id="rdo_115_1")
		public WebElement rdoQuestionNo_5_yes_firstinsured;
		
		@FindBy(id="rdo_115_2")
		public WebElement rdoQuestionNo_5_no_firstinsured;
		
		@FindBy(id="rdo_96_1")
		public WebElement rdoQuestionNo_6_yes_firstinsured;
		
		@FindBy(id="rdo_96_2")
		public WebElement rdoQuestionNo_6_no_firstinsured;
		
		@FindBy(id="rdo_100_1")
		public WebElement rdoQuestionNo_7_yes_firstinsured;
		
		@FindBy(id="rdo_100_2")
		public WebElement rdoQuestionNo_7_no_firstinsured;
		
		@FindBy(id="rdo_104_1")
		public WebElement rdoQuestionNo_8_yes_firstinsured;
		
		@FindBy(id="rdo_104_2")
		public WebElement rdoQuestionNo_8_no_firstinsured;
		
		@FindBy(id="rdo_108_1")
		public WebElement rdoQuestionNo_9_yes_firstinsured;
		
		@FindBy(id="rdo_108_2")
		public WebElement rdoQuestionNo_9_no_firstinsured;
		
		@FindBy(id="rdo_123_1")
		public WebElement rdoQuestionNo_10_yes_firstinsured;
		
		@FindBy(id="rdo_123_2")
		public WebElement rdoQuestionNo_10_no_firstinsured;
		
		@FindBy(id="rdo_130_1")
		public WebElement rdoQuestionNo_11_yes_firstinsured;
		
		@FindBy(id="rdo_130_2")
		public WebElement rdoQuestionNo_11_no_firstinsured;
		
		@FindBy(id="rdo_137_1")
		public WebElement rdoQuestionNo_12_yes_firstinsured;
		
		@FindBy(id="rdo_137_2")
		public WebElement rdoQuestionNo_12_no_firstinsured;
		
		@FindBy(id="rdo_144_1")
		public WebElement rdoQuestionNo_13_yes_firstinsured;
		
		@FindBy(id="rdo_144_2")
		public WebElement rdoQuestionNo_13_no_firstinsured;
		
		@FindBy(id="rdo_151_1")
		public WebElement rdoQuestionNo_14_yes_firstinsured;
		
		@FindBy(id="rdo_151_2")
		public WebElement rdoQuestionNo_14_no_firstinsured;
		
		@FindBy(id="rdo_158_1")
		public WebElement rdoQuestionNo_15_yes_firstinsured;
		
		@FindBy(id="rdo_158_2")
		public WebElement rdoQuestionNo_15_no_firstinsured;
		
		@FindBy(id="rdo_167_1")
		public WebElement rdoQuestionNo_16_yes_firstinsured;
		
		@FindBy(id="rdo_167_2")
		public WebElement rdoQuestionNo_16_no_firstinsured;
		
		@FindBy(id="rdo_174_1")
		public WebElement rdoQuestionNo_17_yes_firstinsured;
		
		@FindBy(id="rdo_174_2")
		public WebElement rdoQuestionNo_17_no_firstinsured;
		
		@FindBy(id="rdo_181_1")
		public WebElement rdoQuestionNo_18_yes_firstinsured;
		
		@FindBy(id="rdo_181_2")
		public WebElement rdoQuestionNo_18_no_firstinsured;
		
		@FindBy(id="rdo_188_1")
		public WebElement rdoQuestionNo_19_yes_firstinsured;
		
		@FindBy(id="rdo_188_2")
		public WebElement rdoQuestionNo_19_no_firstinsured;
		
		@FindBy(id="rdo_195_1")
		public WebElement rdoQuestionNo_20_yes_firstinsured;
		
		@FindBy(id="rdo_195_2")
		public WebElement rdoQuestionNo_20_no_firstinsured;
		
		@FindBy(id="rdo_202_1")
		public WebElement rdoQuestionNo_21_yes_firstinsured;
		
		@FindBy(id="rdo_202_2")
		public WebElement rdoQuestionNo_21_no_firstinsured;
		
		@FindBy(id="rdo_81_1")
		public WebElement rdoQuestionNo_1_yes_secondinsured;
		
		@FindBy(id="rdo_81_2")
		public WebElement rdoQuestionNo_1_no_secondinsured;
		
		@FindBy(id="rdo_85_1")
		public WebElement rdoQuestionNo_2_yes_secondinsured;
		
		@FindBy(id="rdo_85_2")
		public WebElement rdoQuestionNo_2_no_secondinsured;
		
		@FindBy(id="rdo_89_1")
		public WebElement rdoQuestionNo_3_yes_secondinsured;
		
		@FindBy(id="rdo_89_2")
		public WebElement rdoQuestionNo_3_no_secondinsured;
		
		@FindBy(id="rdo_93_1")
		public WebElement rdoQuestionNo_4_yes_secondinsured;
		
		@FindBy(id="rdo_93_2")
		public WebElement rdoQuestionNo_4_no_secondinsured;
		
		@FindBy(id="rdo_116_1")
		public WebElement rdoQuestionNo_5_yes_secondinsured;
		
		@FindBy(id="rdo_116_2")
		public WebElement rdoQuestionNo_5_no_secondinsured;
		
		@FindBy(id="rdo_97_1")
		public WebElement rdoQuestionNo_6_yes_secondinsured;
		
		@FindBy(id="rdo_97_2")
		public WebElement rdoQuestionNo_6_no_secondinsured;
		
		@FindBy(id="rdo_101_1")
		public WebElement rdoQuestionNo_7_yes_secondinsured;
		
		@FindBy(id="rdo_101_2")
		public WebElement rdoQuestionNo_7_no_secondinsured;
		
		@FindBy(id="rdo_162_1")
		public WebElement rdoQuestionNo_8_yes_secondinsured;
		
		@FindBy(id="rdo_162_2")
		public WebElement rdoQuestionNo_8_no_secondinsured;
		
		@FindBy(id="rdo_109_1")
		public WebElement rdoQuestionNo_9_yes_secondinsured;
		
		@FindBy(id="rdo_109_2")
		public WebElement rdoQuestionNo_9_no_secondinsured;
		
		@FindBy(id="rdo_124_1")
		public WebElement rdoQuestionNo_10_yes_secondinsured;
		
		@FindBy(id="rdo_124_2")
		public WebElement rdoQuestionNo_10_no_secondinsured;
		
		@FindBy(id="rdo_131_1")
		public WebElement rdoQuestionNo_11_yes_secondinsured;
		
		@FindBy(id="rdo_131_2")
		public WebElement rdoQuestionNo_11_no_secondinsured;
		
		@FindBy(id="rdo_138_1")
		public WebElement rdoQuestionNo_12_yes_secondinsured;
		
		@FindBy(id="rdo_138_2")
		public WebElement rdoQuestionNo_12_no_secondinsured;
		
		@FindBy(id="rdo_145_1")
		public WebElement rdoQuestionNo_13_yes_secondinsured;
		
		@FindBy(id="rdo_145_2")
		public WebElement rdoQuestionNo_13_no_secondinsured;
		
		@FindBy(id="rdo_152_1")
		public WebElement rdoQuestionNo_14_yes_secondinsured;
		
		@FindBy(id="rdo_152_2")
		public WebElement rdoQuestionNo_14_no_secondinsured;
		
		@FindBy(id="rdo_159_1")
		public WebElement rdoQuestionNo_15_yes_secondinsured;
		
		@FindBy(id="rdo_159_2")
		public WebElement rdoQuestionNo_15_no_secondinsured;
		
		@FindBy(id="rdo_206_1")
		public WebElement rdoQuestionNo_16_yes_secondinsured;
		
		@FindBy(id="rdo_206_2")
		public WebElement rdoQuestionNo_16_no_secondinsured;
		
		@FindBy(id="rdo_207_1")
		public WebElement rdoQuestionNo_17_yes_secondinsured;
		
		@FindBy(id="rdo_207_2")
		public WebElement rdoQuestionNo_17_no_secondinsured;
		
		@FindBy(id="rdo_208_1")
		public WebElement rdoQuestionNo_18_yes_secondinsured;
		
		@FindBy(id="rdo_208_2")
		public WebElement rdoQuestionNo_18_no_secondinsured;
		
		@FindBy(id="rdo_209_1")
		public WebElement rdoQuestionNo_19_yes_secondinsured;
		
		@FindBy(id="rdo_209_2")
		public WebElement rdoQuestionNo_19_no_secondinsured;
		
		@FindBy(id="rdo_211_1")
		public WebElement rdoQuestionNo_21_yes_secondinsured;
		
		@FindBy(id="rdo_211_2")
		public WebElement rdoQuestionNo_21_no_secondinsured;
		
		@FindBy(id="btn_3")
		public WebElement btnNext;	

}
