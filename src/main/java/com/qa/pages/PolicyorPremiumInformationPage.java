package com.qa.pages;

import com.qa.util.GenericFunction;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PolicyorPremiumInformationPage extends GenericFunction {
	
	//Initializing the Page Objects:
	public PolicyorPremiumInformationPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="CossScreenFrame")
	public WebElement PolicyorPremiumInfo;
	
	@FindBy(id="flda_293")
	public WebElement wholelifefaceAmount;
	
	@FindBy(id="lb_194")
	public WebElement productfundingoptions;
	
	@FindBy(id="cb_67")
	public WebElement checkbox_premiumorcash;
	
	@FindBy(id="rdo_11_1")
	public WebElement withapplication_yes;
	
	@FindBy(id="flda_249")
	public WebElement application_premiumAmount;
	
	@FindBy(id="rdo_274_1")
	public WebElement firstproposedinsured_ownerannuity_yes;
	
	@FindBy(id="rdo_274_2")
	public WebElement firstproposedinsured_ownerannuity_no;
	
	@FindBy(id="rdo_273_1")
	public WebElement firstproposedinsured_annuity_yes;
	
	@FindBy(id="rdo_273_2")
	public WebElement firstproposedinsured_annuity_no;
	
	@FindBy(id="rdo_11_2")
	public WebElement withapplication_no;
	
	@FindBy(id="cb_211")
	public WebElement checkbox_cdtransfer;
	
	@FindBy(id="cb_5")
	public WebElement checkbox_exchange1035;
	
	@FindBy(id="cb_258")
	public WebElement checkbox_IRAtransfer;
	
	@FindBy(id="rdo_20_1")
	public WebElement wishStateLifetofacilitate_yes;
	
	@FindBy(id="rdo_20_2")
	public WebElement wishStateLifetofacilitate_no;
	
	@FindBy(id="flda_212")
	public WebElement cdtransfer_premiumAmount;
	
	@FindBy(id="flda_157")
	public WebElement exchange1035_premiumAmount;
	
	@FindBy(id="flda_260")
	public WebElement IRAtransfer_premiumAmount;
	
	@FindBy(id="cb_16")
	public WebElement checkbox_other;
	
	@FindBy(id="flda_4")
	public WebElement other_sourceoffunding;
	
	@FindBy(id="flda_159")
	public WebElement other_premiumAmount;
	
	@FindBy(id="lb_213")
	public WebElement AOB;
	
	@FindBy(id="flda_294")
	public WebElement AOB_premium;

	@FindBy(id="cb_26")
	public WebElement COB_checkbox;
	
	@FindBy(id="cb_278")
	public WebElement premiumdroprider_checkbox;
	
	@FindBy(id="flda_279")
	public WebElement premiumdropAmount;
	
	@FindBy(id="cb_276")
	public WebElement checkbox_clientsendfunddirect;
	
	@FindBy(id="cb_281")
	public WebElement checkbox_insurance_req_fund;
	
	@FindBy(id="cb_275")
	public WebElement checkbox_sendfundafterunderwriting;
	
	@FindBy(id="cb_280")
	public WebElement checkbox_sendthefunds;
	
	@FindBy(id="lb_216")
	public WebElement COB_duration;
	
	@FindBy(id="lb_131")
	public WebElement COB_paymentperiod;
	
	@FindBy(id="lb_129")
	public WebElement COB_billingmode;
	
	@FindBy(id="lb_198")
	public WebElement recurringpremium_payperiod;
	
	@FindBy(id="lb_195")
	public WebElement tax_qualification;
	
	@FindBy(id="lb_197")
	public WebElement recurringpremium_billingmode;
	
	@FindBy(id="rdo_287_1")
	public WebElement  provideMnthlyDraftinformation_yes;
	
	@FindBy(id="rdo_96_1")
	public WebElement  account_type_checking;
	
	@FindBy(id="rdo_96_2")
	public WebElement  account_type_saving;
	
	@FindBy(id="rdo_192_1")
	public WebElement  account_Category_consumer;
	
	@FindBy(id="rdo_192_2")
	public WebElement  account_Category_business;
	
	@FindBy(id="flda_90")
	public WebElement accountnumber;
	
	@FindBy(id="flda_92")
	public WebElement routingnumber;
	
	@FindBy(id="rdo_285_1")
	public WebElement  recurrpremium_provideMnthlyDraftinformation_yes;
	
	@FindBy(id="rdo_285_2")
	public WebElement  recurrpremium_provideMnthlyDraftinformation_no;
	
	@FindBy(id="rdo_255_1")
	public WebElement  recurrpremium_account_type_checking;
	
	@FindBy(id="rdo_255_2")
	public WebElement  recurrpremium_account_type_saving;
	
	@FindBy(id="rdo_256_1")
	public WebElement  recurrpremium_account_Category_consumer;
	
	@FindBy(id="rdo_256_2")
	public WebElement  recurrpremium_account_Category_business;
	
	@FindBy(id="flda_252")
	public WebElement recurrpremium_accountnumber;
	
	@FindBy(id="flda_254")
	public WebElement recurrpremium_routingnumber;
	
	@FindBy(id="rdo_287_2")
	public WebElement  provideMnthlyDraftinformation_no;
	
	@FindBy(id="rdo_40_1")
	public WebElement Nonforfeiturebenefit_yes;
	
	@FindBy(id="rdo_40_2")
	public WebElement Nonforfeiturebenefit_no;
	
	@FindBy(id="rdo_290_1")
	public WebElement annuitantpremiumpaymentsage59by2_yes;
	
	@FindBy(id="rdo_290_2")
	public WebElement annuitantpremiumpaymentsage59by2_no;
	
	@FindBy(id="lb_220")
	public WebElement AOB_inflation;
	
	@FindBy(id="lb_219")
	public WebElement COB_inflation;
	
	@FindBy(id="lb_223")
	public WebElement Inflation_duration;
	
	@FindBy(id="lb_268")
	public WebElement withdrawalbegins;
		
	@FindBy(id="cb_246")
	public WebElement electtodesignateaperson_yes;
	
	@FindBy(id="btn_243")
	public WebElement enterdetails;
	
	@FindBy(id="modalIframe")
	public WebElement framepersonDetails;
	
	@FindBy(id="flda_45")
	public WebElement designatedperson_firstname;
	
	@FindBy(id="flda_40")
	public WebElement designatedperson_lastname;
	
	@FindBy(id="flda_12")
	public WebElement designatedperson_address;
	
	@FindBy(id="flda_13")
	public WebElement designatedperson_city;
	
	@FindBy(id="lb_4")
	public WebElement designatedperson_state;
	
	@FindBy(id="flda_3")
	public WebElement designatedperson_zip;
	
	@FindBy(id="rdo_11_1")
	public WebElement cashwithapp_yes;
	
	@FindBy(id="rdo_11_2")
	public WebElement cashwithapp_no;
	
	@FindBy(id="flda_249")
	public WebElement cashwithapp_premiumamount;
	
	@FindBy(id="btn_36")
	public WebElement savebtn;
	
	@FindBy(id="cb_245")
	public WebElement electtodesignateaperson_no;

	@FindBy(id="btn_47")
	public WebElement btnNext;
	
}

