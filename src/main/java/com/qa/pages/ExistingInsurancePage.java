package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ExistingInsurancePage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameExistingInsurance;
	
	@FindBy(id="modalIframe")
	public WebElement frameExistingInsuranceDetails;
	
	@FindBy(id="rdo_26_2")
	public WebElement rdoQuestionNo_1_no;
	
	@FindBy(id="rdo_27_2")
	public WebElement rdoQuestionNo_2_no;
	
	@FindBy(id="flda_44")
	public WebElement otherhealthinsurancepolicies;
	
	@FindBy(id="flda_46")
	public WebElement Listpoliciessold;
	
	@FindBy(id="flda_48")
	public WebElement Listpoliciessoldlast5yrs;
	
	@FindBy(id="rdo_58_2")
	public WebElement ProposedInsuredreceiveComparativeInfo_no;
	
	@FindBy(id="rdo_43_1")
	public WebElement rdoQuestionNo_3_yes;
	
	@FindBy(id="rdo_43_2")
	public WebElement rdoQuestionNo_3_no;
	
	@FindBy(id="rdo_11_1")
	public WebElement rdoQuestionNo_4_yes;
	
	@FindBy(id="rdo_11_2")
	public WebElement rdoQuestionNo_4_no;

	
	@FindBy(id="rdo_39_2")
	public WebElement rdoQuestionNo_5_no;
	
	@FindBy(id="rdo_23_2")
	public WebElement rdoQuestionNo_6_no;
	
	@FindBy(id="rdo_16_2")
	public WebElement rdoQuestionNo_7_no;
	
	@FindBy(id="rdo_57_2")
	public WebElement rdoQuestionNo_8_no;
	
	@FindBy(id="grdx5_addRowButton")
	public WebElement addbutton_firstproposedinsured;
	
	@FindBy(id="modalIframe")
	public WebElement framelongtermexistinginsurance_firstproposedinsured;
	
	@FindBy(id="lb_44")
	public WebElement insuringcompanyname_firstproposedinsured;
	
	@FindBy(id="flda_43")
	public WebElement companyname_firstproposedinsured;
	
	@FindBy(id="flda_42")
	public WebElement policynumber_firstproposedinsured;
	
	@FindBy(id="lb_38")
	public WebElement typeofinsurance_firstproposedinsured;
	
	@FindBy(id="flda_21")
	public WebElement amountofbenefit_firstproposedinsured;
	
	@FindBy(id="flda_21")
	public WebElement cashvalue_firstproposedinsured;
	
	@FindBy(id="flda_4")
	public WebElement yearissued_firstproposedinsured;
	
	@FindBy(id="rdo_48_1")
	public WebElement beingreplacedorchanged_yes_firstproposedinsured;

	@FindBy(id="rdo_48_2")
	public WebElement beingreplacedorchanged_no_firstproposedinsured;
	
	@FindBy(id="rdo_55_1")
	public WebElement exchange1035_yes_firstproposedinsured;

	@FindBy(id="rdo_55_2")
	public WebElement exchange1035_no_firstproposedinsured;
	
	@FindBy(id="cb_53")
	public WebElement agreementacceptance;
	
	@FindBy(id="flda_1")
	public WebElement liquidate$;
	
	@FindBy(id="flda_10")
	public WebElement liquidateinpercentage;
	
	@FindBy(id="rdo_12_1")
	public WebElement policyrequiredforfullsurrender;
	
	@FindBy(id="flda_49")
	public WebElement typeofoptionalbenefit_firstproposedinsured;
	
	@FindBy(id="rdo_58_2")
	public WebElement financing;

	@FindBy(id="btn_25")
	public WebElement savebtn_firstinsured;
	
	
	
	@FindBy(id="grdx13_addRowButton")
	public WebElement addbutton_secondproposedinsured;
	
	@FindBy(id="modalIframe")
	public WebElement framelongtermexistinginsurance_secondproposedinsured;
	
	@FindBy(id="lb_44")
	public WebElement insuringcompanyname_secondproposedinsured;
	
	@FindBy(id="flda_43")
	public WebElement companyname_secondproposedinsured;
	
	@FindBy(id="flda_42")
	public WebElement policynumber_secondproposedinsured;
	
	@FindBy(id="lb_38")
	public WebElement typeofinsurance_secondproposedinsured;
	
	@FindBy(id="flda_21")
	public WebElement amountofbenefit_secondproposedinsured;
	
	@FindBy(id="flda_21")
	public WebElement cashvalue_secondproposedinsured;
	
	@FindBy(id="flda_4")
	public WebElement yearissued_secondproposedinsured;
	
	@FindBy(id="rdo_48_1")
	public WebElement beingreplacedorchanged_yes_secondproposedinsured;

	@FindBy(id="rdo_48_2")
	public WebElement beingreplacedorchanged_no_secondproposedinsured;
	
	@FindBy(id="rdo_55_1")
	public WebElement exchange1035_yes_secondproposedinsured;

	@FindBy(id="rdo_55_2")
	public WebElement exchange1035_no_secondproposedinsured;
	
	@FindBy(id="flda_49")
	public WebElement typeofoptionalbenefit_secondproposedinsured;

	@FindBy(id="btn_25")
	public WebElement savebtn_secondinsured;

	
	@FindBy(id="btn_9")
	public WebElement btnNext;
	
//	@FindBy(id="rdo_19_1")
//	public WebElement rdoExistingInsYes;
//	
//	@FindBy(id="rdo_28_1")
//	public WebElement rdoPolicyReplacing_Yes;
//	
//	
//	@FindBy(id="rdo_20_2")
//	public WebElement rdoPolicySummaryStatement_No;
//	
//	@FindBy(id="rdo_17_2")
//	public WebElement rdousingfundsfromyourexistingpolicies_No;
//	
//	@FindBy(id="rdo_18_2")
//	public WebElement rdoterminatingexistingPolicy_No;
//	@FindBy(id="flda_67")
//	public WebElement Existingpolicydetail;	
//	@FindBy(id="rdo_69_2")
//	public WebElement Noticereadaloud_no;
//	@FindBy(id="rdo_63_1")
//	public WebElement Policyreplaced;
//	@FindBy(id="rdo_63_2")
//	public WebElement Usedasfinancing;
//	
//	
//	@FindBy(id="rdo_28_2")
//	public WebElement rdoPolicyReplacing_No;
//	
//	@FindBy(id="grdx40_addRowButton")
//	public WebElement btnClickAdd;
//	
//	@FindBy(id="lb_42")
//	public WebElement dropdownInsuringCompanyName;
//	
//	@FindBy(id="lb_36")
//	public WebElement dropdownTypeInsurance;
//	
//	@FindBy(id="flda_40")
//	public WebElement txtPolicyNumber;
//	
//	@FindBy(id="flda_25")
//	public WebElement homeofficelocation;
//	
//	@FindBy(id="flda_6")
//	public WebElement txtAmountBenefit;
//	
//	@FindBy(id="flda_64")
//	public WebElement txtYearIssued;
//	
//	@FindBy(id="rdo_33_1")
//	public WebElement rdoBeingReplaced_Yes;
//	
//	@FindBy(id="rdo_33_2")
//	public WebElement rdoBeingReplaced_No;
//	
//	
//	@FindBy(id="rdo_56_1")
//	public WebElement rdo1035Exchange_Yes;
//	
//	@FindBy(id="rdo_56_2")
//	public WebElement rdo1035Exchange_No;
//	
//	@FindBy(id="rdo_63_1")
//	public WebElement rdoPolicyFinancing;
//	
//	@FindBy(id="rdo_74_1")
//	public WebElement rdo1035InitPrem_Yes;
//	
//	@FindBy(id="cb_66")
//	public WebElement cb1stCheckbox;
//	
//	@FindBy(id="rdo_46_3")
//	public WebElement rdoPolicyRequired;
//	
//	
//	@FindBy(id="rdo_28_2")
//	public WebElement rdoQuestionNo_2;
//	
//	@FindBy(id="rdo_66_2")
//	public WebElement rdoQuestionNo_3;
//	
//	@FindBy(id="rdo_58_2")
//	public WebElement rdoQuestionNo_4;
//	
//	@FindBy(id="flda_56")
//	public WebElement txtTotalAmountGuardian1;
//	
//	@FindBy(id="flda_73")
//	public WebElement txtTotalAmountGuardian2;
//	
//	@FindBy(id="flda_52")
//	public WebElement txtAgeAmount;
//	
//	@FindBy(id="flda_50")
//	public WebElement txtProvideDetails;
	
	
	
	//Initializing the Page Objects:
	public ExistingInsurancePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
