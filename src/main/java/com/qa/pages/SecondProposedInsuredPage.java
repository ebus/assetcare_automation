package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class SecondProposedInsuredPage extends GenericFunction{
	
	//Initializing the Page Objects:
	public SecondProposedInsuredPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameSecondProposedInsured;
	
	@FindBy(id="flda_29")
	public WebElement SecondProposedInsuredfirstname;
	
	@FindBy(id="flda_9")
	public WebElement SecondProposedInsuredlastname;
	
	@FindBy(id="flda_21")
	public WebElement SecondProposedInsuredSSN;
	
	@FindBy(id="rdo_35_1")
	public WebElement rdoGender_Male;
	
	@FindBy(id="rdo_35_2")
	public WebElement rdoGender_Female;
	
	@FindBy(id="rdo_144_1")
	public WebElement rdoProposedInsuredmarriedorpartner_Yes;       
	
	@FindBy(id="rdo_144_2")
	public WebElement rdoProposedInsuredmarriedorpartner_No;	
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	public WebElement SecondProposedInsureddobmonth;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	public WebElement SecondProposedInsureddobday;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	public WebElement SecondProposedInsureddobyear;
	
	@FindBy(id="lb_142")
	public WebElement dropdownCountryBirth;
	
	@FindBy(id="lb_37")
	public WebElement dropdownBirthState;
	
	@FindBy(id="rdo_147_1")
	public WebElement rdoUSCitizen_Yes;
	
	@FindBy(id="rdo_147_2")
	public WebElement rdoUSCitizen_No;
	
	@FindBy(id="btn_150")
	public WebElement btnAddCitizenShipDetails;
	
	@FindBy(id="modalIframe")
	public WebElement frameCitizenshipDetails;
	
	@FindBy(id="modalIframe5")
	public WebElement frameForSaveBtn;
	
	@FindBy(id="lb_15")
	public WebElement dropdownCountryCitizenship;
	
	@FindBy(id="rdo_28_1")
	public WebElement rdoProposedInsGreenCard_Yes;
	
	@FindBy(id="rdo_28_2")
	public WebElement rdoProposedInsGreenCard_No;
	
	@FindBy(id="flda_9")
	public WebElement txtGreenCardNumber;
	
	@FindBy(id="rdo_27_1")
	public WebElement rdoProposedInsHoldUSVisa_Yes;
	
	@FindBy(id="rdo_27_2")
	public WebElement rdoProposedInsHoldUSVisa_No;
	
	@FindBy(id="flda_22")
	public WebElement txtTypeOfVisa;
	
	@FindBy(id="flda_16")
	public WebElement txtVISANumber;
	
	@FindBy(id="flda_24")
	public WebElement txtProvideDetails;
	
	@FindBy(id="lb_39")
	public WebElement relationshiptofirstinsured;
	
	@FindBy(id="cb_44")
	public WebElement checkboxforsameaddress;
	
	@FindBy(id="flda_16")
	public WebElement txtStreetAddress;
	
	@FindBy(id="flda_17")
	public WebElement txtCity;
	
	@FindBy(id="lb_12")
	public WebElement drpdwnstate;
	
	@FindBy(id="flda_125")
	public WebElement txtZip;

	@FindBy(id="flda_10")
	public WebElement txtPhoneNumber;
	
	@FindBy(id="flda_26")
	public WebElement email;
	
	@FindBy(id="flda_49")
	public WebElement employername;
	
	@FindBy(id="flda_47")
	public WebElement occupation;
	
	@FindBy(id="btn_24")
	public WebElement btnNext;
	
}
