package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ConsumerSuitabilityDueDiligencePage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameConsumerSuitabilityDueDiligence;
	
	@FindBy(id="btn_6")
	public WebElement btnDetails;
	
	@FindBy(id="lb_8")
	public WebElement maritalstatus;
	
	@FindBy(id="lb_26")
	public WebElement networth;

	@FindBy(id="lb_24")
	public WebElement annual_income;

	@FindBy(id="lb_110")
	public WebElement liquidnetworth;

	@FindBy(id="lb_19")
	public WebElement taxrate;

	@FindBy(id="lb_108")
	public WebElement annualexpenses;
	
	@FindBy(id="cb_15")
	public WebElement currentlyownstock;

	
	@FindBy(id="modalIframe")
	public WebElement frameFinancialinfo;
	
	@FindBy(id="cb_102")
	public WebElement financialobj_preservationofcapital;
	
	@FindBy(id="cb_101")
	public WebElement financialobj_income;
	
	@FindBy(id="cb_66")
	public WebElement financialobj_longtermgrowth;
	
	@FindBy(id="cb_100")
	public WebElement financialobj_shorttermgrowth;
	
	@FindBy(id="cb_99")
	public WebElement financialobj_incomeandgrowth;
	
	@FindBy(id="cb_98")
	public WebElement financialobj_longtermcarebenefit;
	
	@FindBy(id="cb_97")
	public WebElement financialobj_interestearning;
	
	@FindBy(id="cb_67")
	public WebElement financialobj_taxdeferral;
	
	@FindBy(id="cb_96")
	public WebElement intendeduse_income;
	
	@FindBy(id="cb_33")
	public WebElement intendeduse_assetaccumulation;
	
	@FindBy(id="cb_68")
	public WebElement intendeduse_protectionofPrincipal;
	
	@FindBy(id="cb_35")
	public WebElement intendeduse_retirementneedsplanning;
	
	@FindBy(id="cb_32")
	public WebElement intendeduse_estatepreservationplanning;
	
	@FindBy(id="cb_34")
	public WebElement intendeduse_longtermcareexpress;
	
	@FindBy(id="cb_36")
	public WebElement intendeduse_deathbenefit;
	
	@FindBy(id="cb_31")
	public WebElement intendeduse_other;
	
	@FindBy(id="flda_28")
	public WebElement intendeduse_other_desc;
	
	@FindBy(id="cb_95")
	public WebElement financial_stocks;
	
	@FindBy(id="flda_86")
	public WebElement financial_stocks_years;
	
	@FindBy(id="cb_94")
	public WebElement financial_bonds;
	
	@FindBy(id="flda_85")
	public WebElement financial_bonds_years;
	
	@FindBy(id="cb_93")
	public WebElement financial_mutualfund;
	
	@FindBy(id="flda_84")
	public WebElement financial_mutualfund_years;
	
	@FindBy(id="cb_92")
	public WebElement financial_variableannuities;
	
	@FindBy(id="flda_83")
	public WebElement financial_variableannuities_years;
	
	@FindBy(id="cb_91")
	public WebElement financial_fixedannuities;
	
	@FindBy(id="flda_82")
	public WebElement financial_fixedannuities_years;
	
	@FindBy(id="cb_90")
	public WebElement financial_lifeinsurance;
	
	@FindBy(id="flda_81")
	public WebElement financial_lifeinsurance_years;
	
	@FindBy(id="cb_89")
	public WebElement financial_other;
	
	@FindBy(id="flda_80")
	public WebElement financial_other_years;
	
	@FindBy(id="flda_88")
	public WebElement financial_other_desc;
	
	@FindBy(id="lb_79")
	public WebElement dropdown_riskexposure;
	
	@FindBy(id="lb_76")
	public WebElement dropdown_FinancialTimeHorizon;
	
	@FindBy(id="flda_78")
	public WebElement txt_yearstoretirement;
	
	@FindBy(id="rdo_73_1")
	public WebElement surrenderedannuity60mnths_yes;
	
	@FindBy(id="rdo_73_2")
	public WebElement surrenderedannuity60mnths_no;
	
	@FindBy(id="rdo_71_1")
	public WebElement incursurrendercharge_yes;
	
	@FindBy(id="rdo_71_2")
	public WebElement incursurrendercharge_no;
	
	@FindBy(id="flda_50")
	public WebElement txt_surrendercharge_explanation;
	
	@FindBy(id="rdo_133_1")
	public WebElement proposedannuitycontainnonguarnteedele_yes;
	
	@FindBy(id="rdo_133_2")
	public WebElement proposedannuitycontainnonguarnteedele_no;
	
	@FindBy(id= "flda_141")
	public WebElement explanation;
	
	@FindBy(id="rdo_131_1")
	public WebElement acceptnonguaranteedelement_yes;
	
	@FindBy(id="rdo_131_2")
	public WebElement acceptnonguaranteedelement_no;
	
	@FindBy(id="flda_60")
	public WebElement explanation_possibilitytaxpenalties;
	
	@FindBy(id="flda_114")
	public WebElement explanation_relationtootheravailableproduct;
	
	@FindBy(id="rdo_64_1")
	public WebElement sufficient_liquidassets_paymedicalexpenses_yes;
	
	@FindBy(id="rdo_64_2")
	public WebElement sufficient_liquidassets_paymedicalexpenses_no;
	
	@FindBy(id="flda_63")
	public WebElement sufficient_liquidassets_paymedicalexpenses_explanation;
	
	@FindBy(id="rdo_69_1")
	public WebElement pay_surrenderchargeorpenalty_yes;
	
	@FindBy(id="rdo_69_2")
	public WebElement pay_surrenderchargeorpenalty_no;
	
	@FindBy(id="flda_62")
	public WebElement  percentageliquidnetworth;
	
	@FindBy(id="flda_65")
	public WebElement explanationetworthmovingintoannuity;
	
	@FindBy(id="lb_139")
	public WebElement drpdwnpercentageliquidnetworthusedpurchaseannuity;
	
	@FindBy(id="flda_137")
	public WebElement drpdwnpercentageliquidnetworthusedpurchaseannuity_explanation_rationale;
	
	@FindBy(id="flda_135")
	public WebElement drpdwnpercentageliquidnetworthusedpurchaseannuity_explanation;
	
	@FindBy (id = "btn_1")
	public WebElement btnSave;
	
	@FindBy(id="rdo_36_1")
	public WebElement premiumpaidfromfundingsources_yes;
	
	@FindBy(id="rdo_36_2")
	public WebElement premiumpaidfromfundingsources_no;
	
	@FindBy(xpath="//input[@alt_id='PACT_SC_FeesAmount']")
	public WebElement surrenderchargeamount;
	
	@FindBy(id="flda_34")
	public WebElement suitablerecommendation;
	
	@FindBy(id="rdo_32_1")
	public WebElement plantokeepproposedannuitycontract_yes;
	
	@FindBy(id="rdo_32_2")
	public WebElement plantokeepproposedannuitycontract_no;

	@FindBy(id="flda_30")
	public WebElement plantokeepproposedannuitycontract_explanation;
	
	@FindBy(id="cb_91")
	public WebElement offerfixedannuities;
	
	@FindBy(id="cb_90")
	public WebElement offervariableannuities;
	
	@FindBy(id="cb_89")
	public WebElement offerlifeinsurance;
	
	@FindBy(id="cb_97")
	public WebElement offermutualFunds;
	
	@FindBy(id="cb_98")
	public WebElement offerstocks;
	
	@FindBy(id="cb_99")
	public WebElement offercertificateofdeposits;
	
	@FindBy(id="lb_111")
	public WebElement authorized_to_sell;
	
	@FindBy(id="flda_109")
	public WebElement primarilysellannuitiesfrom;
	
	@FindBy(id="cb_114")
	public WebElement paidcashcompensation_commission;
	
	@FindBy(id="flda_104")
	public WebElement paidcashcompensation_commission_desc;
	
	@FindBy(id="cb_113")
	public WebElement paidcashcompensation_fees;
	
	@FindBy(id="cb_112")
	public WebElement paidcashcompensation_other;
	
	@FindBy(id="flda_102")
	public WebElement paidcashcompensation_other_desc;
	
	@FindBy(id="rdo_128_1")
	public WebElement producerexercisedmaterialcontrol_yes;
	
	@FindBy(id="flda_135")
	public WebElement representativename;
	
	@FindBy(id="rdo_128_2")
	public WebElement producerexercisedmaterialcontrol_no;
	
	@FindBy (id = "btn_28")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public ConsumerSuitabilityDueDiligencePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
