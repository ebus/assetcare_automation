package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ApplicationSetupPage extends GenericFunction{
	
	//Initializing the Page Objects:
	public ApplicationSetupPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameProposedInsuredInformation;
	
	@FindBy(id="lb_35")
	public WebElement agentcode;
	
	@FindBy(id="lb_36")
	public WebElement licensenumber;
	
	@FindBy(id="rdo_14_1")
	public WebElement secondproposedinsured_yes;
	
	@FindBy(id="rdo_14_2")
	public WebElement secondproposedinsured_no;
	
	@FindBy(id="rdo_55_1")
	public WebElement thirdPartyHIPAA_yes;
	
	@FindBy(id="flda_72")
	public WebElement thirdPartyAuthorizationName;
	
	@FindBy(id="flda_71")
	public WebElement thirdPartyAuthorizationAddress;
	
	@FindBy(id="flda_70")
	public WebElement thirdPartyAuthorizationCity;
	
	@FindBy(id="lb_69")
	public WebElement thirdPartyAuthorizationState;
	
	@FindBy(id="flda_68")
	public WebElement thirdPartyAuthorizationZip;
	
	@FindBy(id="rdo_55_2")
	public WebElement thirdPartyHIPAA_no;
	
	@FindBy(id="btn_46")
	public WebElement common_ineligible_impairments;
	
	@FindBy(id="cb_45")
	public WebElement assetcaredeclaration;
	
	@FindBy(id="rdo_79_1")
	public WebElement medicalapplicationthroughteleinterview;
	
	@FindBy(id="rdo_79_2")
	public WebElement medicalapplicationthroughfullunderwriting;
	
	@FindBy(id="btn_51")
	public WebElement btnNext;
	
	
}
