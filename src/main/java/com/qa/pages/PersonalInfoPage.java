package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PersonalInfoPage extends GenericFunction {
	
	public PersonalInfoPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
		@FindBy(id="CossScreenFrame")
		public WebElement framePersonalInformation;
		
		@FindBy(id="rdo_8_1")
		public WebElement rdoQuestionNo_1_yes_firstinsured;
		
		@FindBy(id="rdo_8_2")
		public WebElement rdoQuestionNo_1_no_firstinsured;
		
		@FindBy(id="rdo_33_1")
		public WebElement rdoQuestionNo_2_yes_firstinsured;
		
		@FindBy(id="rdo_33_2")
		public WebElement rdoQuestionNo_2_no_firstinsured;
		
		@FindBy(id="rdo_44_1")
		public WebElement rdoQuestionNo_3_yes_firstinsured;
		
		@FindBy(id="rdo_44_2")
		public WebElement rdoQuestionNo_3_no_firstinsured;
		
		@FindBy(id="rdo_58_1")
		public WebElement rdoQuestionNo_4_yes_firstinsured;
		
		@FindBy(id="rdo_58_2")
		public WebElement rdoQuestionNo_4_no_firstinsured;
		
		@FindBy(id="rdo_62_1")
		public WebElement rdoQuestionNo_5_yes_firstinsured;
		
		@FindBy(id="rdo_62_2")
		public WebElement rdoQuestionNo_5_no_firstinsured;
		
		@FindBy(id="rdo_66_1")
		public WebElement rdoQuestionNo_6_yes_firstinsured;
		
		@FindBy(id="rdo_66_2")
		public WebElement rdoQuestionNo_6_no_firstinsured;
		
		@FindBy(id="rdo_79_1")
		public WebElement rdoQuestionNo_7_yes_firstinsured;
		
		@FindBy(id="rdo_79_2")
		public WebElement rdoQuestionNo_7_no_firstinsured;
		
		@FindBy(id="rdo_96_1")
		public WebElement rdoQuestionNo_8_yes_firstinsured;
		
		@FindBy(id="rdo_96_2")
		public WebElement rdoQuestionNo_8_no_firstinsured;
		
		@FindBy(id="rdo_98_1")
		public WebElement rdoQuestionNo_9_yes_firstinsured;
		
		@FindBy(id="rdo_98_2")
		public WebElement rdoQuestionNo_9_no_firstinsured;
		
		@FindBy(id="rdo_100_1")
		public WebElement rdoQuestionNo_10_yes_firstinsured;
		
		@FindBy(id="rdo_100_2")
		public WebElement rdoQuestionNo_10_no_firstinsured;
		
		@FindBy(id="rdo_113_1")
		public WebElement rdoQuestionNo_11_yes_firstinsured;
		
		@FindBy(id="rdo_113_2")
		public WebElement rdoQuestionNo_11_no_firstinsured;
		
		@FindBy(id="rdo_6_1")
		public WebElement rdoQuestionNo_1_yes_secondinsured;
		
		@FindBy(id="rdo_6_2")
		public WebElement rdoQuestionNo_1_no_secondinsured;
		
		@FindBy(id="rdo_31_1")
		public WebElement rdoQuestionNo_2_yes_secondinsured;
		
		@FindBy(id="rdo_31_2")
		public WebElement rdoQuestionNo_2_no_secondinsured;
		
		@FindBy(id="rdo_47_1")
		public WebElement rdoQuestionNo_3_yes_secondinsured;
		
		@FindBy(id="rdo_47_2")
		public WebElement rdoQuestionNo_3_no_secondinsured;
		
		@FindBy(id="rdo_59_1")
		public WebElement rdoQuestionNo_4_yes_secondinsured;
		
		@FindBy(id="rdo_59_2")
		public WebElement rdoQuestionNo_4_no_secondinsured;
		
		@FindBy(id="rdo_63_1")
		public WebElement rdoQuestionNo_5_yes_secondinsured;
		
		@FindBy(id="rdo_63_2")
		public WebElement rdoQuestionNo_5_no_secondinsured;
		
		@FindBy(id="rdo_67_1")
		public WebElement rdoQuestionNo_6_yes_secondinsured;
		
		@FindBy(id="rdo_67_2")
		public WebElement rdoQuestionNo_6_no_secondinsured;
		
		@FindBy(id="rdo_77_1")
		public WebElement rdoQuestionNo_7_yes_secondinsured;
		
		@FindBy(id="rdo_77_2")
		public WebElement rdoQuestionNo_7_no_secondinsured;
		
		@FindBy(id="rdo_95_1")
		public WebElement rdoQuestionNo_8_yes_secondinsured;
		
		@FindBy(id="rdo_95_2")
		public WebElement rdoQuestionNo_8_no_secondinsured;
		
		@FindBy(id="rdo_97_1")
		public WebElement rdoQuestionNo_9_yes_secondinsured;
		
		@FindBy(id="rdo_97_2")
		public WebElement rdoQuestionNo_9_no_secondinsured;
		
		@FindBy(id="rdo_99_1")
		public WebElement rdoQuestionNo_10_yes_secondinsured;
		
		@FindBy(id="rdo_99_2")
		public WebElement rdoQuestionNo_10_no_secondinsured;
		
		@FindBy(id="rdo_112_1")
		public WebElement rdoQuestionNo_11_yes_secondinsured;
		
		@FindBy(id="rdo_112_2")
		public WebElement rdoQuestionNo_11_no_secondinsured;
		
		@FindBy(id="btn_14")
		public WebElement btnNext;	

}
