package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class eSignaturePartiesPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameESignatureParties;
	
//	@FindBy(id="cb_128")
//	public WebElement cbInsuredAgreement;
//	
//	@FindBy(id="cb_47")
//	public WebElement cbOwner;
//	
//	@FindBy(id="cb_106")
//	public WebElement cbMO;
//	
//	@FindBy(id="cb_132")
//	public WebElement cbSurvivor;
//	
//	@FindBy(id="cb_136")
//	public WebElement cbPayor;
	
	@FindBy(id="cb_149")
	public WebElement firstproposedinsured;
	
	@FindBy(id="cb_153")
	public WebElement secondproposedinsured;
	
	@FindBy(id="cb_155")
	public WebElement ownerinsured;
	
	@FindBy(id="cb_152")
	public WebElement payorinsured;
	
	@FindBy(id="cb_156")
	public WebElement coownerinsured;
	
	@FindBy(id="lb_134")
	public WebElement signedstate;
	
	@FindBy(id="flda_9")
	public WebElement txtCity;
	
	@FindBy(id="btn_110")
	public WebElement btnApply;
	
	@FindBy(id="cb_86")
	public WebElement cb_LG;
	
	@FindBy(id="btn_140")
	public WebElement btnYesContinue;
	
	//Initializing the Page Objects:
	public eSignaturePartiesPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
