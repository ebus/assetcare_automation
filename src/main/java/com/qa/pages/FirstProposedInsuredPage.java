package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class FirstProposedInsuredPage extends GenericFunction{
	
	//Initializing the Page Objects:
	public FirstProposedInsuredPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameFirstProposedInsured;
	
	@FindBy(id="flda_21")
	public WebElement txtSSN;
	
	@FindBy(id="rdo_35_1")
	public WebElement rdoGender_Male;
	
	@FindBy(id="rdo_35_2")
	public WebElement rdoGender_Female;
	
	@FindBy(id="rdo_144_1")
	public WebElement rdoProposedInsuredmarriedorpartner_Yes;       
	
	@FindBy(id="rdo_144_2")
	public WebElement rdoProposedInsuredmarriedorpartner_No;	
	
	@FindBy(id="lb_142")
	public WebElement dropdownCountryBirth;
	
	@FindBy(id="lb_37")
	public WebElement dropdownBirthState;
	
	@FindBy(id="rdo_147_1")
	public WebElement rdoUSCitizen_Yes;
	
	@FindBy(id="rdo_147_2")
	public WebElement rdoUSCitizen_No;
	
	@FindBy(id="btn_149")
	public WebElement btnAddCitizenShipDetails;
	
	@FindBy(id="modalIframe")
	public WebElement frameCitizenshipDetails;
	
	@FindBy(id="modalIframe5")
	public WebElement frameForSaveBtn;
	
	@FindBy(id="lb_15")
	public WebElement dropdownCountryCitizenship;
	
	@FindBy(id="rdo_28_1")
	public WebElement rdoProposedInsGreenCard_Yes;
	
	@FindBy(id="rdo_28_2")
	public WebElement rdoProposedInsGreenCard_No;
	
	@FindBy(id="flda_9")
	public WebElement txtGreenCardNumber;
	
	@FindBy(id="rdo_27_1")
	public WebElement rdoProposedInsHoldUSVisa_Yes;
	
	@FindBy(id="rdo_27_2")
	public WebElement rdoProposedInsHoldUSVisa_No;
	
	@FindBy(id="flda_22")
	public WebElement txtTypeOfVisa;
	
	@FindBy(id="flda_16")
	public WebElement txtVISANumber;
	
	@FindBy(id="flda_24")
	public WebElement txtProvideDetails;
	
	@FindBy(id="flda_16")
	public WebElement txtStreetAddress;
	
	@FindBy(id="flda_17")
	public WebElement txtCity;
	
	@FindBy(id="flda_125")
	public WebElement txtZip;

	@FindBy(id="flda_10")
	public WebElement txtPhoneNumber;
	
	@FindBy(id="flda_26")
	public WebElement email;
	
	@FindBy(id="flda_49")
	public WebElement employername;
	
	@FindBy(id="flda_47")
	public WebElement occupation;
	
	@FindBy(id="lb_34")
	public WebElement dropdownOwnerofLifepolicy;
	
	@FindBy(id="lb_85")
	public WebElement dropdownPremiumPayor;
	
	@FindBy(id="lb_83")
	public WebElement dropdownRelationshiptoowner;
	
	@FindBy(id="flda_102")
	public WebElement payor_other_firstname;
	
	@FindBy(id="flda_120")
	public WebElement payor_other_lastname;
	
	@FindBy(id="flda_109")
	public WebElement payor_other_street;
	
	@FindBy(id="flda_110")
	public WebElement payor_other_city;
	
	@FindBy(id="lb_107")
	public WebElement payor_other_state;
	
	@FindBy(id="flda_113")
	public WebElement payor_other_zip;
	
	@FindBy(id="flda_162")
	public WebElement txtEarned;
	
	@FindBy(id="flda_163")
	public WebElement txtUnearned;
	
	@FindBy(id="flda_164")
	public WebElement txtNetWorth;
	
	@FindBy(id="cb_173")
	public WebElement authorization;
	
	@FindBy(id="btn_24")
	public WebElement btnNext;
	
}
