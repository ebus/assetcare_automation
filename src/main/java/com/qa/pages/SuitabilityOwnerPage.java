package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class SuitabilityOwnerPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameSuitabilityOwner;
	
	@FindBy(id="flda_56")
	public WebElement annualincome;
	
	@FindBy(id="flda_3")
	public WebElement sourceincome;
	
	@FindBy(id="flda_57")
	public WebElement annualhouseholdincome;
	
	@FindBy(id="flda_58")
	public WebElement networth;
	
	@FindBy(id="flda_59")
	public WebElement liquidAssets;
	
	@FindBy(id="lb_54")
	public WebElement taxbracket;
	
	@FindBy(id="rdo_1_1")
	public WebElement currentlyownanyannuities_yes;
	
	@FindBy(id="rdo_1_2")
	public WebElement currentlyownanyannuities_no;
	
	@FindBy(id="flda_2")
	public WebElement annuitieslist;
	
	@FindBy(id="rdo_19_1")
	public WebElement currentlyownlifeinsurance_yes;
	
	@FindBy(id="rdo_19_2")
	public WebElement currentlyownlifeinsurance_no;
	
	@FindBy(id="flda_20")
	public WebElement lifeinsurancelist;
	
	@FindBy(id="rdo_22_1")
	public WebElement incomecoveryourlivingexpenses_yes;
	
	@FindBy(id="rdo_22_2")
	public WebElement incomecoveryourlivingexpenses_no;
	
	@FindBy(id="rdo_27_1")
	public WebElement expectchangesyourlivingexpenses_yes;
	
	@FindBy(id="rdo_27_2")
	public WebElement expectchangesyourlivingexpenses_no;
	
	@FindBy(id="rdo_30_1")
	public WebElement anticipatechangesyouroutofpocketmedicalexpenses_yes;
	
	@FindBy(id="rdo_30_2")
	public WebElement anticipatechangesyouroutofpocketmedicalexpenses_no;
	
	@FindBy(id="rdo_35_1")
	public WebElement incomesufficienttocoverfuturechanges_yes;
	
	@FindBy(id="rdo_35_2")
	public WebElement incomesufficienttocoverfuturechanges_no;
	
	@FindBy(id="rdo_38_1")
	public WebElement emergencyfundforunexpectedexpenses_yes;
	
	@FindBy(id="rdo_38_2")
	public WebElement emergencyfundforunexpectedexpenses_no;
	
	@FindBy(id="cb_71")
	public WebElement annuitypurchasenotbasedonrecommendationofagent;
	
	@FindBy(id="btn_6")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public SuitabilityOwnerPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
