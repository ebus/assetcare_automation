package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PersonalWorksheetPage extends GenericFunction{
	//Initializing the Page Objects:
	public PersonalWorksheetPage() {
		super();
		PageFactory.initElements(driver, this);
	}
		
		
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement framePersonalWorksheet;
	
	@FindBy(id="modalIframe")
	public WebElement frameTobaccoDetails;
	
	@FindBy(id="flda_132")
	public WebElement permonthpremium;
	
	@FindBy(id="flda_131")
	public WebElement peryearpremium;
	
	@FindBy(id="flda_127")
	public WebElement onetime_single_premium;
	
	@FindBy(id="cb_136")
	public WebElement curr_employment_income;
	
	@FindBy(id="cb_36")
	public WebElement curr_employment_income_florida;
	
	@FindBy(id="cb_139")
	public WebElement savings;
	
	@FindBy(id="cb_44")
	public WebElement savings_florida;
	
	@FindBy(id="cb_137")
	public WebElement curr_investment_income;
	
	@FindBy(id="cb_140")
	public WebElement sellinvestments;
	
	@FindBy(id="cb_138")
	public WebElement other_curr_income;
	
	@FindBy(id="cb_45")
	public WebElement fmilywillpay;
	
	@FindBy(id="cb_141")
	public WebElement sell_other_assets;
	
	@FindBy(id="cb_143")
	public WebElement other;
	
	@FindBy(id="cb_142")
	public WebElement moneyfrom_family;
	
	@FindBy(id="flda_135")
	public WebElement other_details;
	
	@FindBy(id="lb_145")
	public WebElement affordto_keep_policy;
	
	@FindBy(id="lb_10")
	public WebElement household_annualincome;
	
	@FindBy(id="lb_12")
	public WebElement incometochangeovernxt10yrs;
	
	@FindBy(id="lb_147")
	public WebElement plan_pay_premiums_from_income;
	
	@FindBy(id="rdo_6_1")
	public WebElement inflation_protection_yes;
	
	@FindBy(id="rdo_6_2")
	public WebElement inflation_protection_no;
	
	@FindBy(id="cb_155")
	public WebElement pay_differences_from_income;
	
	@FindBy(id="cb_46")
	public WebElement pay_differences_from_income_florida;
	
	@FindBy(id="cb_151")
	public WebElement pay_differences_from_investments;
	
	@FindBy(id="cb_47")
	public WebElement pay_differences_from_savings_florida;
	
	@FindBy(id="cb_150")
	public WebElement pay_differences_from_savings;
	
	@FindBy(id="cb_152")
	public WebElement pay_differences_from_sellingassets;
	
	@FindBy(id="cb_154")
	public WebElement pay_differences_from_other;
	
	@FindBy(id="flda_149")
	public WebElement pay_differences_from_other_details;
	
	@FindBy(id="cb_153")
	public WebElement pay_differences_from_familymoney;
	
	@FindBy(id="cb_48")
	public WebElement pay_differences_from_familymoney_florida;
	
	@FindBy(id="flda_4")
	public WebElement elimination_period_days;
	
	@FindBy(id="flda_3")
	public WebElement elimination_period_cost;
	
	@FindBy(id="lb_21")
	public WebElement elimination_period_plantopay_your_care;
	
	@FindBy(id="lb_92")
	public WebElement assets;
	
	@FindBy(id="lb_90")
	public WebElement assetstochangeovernext10years;
	
	@FindBy(id="rdo_93_1")
	public WebElement disclosure_Statement1;
	
	@FindBy(id="rdo_93_2")
	public WebElement disclosure_Statement2;
	
	@FindBy(id="cb_156")
	public WebElement applicant_acknowledgement;
	
	@FindBy(id="cb_95")
	public WebElement applicant_acknowledgement_florida;
	
	@FindBy(id="cb_157")
	public WebElement agent_acknowledgementforimportance;
	
	@FindBy(id="cb_97")
	public WebElement agent_acknowledgementforimportance_florida;
	
	@FindBy(id="cb_99")
	public WebElement agent_acknowledgementfornotsuitability;

	@FindBy(id="btn_29")
	public WebElement btnNext;
	
	
}
