package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class eSigDisclosuresCoownerPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameESigDisclosures;
	
	@FindBy(id="rdo_57_1")
	public WebElement rdocoownerInsYes;
	
	@FindBy(id="lb_55")
	public WebElement dropdownIdentification_coowner;

	@FindBy(id="btn_82")
	public WebElement btnNext;
	
	
	//Initializing the Page Objects:
	public eSigDisclosuresCoownerPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
