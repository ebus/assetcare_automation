package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class SuitabilityOwnerContPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameSuitabilityOwnerCont;
	
	@FindBy(id="flda_71")
	public WebElement annuitypurchasingreason;
	
	@FindBy(id="cb_47")
	public WebElement investmentobjective_income;
	
	@FindBy(id="cb_54")
	public WebElement risktolerance_conservative;
	
	@FindBy(id="flda_68")
	public WebElement comments;
	
	@FindBy(id="flda_62")
	public WebElement investmentexperiencetypelengthoftime;
	
	@FindBy(id="flda_63")
	public WebElement sourceoffunds;
	
	@FindBy(id="flda_64")
	public WebElement howlongplantokeepproposedannuity;

	@FindBy(id="rdo_60_2")
	public WebElement proposedannuityreplaceproduct_no;
	
	@FindBy(id="btn_9")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public SuitabilityOwnerContPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
