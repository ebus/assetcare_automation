package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class RequestofFundsPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameRequestofFundsInfo;
	
	@FindBy(id="grdx8_addRowButton")
	public WebElement btnAdd;
	
	@FindBy(id="modalIframe")
	public WebElement frameRequestofFunds;
		
	@FindBy (id = "flda_75")
	public WebElement curr_Custodian;
	
	@FindBy (id = "flda_76")
	public WebElement address;
	
	@FindBy (id = "flda_77")
	public WebElement city;
	
	@FindBy (id = "lb_74")
	public WebElement state;
	
	@FindBy (id = "flda_73")
	public WebElement zip;
		
	@FindBy (id = "flda_72")
	public WebElement telephone;
	
	@FindBy (id = "lb_78")
	public WebElement dropDownSelectaccountinfo;
	
	@FindBy (id = "lb_119")
	public WebElement funds_transfered_from;
	
	@FindBy (id = "flda_79")
	public WebElement curr_policyoraccountno;
	
	@FindBy (id = "flda_80")
	public WebElement customername;
	
	@FindBy (id = "flda_116")
	public WebElement policy_value;
	
	@FindBy (id = "flda_81")
	public WebElement insured_name;	
	
	@FindBy (id = "flda_70")
	public WebElement taxpayor_identification_No;
	
	@FindBy (id = "rdo_100_1")
	public WebElement checkbox_policy_enclosed;
	
	@FindBy (id = "rdo_100_2")
	public WebElement checkbox_policy_lost;
	
	@FindBy (id = "flda_86")
	public WebElement txt1035ExchangesDetails;
	
	@FindBy (id = "rdo_115_1")
	public WebElement curr_status_qualifiedRetirement_plan;
	
	@FindBy (id = "rdo_115_2")
	public WebElement curr_status_traditional_IRA;
		
	@FindBy (id = "rdo_115_3")
	public WebElement curr_status_other;
	
	@FindBy (id = "flda_114")
	public WebElement curr_status_other_desc;
	
	@FindBy (id = "rdo_88_1")
	public WebElement taxtype_direct_rollover;
	
	@FindBy (id = "rdo_88_2")
	public WebElement taxtype_nondirect_rollover;
	
	@FindBy (id = "rdo_88_3")
	public WebElement taxtype_trusteetotrusteetransfer;
	
	@FindBy (id = "rdo_89_1")
	public WebElement amountoffund_rollover;
	
	@FindBy (id = "rdo_89_2")
	public WebElement amountoffund_partial_rollover;
	
	@FindBy (id = "rdo_99_1")
	public WebElement fundstype_depositscertificate;
	
	@FindBy (xpath = "//input[@class= 'jq-dte-month jq-dte-is-required hint']")
	public WebElement maturitydate_month;
	
	@FindBy (xpath = "//input[@class= 'jq-dte-day jq-dte-is-required hint']")
	public WebElement maturitydate_day;
	
	@FindBy (xpath = "//input[@class= 'jq-dte-year jq-dte-is-required hint']")
	public WebElement maturitydate_year;
	
	@FindBy (id = "rdo_99_2")
	public WebElement fundstype_mutualfund;
	
	@FindBy (id = "rdo_98_1")
	public WebElement amount_fund_all;
	
	@FindBy (id = "rdo_98_2")
	public WebElement amount_fund_partial;
	
	@FindBy (id = "rdo_94_1")
	public WebElement amount_fund_partial_amount;
	
	@FindBy (id = "rdo_94_2")
	public WebElement amount_fund_partial_percentage;
	
	@FindBy (id = "flda_93")
	public WebElement amount_fund_desc;
	
	@FindBy (id = "btn_28")
	public WebElement btnSave;
	
	@FindBy (id = "btn_5")
	public WebElement btnNext;
	
	//Initializing the Page Objects:
	public RequestofFundsPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
