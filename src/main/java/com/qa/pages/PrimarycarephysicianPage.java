package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PrimarycarephysicianPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameprimarycarephysician;
	
	@FindBy(id="flda_76")
	public WebElement primarycarephysician_name;
	
	@FindBy(id="flda_78")
	public WebElement city;
	
	@FindBy(id="lb_74")
	public WebElement state;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	public WebElement lastseendate_month;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	public WebElement lastseendate_date;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	public WebElement lastseendate_year;
	
	@FindBy(id="flda_94")
	public WebElement reasonforvisit;
	
	@FindBy(id="rdo_72_1")
	public WebElement medication_yes;
	
	@FindBy(id="rdo_72_2")
	public WebElement medication_no;
	
	
	
	@FindBy(id="flda_84")
	public WebElement primarycarephysician_name_secondinsured;
	
	@FindBy(id="flda_88")
	public WebElement city_secondinsured;
	
	@FindBy(id="lb_82")
	public WebElement state_secondinsured;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[1]")
	public WebElement lastseendate_monthsecondinsured;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[2]")
	public WebElement lastseendate_datesecondinsured;
	
	@FindBy(xpath = "//span[@class='jq-dte-inner jq-dte-is-required']/input[3]")
	public WebElement lastseendate_yearsecondinsured;
	
	@FindBy(id="flda_95")
	public WebElement reasonforvisit_secondinsured;
	
	@FindBy(id="rdo_99_1")
	public WebElement medication_yes_secondinsured;
	
	@FindBy(id="rdo_99_2")
	public WebElement medication_no_secondinsured;
	
	@FindBy(id="btn_7")
	public WebElement btnNext;
	
		
	//Initializing the Page Objects:
	public PrimarycarephysicianPage() {
		super();
		PageFactory.initElements(driver, this);
	}	
		
		
}
