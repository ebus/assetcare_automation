package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class FinancialSupplementContPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameFinancialSupplementCont;
	
	@FindBy(id="lb_44")
	public WebElement purpose_Business_Insurance;
	
	@FindBy(id="rdo_46_1")
	public WebElement creditor_Yes;
	
	@FindBy(id="rdo_46_2")
	public WebElement creditor_No;
	
	@FindBy(id="rdo_48_1")
	public WebElement insurance_requested_by_lender_yes;
	
	@FindBy(id="rdo_48_2")
	public WebElement insurance_requested_by_lender_no;
	
	@FindBy(id="flda_70")
	public WebElement lender_name;
	
	@FindBy(id="flda_72")
	public WebElement coverage_amount_required_Creditor;
	
	@FindBy(id="flda_71")
	public WebElement loan_amount;
	
	@FindBy(id="flda_53")
	public WebElement loan_duration;
	
	@FindBy(id="flda_1")
	public WebElement loan_purpose;
	
	@FindBy(id="flda_32")
	public WebElement other_purposes;
	
	@FindBy(id="rdo_55_1")
	public WebElement officers_Partners_being_insured_yes;
	
	@FindBy(id="rdo_55_2")
	public WebElement officers_Partners_being_insured_no;
	
	@FindBy(id="flda_25")
	public WebElement officers_Partnersdetails;
	
	@FindBy(id="flda_75")
	public WebElement current_year_to_date;
		
	@FindBy(id="flda_74")
	public WebElement current_year_to_date_thru;	
	
	@FindBy(id="flda_5")
	public WebElement total_assets_currentyear;
	
	@FindBy(id="flda_11")
	public WebElement total_liabilities_currentyear;
	
	@FindBy(id="flda_12")
	public WebElement GrosssalesRevenue_currentyear;
	
	@FindBy (id = "flda_14")
	public WebElement netincome_currentyear;
		
	@FindBy(id="flda_18")
	public WebElement total_assets_prevyear;
	
	@FindBy(id="flda_19")
	public WebElement total_liabilities_prevyear;
	
	@FindBy(id="flda_20")
	public WebElement GrosssalesRevenue_prevyear;
	
	@FindBy (id = "flda_22")
	public WebElement netincome_prevyear;
	
	@FindBy(id="btn_29")
	public WebElement btnNext;
	
	
	//Initializing the Page Objects:
	public FinancialSupplementContPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
