package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PartIIMedicalInterviewPage extends GenericFunction{
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement framePartIIMedicalInterview;
	
	@FindBy(id="btn_23")
	public WebElement orderinterview;
	
	@FindBy(id="btn_1")
	public WebElement btnNext;
	

	//Initializing the Page Objects:
	public PartIIMedicalInterviewPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
