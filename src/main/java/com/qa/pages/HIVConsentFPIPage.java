package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HIVConsentFPIPage extends GenericFunction{
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameHIVConsentFPI;
	
	@FindBy(id="rdo_227_1")
	public WebElement consenttobetestedforHIV;
	
	@FindBy(id="btn_21")
	public WebElement btnNext;
		
	//Initializing the Page Objects:
	public HIVConsentFPIPage() {
		super();
		PageFactory.initElements(driver, this);
	}	
		
		
}
