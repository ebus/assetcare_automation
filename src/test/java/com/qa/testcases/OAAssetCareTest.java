package com.qa.testcases;


import java.util.HashMap;
import java.util.Map;
import com.qa.util.GenericFunction;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AgentInformationAction;
import com.qa.pageActions.ApplicationSetupAction;
import com.qa.pageActions.ApplyESignatureSubmitAction;
import com.qa.pageActions.BeneficiaryInfoPageAction;
import com.qa.pageActions.CaseInformationPageAction;
import com.qa.pageActions.ConsumerSuitabilityDueDiligencePageAction;
import com.qa.pageActions.ExistingInsuranceAction;
import com.qa.pageActions.FirstProposedInsuredAction;
import com.qa.pageActions.HIVConsentFPIAction;
import com.qa.pageActions.HIVConsentSPIAction;
import com.qa.pageActions.HealthQuesAction;
import com.qa.pageActions.Illustration_PolicyPageAction;
import com.qa.pageActions.LeaveBehindFormsPageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.OtherInfoPageAction;
import com.qa.pageActions.OwnerInfoContPageAction;
import com.qa.pageActions.OwnerInformationPageAction;
import com.qa.pageActions.PartIIMedicalInterviewAction;
import com.qa.pageActions.PatriotActAction;
import com.qa.pageActions.PatriotActContAction;
import com.qa.pageActions.PersonalInfoAction;
import com.qa.pageActions.PersonalWorkbookAction;
import com.qa.pageActions.PolicyorPremiumInformationAction;
import com.qa.pageActions.PrimarycarephysicianAction;
import com.qa.pageActions.ProposedInsuredInfoAction;
import com.qa.pageActions.RepresentativeInformationAction;
import com.qa.pageActions.RequestofFundsPageAction;
import com.qa.pageActions.SecondProposedInsuredAction;
import com.qa.pageActions.SignatureMethodAction;
import com.qa.pageActions.SuitabilityAgentAction;
import com.qa.pageActions.SuitabilityOwnerAction;
import com.qa.pageActions.SuitabilityOwnerContAction;
import com.qa.pageActions.TemporaryInsuranceAgreementAction;
import com.qa.pageActions.ThirdPartyOptionPageAction;
import com.qa.pageActions.ValidateAndLockDataAction;
import com.qa.pageActions.eSigDisclosuresAction;
import com.qa.pageActions.eSigDisclosuresCoownerAction;
import com.qa.pageActions.eSignatureConsentAction;
import com.qa.pageActions.eSignaturePartiesAction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OAAssetCareTest extends TestBase {

	LoginPageAction loginPageAction;
	CaseInformationPageAction caseInformationPageAction;
	ProposedInsuredInfoAction proposedInsuredInfoAction;
	ApplicationSetupAction applicationSetupAction;
	FirstProposedInsuredAction firstProposedInsuredAction;
	SecondProposedInsuredAction secondProposedInsuredAction;
	PolicyorPremiumInformationAction policyorPremiumInformationAction;
	Illustration_PolicyPageAction illustration_PolicyPageAction;
	OwnerInformationPageAction ownerInformationPageAction;
	OwnerInfoContPageAction ownerInfoContPageAction;
	PrimarycarephysicianAction primarycarephysicianAction;
	BeneficiaryInfoPageAction beneficiaryInfoPageAction;
	HealthQuesAction healthQuesAction;
	PersonalInfoAction personalInfoAction;
	HIVConsentFPIAction hivConsentFPIAction;
	HIVConsentSPIAction hivConsentSPIAction;
	PatriotActAction patriotActAction;
	PatriotActContAction patriotActContAction;
	SuitabilityOwnerAction suitabilityOwnerAction;
	SuitabilityOwnerContAction suitabilityOwnerContAction;
	SuitabilityAgentAction suitabilityAgentAction;
	ConsumerSuitabilityDueDiligencePageAction consumerSuitabilityDueDiligencePageAction;
	OtherInfoPageAction otherInfoPageAction;
	ExistingInsuranceAction existingInsuranceAction;
	PersonalWorkbookAction personalWorkbookAction;
	RequestofFundsPageAction requestofFundsPageAction;
	ValidateAndLockDataAction validateAndLockDataAction;
	PartIIMedicalInterviewAction partIIMedicalInterviewAction;
	LeaveBehindFormsPageAction leaveBehindFormsPageAction;
	TemporaryInsuranceAgreementAction temporaryInsuranceAgreementAction;
	ThirdPartyOptionPageAction thirdPartyOptionPageAction;
	SignatureMethodAction signatureMethodAction;
	eSigDisclosuresAction eSigdiclosuresAction;
	eSigDisclosuresCoownerAction eSigDiclosuresCoownerAction;
	eSignatureConsentAction eSignatureconsentAction;
	eSignaturePartiesAction eSignaturepartiesAction;
	AgentInformationAction agentInformationAction;
	RepresentativeInformationAction representativeInformationAction;
	ApplyESignatureSubmitAction applyESignatureSubmitAction;
	
	GenericFunction genericFunction = new GenericFunction();

	public OAAssetCareTest() {
		super();
	}

	@DataProvider
	public Object[][] getApplicationData() {
		Object data[][] = TestUtil.getTestData("Application_I");
		return data;
	}
	
	public static Map<String, String> getApplicationextraData() {
		int iteration = 0;
		Map<String,String> m = new HashMap<String,String>();
		String[] arr = {"financiallifeinsurance", "financiallifeinsuranceyears","financialother","financialotheryears","riskexposure","financialtimehorizon",
				"surrenderedannuitylast5yrs","surrender_charge","aware_annuitycontractcontainnonguaranteedelements","accept_nonguaranteedelements",
				"sufficientliquidassetspaymedicalexpenses","pay_surrenderchargeorpenalty","Percentageofliquidnetworth","percentagenetworthpurchaseannuity","premiumpaidfromfundingsources",
				"surrendercharge_amount","plantokeepproposedannuitycontract","offer_fixedannuities","offer_variableannuities","offer_lifeinsurance",
				"offer_mutualFunds","offer_stocks","offer_certificateofdeposits","authorizedtosell","paidcashcompensationcommission",
				"paidcashcompensationfees","paidcashcompensationother","producerexercisedmaterialcontrol","insuring_company_name","policynumber","typeofinsurance","cashvalue","amountbenefit","yearissued","beingreplaced","exchange1035","premium_permonth","premium_permonth_amount","premium_peryear","premium_peryear_amount","onetime_single_premium_amount","current_employment_income",
				"current_investment_income","saving","sell_investments","other_current_income","sell_assets","moneyfromfamily","others",
				"otherfund","ques_affordto_keep_policy","household_annual_income","ques_incometochangeovernxt10yrs","ques_plan_pay_premiums_from_income","buy_inflation_protection","differences_paymentsource_income","differences_paymentsource_savings","differences_paymentsource_investments","differences_paymentsource_sell_assets",
				"differences_paymentsource_fromfamily","differences_paymentsource_other","paydiff_fromotherfund","No_of_days_elimination_prod","Approx_cost_period_of_care",
				"Payment_plan_for_care","assets_worth","assets_change_over_10years","disclosure_statement","curr_custodian","company_street_address","company_city","company_state","company_zip","company_telephone","account_info",
				"fund_transferred_from","current_AccNo","cust_name","policyvalue","insuredname","identification_no","policy_enclosed",
				"txt1035Exchangesdetail","policy_current_status","typeoftaxfreefundtransfer","fundamount","otherfund","amountoffund","partialfund",
				"maturitydate","streetaddress","city","zip","email","knowledge_replacement_exist_insurance","witness_completion_signatures","identificationway_wellknown",
				"identificationway_photoid","identificationway_related","Desc","evaluated_other_app","relationship","additional_representative"};
		Object data2[][] = TestUtil.getTestData("Application_II");
		for(int i=0; i< data2[iteration].length; i++) {
			m.put(arr[i], (String) data2[iteration][i]);
		}
		iteration += 1;
		return m;
	}



	@Test(priority = 1, dataProvider = "getApplicationData")
	public void AssetCareAppTest(String scenarioName, String indExecute, String userId, String pwd, String firstName, String middleName, String lastName,String dob, String gender, String state, String productType,
			String product, String illustration, String firstinsuredrisk, String tableratingfirstinsured, String secondinsured, String secondinsured_firstname, String secondinsured_lastname, String gender_secondinsured, 
			String dob_secondinsured, String ssn_secondInsured,String willThesecondProposedInsuredmarriedorpartner,String birthCountry_secondProposed,String birthState_secondProposed,String issecondProposedInsuredUSCitizen,
			String relationshiptoFirstinsured,String sameaddressasfirstproposedinsured,String Street_secondProposed,String City_secondProposed,String Zip_secondProposed,String email_secondProposed,String Phone_secondProposed,
			String risk_secondinsured, String tableratingsecondinsured,String productFundingOptions_illus,String recurr_premium_payperiod,String taxqualification,String inputmethod,String annuity_premium_amount,
			String life_premuim_amount,String inputmethod_recurringpremium,String inputmethod_premiumamount,String inputmethod_faceamount,String inputmethod_initialmonthlyltcbenefit,String acceleration_Benefits_Duration,
			String COB_Rider,String COB_paymentoption,String COB_duration,String COB_inflation,String AOB_inflation,String inflationduration,String PremiumDropinRider,String PremiumDropinRiderAmount,String NonforfeitureRider,
			String yearstoquote,String summarywithsign,String aboutOA,String financialprofessionals,String revisedQuote,String policynumber,String returnofpremium_cashfunding,String policyholderpayspremium,String startingyearthroughyear,
			String agentCode,String licenseNumber,String secondproposedinsured,String thirdPartyHIPAA,String name,String address,String city,String thirdpartystate,String zip,String medical_application_medium,String SSN,
			String willTheProposedInsuredmarriedorpartner, String birthCountry, String birthState, String Street, String City, String ZipCode,String PhoneNumber, String Email,   
			String earned,String unEarned, String netWorth, String isProposedInsuredUSCitizen, String countryofCitizenship, String doesProposedInsuredHoldGreenCard,	String greenCardNumber, 
			String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, String visaNumber, String provideDetails, String employerName,String occupation1,String dropdownOwneroflifepolicy,
			String dropdownPremiumpayor,String dropdownrelationshiptoowner, String productFundingOptions,String faceamount,String annuityowner_firstproposedinsured,String premiumorcash,String withapp,String app_premium_amount,
			String exchange_1035,String exchange_1035_premiumamount,String IRAtransfer,String IRAtransfer_premiumamount,String cdtransfer,String cdtransferpremium,String wishStateLifetofacilitate,String other_source,String sourceoffund,
			String other_premium,String aob_duration,String cob,String cob_duration,String cob_paymentperiod,String billing_mode,String providingbankinfo,String account_type,String account_category,String accountno,String routingno,
			String Nonforfeiture_Benefit,String aob_inflation,String cob_inflation,String withdrawal_begins,String annuitantpremiumpaymentsage59by2,String electperson,String recurr_premium_billingmode,String recurrpremium_providingbankinfo,
			String recurrpremium_account_type,String recurrpremium_account_category,String recurrpremium_cashwithapp,String cashorpremium,String cashwithapp_PremiumAmount,String premiumdropinrider,String premiumDropAmount,
			String clientsendfunddirect,String sendfundafterissuingpolicy,String typeOwner, String relnProposedIns, String ownerFirstName, String ownerLastName, String ownerSSN, String strDOB,  String ownerGender,String isMultipleOwner,
			String mulOwnerFirst,String mulOwnerLast,  String mOwner_Street, String mOwner_city, String mOwner_State, String mOwner_ZIPCode,String mOwner_DOB, String mOwner_SSN,  String numOfPB,String strShareIndicator, 
			String strDeceasedSharePaid, String strRelationshipBene1, String strFirstNameBene1, String strLastNameBene1,String strGenderBene1, String strSSNBene1, String strSharePerBene1, String strEntityNameBene1, String strCorporateOfficer_Bene1, 
			String strTitle_Bene1, String strStateIncorporation_Bene1, String strOtherRelation_Bene1, String strRelationshipBene2, String strFirstNameBene2, String strLastNameBene2, String strGenderBene2, String strSSNBene2, String strSharePerBene2, 
			String strEntityNameBene2, String strOtherRelation_Bene2, String strRelationshipBene3, String strFirstNameBene3, String strLastNameBene3, String strGenderBene3,String strSSNBene3, String strSharePerBene3, String strEntityNameBene3, 
			String strOtherRelation_Bene3, String strRelationshipBene4, String strFirstNameBene4, String strLastNameBene4, String strGenderBene4, String strSSNBene4, String strSharePerBene4, String strEntityNameBene4, String strOtherRelation_Bene4, 
			String strRelationshipBene5, String strFirstNameBene5, String strLastNameBene5, String strGenderBene5, String strSSNBene5, String strSharePerBene5, String strEntityNameBene5, String strOtherRelation_Bene5, String assignshareamongallbeneficiary,
			String deceasedbeneficiaryportionPaid,String relationshipProposedInsured,String cashorsavings,String earningsincome,String inheritance,String insuranceproceeds,String investementproceeds,String pension_IRA_RetirementSavings,
			String legalSettlements,String gift,String saleofbusiness,String transferorExchange_from_Existing_Life_Insurance,String transferorExchange_from_Existing_Annuity,String other,String otherfund,String formofID_value,String certify,
			String notifiedbyIRS, String financialobject_preservationofcapital,String financialobject_income,String financialobject_longtermgrowth,String financialobject_shorttermgrowth,String financialobject_incomeandgrowth, 
			String financialobject_longtermcarebenefit,String financialobject_interestearning,String financialobject_taxdeferral,String intendedUse_income,String intendedUse_assetaccumulation,String intendedUse_protectionofPrincipal,
			String intendedUse_retirementneedsplanning, String intendedUse_estatepreservationplanning,String intendedUse_longtermcareexpress,String intendedUse_deathbenefit,String intendedUse_other,String financialstocks,String financialstocksyears,
			String financialbonds,String financialbondsyears,String financialmutualfund,String financialmutualfundyears,String financialvariableannuities,String financialvariableannuitiesyears,String financialfixedannuities,String financialfixedannuitiesyears,String Signed_city) {
			
				Map<String,String> m = new HashMap<String,String>();
				m = getApplicationextraData(); 
				
				
				extentTest = extent.startTest("IllustrationTest - " + scenarioName);
				if (indExecute.equalsIgnoreCase("No")) {
					extentTest.log(LogStatus.INFO, "Execute column is No in the data sheet");
					throw new SkipException(scenarioName + " is Skipped");
				}
				
					
					try {
						Thread.sleep(3000);
						
						intialization();
		
						extentTest.log(LogStatus.INFO, "Browser used: " + prop.getProperty("browser"));
						extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
						
						loginPageAction = new LoginPageAction();
						caseInformationPageAction = loginPageAction.logIn(userId, pwd);
						illustration_PolicyPageAction = caseInformationPageAction.selectProductIllustration(firstName, lastName, dob, gender,
								state, productType, product, illustration);
						
						applicationSetupAction = illustration_PolicyPageAction.enterDataPolicyPage(illustration,state,
								firstinsuredrisk,tableratingfirstinsured, secondinsured, secondinsured_firstname, secondinsured_lastname, gender_secondinsured, dob_secondinsured, 
								risk_secondinsured, tableratingsecondinsured, productFundingOptions_illus,recurr_premium_payperiod,taxqualification,inputmethod,annuity_premium_amount, life_premuim_amount,inputmethod_recurringpremium,inputmethod_premiumamount,
								inputmethod_faceamount,inputmethod_initialmonthlyltcbenefit,acceleration_Benefits_Duration,COB_Rider, COB_paymentoption, COB_duration,COB_inflation,
								AOB_inflation,inflationduration,PremiumDropinRider,PremiumDropinRiderAmount,NonforfeitureRider,yearstoquote,summarywithsign,aboutOA,
								financialprofessionals,revisedQuote,policynumber,returnofpremium_cashfunding,policyholderpayspremium, startingyearthroughyear);
						
						firstProposedInsuredAction = applicationSetupAction.enterData(illustration,agentCode,licenseNumber,secondproposedinsured,thirdPartyHIPAA,name,address,city,
								thirdpartystate,zip,medical_application_medium);
						
						secondProposedInsuredAction = firstProposedInsuredAction.enterFirstProposedInsuredData(illustration,  
								SSN, willTheProposedInsuredmarriedorpartner, birthCountry, birthState, 
								Street, City, ZipCode, PhoneNumber, Email,earned, unEarned, netWorth, isProposedInsuredUSCitizen, countryofCitizenship, 
								doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate, doesProposedInsuredHoldUSVisa, 
								typeOfVisa, visaExpirationDate, visaNumber, provideDetails,employerName,occupation1,dropdownOwneroflifepolicy,dropdownPremiumpayor,dropdownrelationshiptoowner);
						
						policyorPremiumInformationAction = secondProposedInsuredAction.enterSecondProposedInsuredData(illustration,secondinsured_firstname, secondinsured_lastname, gender_secondinsured, dob_secondinsured,ssn_secondInsured,willThesecondProposedInsuredmarriedorpartner,birthCountry_secondProposed,birthState_secondProposed,issecondProposedInsuredUSCitizen,relationshiptoFirstinsured
								,sameaddressasfirstproposedinsured,Street_secondProposed,City_secondProposed,Zip_secondProposed,email_secondProposed,Phone_secondProposed,employerName,occupation1, countryofCitizenship, 
								doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate, doesProposedInsuredHoldUSVisa, 
								typeOfVisa, visaExpirationDate, visaNumber, provideDetails);
						
						ownerInformationPageAction = policyorPremiumInformationAction.enterDataPolicyorPremiumInformation(state,illustration,productFundingOptions_illus,taxqualification,productFundingOptions,faceamount,annuityowner_firstproposedinsured,premiumorcash,withapp,app_premium_amount,exchange_1035,exchange_1035_premiumamount,IRAtransfer,IRAtransfer_premiumamount,
								cdtransfer,cdtransferpremium,wishStateLifetofacilitate,other_source,
								sourceoffund,other_premium,aob_duration,cob,cob_duration,cob_paymentperiod,billing_mode,providingbankinfo,account_type,account_category,
								accountno,routingno,Nonforfeiture_Benefit,aob_inflation,cob_inflation,withdrawal_begins,annuitantpremiumpaymentsage59by2,inflationduration,electperson,recurr_premium_payperiod,
								recurr_premium_billingmode,recurrpremium_providingbankinfo,recurrpremium_account_type,recurrpremium_account_category,recurrpremium_cashwithapp,cashorpremium,cashwithapp_PremiumAmount,premiumdropinrider,premiumDropAmount,clientsendfunddirect,sendfundafterissuingpolicy);
		
						
						ownerInfoContPageAction = ownerInformationPageAction.enterOwnerInformationData(state, typeOwner, relnProposedIns,
								   ownerFirstName, ownerLastName, ownerSSN, strDOB,  ownerGender, isMultipleOwner,mulOwnerFirst, mulOwnerLast, 
								   mOwner_Street, mOwner_city, mOwner_State, mOwner_ZIPCode, mOwner_DOB, mOwner_SSN);
						
						primarycarephysicianAction = ownerInfoContPageAction.enterOwnerinfoCont(typeOwner);
						
						healthQuesAction = primarycarephysicianAction.enterPrimarycarephysiciandetails(medical_application_medium);
						 
						personalInfoAction = healthQuesAction.enterDataHealthpage(medical_application_medium);
						
						beneficiaryInfoPageAction = personalInfoAction.enterDataPersonalInfopage(medical_application_medium);

						temporaryInsuranceAgreementAction = beneficiaryInfoPageAction.detailsOfPrimaryBenef(numOfPB, strShareIndicator, strDeceasedSharePaid, strRelationshipBene1, 
								strFirstNameBene1, strLastNameBene1, strGenderBene1, strSSNBene1, strSharePerBene1, strEntityNameBene1, strCorporateOfficer_Bene1, 
								strTitle_Bene1, strStateIncorporation_Bene1, strOtherRelation_Bene1, strRelationshipBene2, strFirstNameBene2, strLastNameBene2, strGenderBene2, 
								strSSNBene2, strSharePerBene2, strEntityNameBene2, strOtherRelation_Bene2, strRelationshipBene3, strFirstNameBene3, strLastNameBene3, strGenderBene3, 
								strSSNBene3, strSharePerBene3, strEntityNameBene3, strOtherRelation_Bene3, strRelationshipBene4, strFirstNameBene4, strLastNameBene4, strGenderBene4, 
								strSSNBene4, strSharePerBene4, strEntityNameBene4, strOtherRelation_Bene4, strRelationshipBene5, strFirstNameBene5, strLastNameBene5, strGenderBene5, 
								strSSNBene5, strSharePerBene5, strEntityNameBene5, strOtherRelation_Bene5,assignshareamongallbeneficiary,deceasedbeneficiaryportionPaid,relationshipProposedInsured,productFundingOptions);
							
						hivConsentFPIAction = temporaryInsuranceAgreementAction.enterDataTempInsAgreement();
						
						hivConsentSPIAction = hivConsentFPIAction.enterDataHIVFPIConsent(illustration,state);
						
						patriotActAction = hivConsentSPIAction.enterDataHIVSPIConsent(illustration,state);
						
						patriotActContAction = patriotActAction.enterDataPatriotAct(cashorsavings,earningsincome,inheritance,insuranceproceeds,investementproceeds,pension_IRA_RetirementSavings,
								legalSettlements,gift,saleofbusiness,transferorExchange_from_Existing_Life_Insurance,transferorExchange_from_Existing_Annuity,
								other,otherfund,formofID_value,certify,notifiedbyIRS,state);
						 
						 suitabilityOwnerAction = patriotActContAction.enterDataPatriotActCont(formofID_value,isMultipleOwner);
						 
						 suitabilityOwnerContAction = suitabilityOwnerAction.enterDataSuitabilityOwner(state,illustration,productFundingOptions,productFundingOptions_illus);
						 
						 suitabilityAgentAction = suitabilityOwnerContAction.enterDataSuitabilityOwnerCont(state,illustration,productFundingOptions,productFundingOptions_illus);
						 
						 consumerSuitabilityDueDiligencePageAction = suitabilityAgentAction.enterDataeSuitabilityAgent(state,illustration,productFundingOptions,productFundingOptions_illus);
						
						 otherInfoPageAction = consumerSuitabilityDueDiligencePageAction.detailsOfConsumerSuitabilityDueDiligence(state,financialobject_preservationofcapital,financialobject_income,financialobject_longtermgrowth,financialobject_shorttermgrowth,financialobject_incomeandgrowth,financialobject_longtermcarebenefit,
								financialobject_interestearning,financialobject_taxdeferral,intendedUse_income,intendedUse_assetaccumulation,intendedUse_protectionofPrincipal,
								intendedUse_retirementneedsplanning,intendedUse_estatepreservationplanning,intendedUse_longtermcareexpress,intendedUse_deathbenefit,intendedUse_other,financialstocks,financialstocksyears,financialbonds,financialbondsyears,financialmutualfund,financialmutualfundyears,
								financialvariableannuities,financialvariableannuitiesyears,financialfixedannuities,financialfixedannuitiesyears,m.get("financiallifeinsurance"),m.get("financiallifeinsuranceyears"),m.get("financialother"),m.get("financialotheryears"),m.get("riskexposure"),m.get("financialtimehorizon"),
								m.get("surrenderedannuitylast5yrs"),m.get("surrender_charge"),m.get("aware_annuitycontractcontainnonguaranteedelements"),m.get("accept_nonguaranteedelements"),m.get("sufficientliquidassetspaymedicalexpenses"),m.get("pay_surrenderchargeorpenalty"),m.get("Percentageofliquidnetworth"),m.get("percentagenetworthpurchaseannuity"),m.get("premiumpaidfromfundingsources"),m.get("surrendercharge_amount"),m.get("plantokeepproposedannuitycontract"),m.get("offer_fixedannuities"),m.get("offer_variableannuities"),
								m.get("offer_lifeinsurance"),m.get("offer_mutualFunds"),m.get("offer_stocks"),m.get("offer_certificateofdeposits"),m.get("authorizedtosell"),m.get("paidcashcompensationcommission"),m.get("paidcashcompensationfees"),m.get("paidcashcompensationother"),m.get("producerexercisedmaterialcontrol"),productFundingOptions,typeOwner);
						
						existingInsuranceAction =  otherInfoPageAction.enterdataotherinfo(state);
						personalWorkbookAction = existingInsuranceAction.enterDataExistingInsurance(state,m.get("insuring_company_name"),m.get("policynumber"),m.get("typeofinsurance"),m.get("cashvalue"),m.get("amountbenefit"),m.get("yearissued"),m.get("beingreplaced"),m.get("exchange1035"));
						
						requestofFundsPageAction = personalWorkbookAction.enterDataPersonalWorkbook(state,m.get("premium_permonth"),m.get("premium_permonth_amount"),m.get("premium_peryear"),m.get("premium_peryear_amount"),m.get("onetime_single_premium_amount"),m.get("current_employment_income"),m.get("current_investment_income"),m.get("saving"),m.get("sell_investments"),m.get("other_current_income"),m.get("sell_assets"),m.get("moneyfromfamily"),
								m.get("others"),m.get("otherfund"),m.get("ques_affordto_keep_policy"),m.get("household_annual_income"),m.get("ques_incometochangeovernxt10yrs"),m.get("ques_plan_pay_premiums_from_income"),m.get("buy_inflation_protection"),m.get("differences_paymentsource_income"),m.get("differences_paymentsource_savings"),m.get("differences_paymentsource_investments"),m.get("differences_paymentsource_sell_assets"),
								m.get("differences_paymentsource_fromfamily"),m.get("differences_paymentsource_other"),m.get("paydiff_fromotherfund"),m.get("No_of_days_elimination_prod"),m.get("Approx_cost_period_of_care"),
								m.get("Payment_plan_for_care"),m.get("assets_worth"),m.get("assets_change_over_10years"),m.get("disclosure_statement"));
						
						validateAndLockDataAction = requestofFundsPageAction.detailsOfRequestfund(illustration,productFundingOptions_illus,productFundingOptions,cdtransfer,other_source,m.get("curr_custodian"),m.get("company_street_address"),m.get("company_city"),m.get("company_state"),m.get("company_zip"),m.get("company_telephone"),m.get("account_info"),
								m.get("fund_transferred_from"),m.get("current_AccNo"),m.get("cust_name"),m.get("policyvalue"),m.get("insuredname"),m.get("identification_no"),m.get("policy_enclosed"),
								m.get("txt1035Exchangesdetail"),m.get("policy_current_status"),m.get("typeoftaxfreefundtransfer"),m.get("fundamount"),m.get("otherfund"),m.get("amountoffund"),m.get("partialfund"),
								m.get("maturitydate"),premiumdropinrider,clientsendfunddirect);
						
						partIIMedicalInterviewAction = validateAndLockDataAction.enterDataValAndLockData();
						                                     
						leaveBehindFormsPageAction = partIIMedicalInterviewAction.validatePartIIMedicalInterview(medical_application_medium);
						
						signatureMethodAction = leaveBehindFormsPageAction.validateleaveBehindForms();
						
						eSigdiclosuresAction = signatureMethodAction.enterDataSignatureMethod();
						
						eSigDiclosuresCoownerAction = eSigdiclosuresAction.enterDataESigDisclosures();
						
						eSignatureconsentAction = eSigDiclosuresCoownerAction.enterDataESigDisclosurescoowner(isMultipleOwner);
						
						
						eSignaturepartiesAction = eSignatureconsentAction.enterDataESignatureConsent();
						
						agentInformationAction = eSignaturepartiesAction.enterDataeSignatureParties(Signed_city);
						
						representativeInformationAction = agentInformationAction.enterDataAgentInfo(state,m.get("streetaddress"),m.get("city"),m.get("zip"),m.get("email"),m.get("knowledge_replacement_exist_insurance"),m.get("witness_completion_signatures"),m.get("identificationway_wellknown"),
								m.get("identificationway_photoid"),m.get("identificationway_related"),m.get("Desc"),m.get("evaluated_other_app"),m.get("relationship"),m.get("additional_representative"));
						
						
						applyESignatureSubmitAction = representativeInformationAction.enterDataRepresentativeInfo(state,m.get("streetaddress"),m.get("city"),m.get("zip"),m.get("email"),m.get("knowledge_replacement_exist_insurance"),m.get("witness_completion_signatures"),m.get("identificationway_wellknown"),
								m.get("identificationway_photoid"),m.get("identificationway_related"),m.get("Desc"),m.get("evaluated_other_app"),m.get("relationship"),m.get("additional_representative"));
							
						applyESignatureSubmitAction.enterApplyESignatureSubmit(City,state);
			} catch (Exception e) {
				e.printStackTrace();
				genericFunction.takefailedScreenshot("Error");
				extentTest.log(LogStatus.FAIL, "Execute column is No in the data sheet");
				
			} finally {

			}
		}
	}

